# Podrozownik - React

Frontend written in React that runs on user website, and comunicates with api

## Installation

Install app with:

```bash
npm install
```

This will install all dependencies from `package.json` file and configure [git hooks](#git-hooks).

## Usage

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm pretty`

Launches the prettier, that make some actions to help team to read the same code the same way.

### `npm lint`

Checks if code is up to date with standards like `es6`.


## Git hooks

Git hooks are pre defined actions, that runs before `git commit` command. It is very helpful, becouse you don't have to run all the commands like `npm run lint` or `npm run test` etc. and do it for you to ensure that, code is valid with pep8.
