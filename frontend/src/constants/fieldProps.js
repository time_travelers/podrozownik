import * as emailValidator from "email-validator";

const isLetterIn = "(?=.*[A-Za-z])";
const isDigitIn = "(?=.*[0-9])";

const fieldProps = {
  email: {
    required: true,
    minLength: null,
    maxLength: 64,
    isValid: email => emailValidator.validate(email),
  },
  username: {
    required: true,
    minLength: 1,
    maxLength: 10,
    isValid: username => /^[A-Za-z0-9]+$/.test(username),
  },
  firstName: {
    required: false,
    minLength: null,
    maxLength: 64,
    isValid: name => /^[A-Za-z]*$/.test(name),
  },
  lastName: {
    required: false,
    minLength: null,
    maxLength: 64,
    isValid: name => /^[A-Za-z]*$/.test(name),
  },
  password: {
    required: true,
    minLength: 8,
    maxLength: 64,
    isValid: password => new RegExp(`^${isLetterIn}${isDigitIn}[A-Za-z0-9]{8,}$`).test(password),
  },
};

export default fieldProps;
