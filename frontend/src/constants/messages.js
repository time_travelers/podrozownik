export const userInfoMessages = {
  infoView: {
    header: "Dane osobowe",
    uploadPhotoButton: "Wybierz zdjęcie",
    editProfileButton: "Edytuj profil",
  },
  fieldLabels: {
    email: "email",
    username: "nazwa użytkownika",
    firstName: "imię",
    lastName: "nazwisko",
    oldPassword: "obecne hasło",
    newPassword: "nowe hasło",
    repeatNew: "powtórz nowe hasło",
  },
  editProfileView: {
    header: "Edytuj dane",
    changePasswordButton: "Ustaw nowe hasło",
    saveDataButton: "Zapisz",
    messages: {
      success: "Pomyślnie zaktualizowano dane!",
      noChangesError: "Nie wprowadzono żadnych zmian",
      passwordDiffers: "Hasła się różnią",
    },
  },
  editPasswordView: {
    header: "Edytuj hasło",
    saveDataButton: "Zapisz",
  },
  errorMessages: {
    parseError: {
      title: "Nie można nawiązać połączenia z serwerem!",
      message: "Niestety coś poszło nie tak 😞, spróbuj ponownie za chwilę!",
      buttonMessage: "Odśwież stronę",
    },
  },

  validation: {
    email: "Nieprawidłowy format emaila",
    firstName: "Nieprawidłowa wartość imienia",
    lastName: "Nieprawidłowa wartość nazwiska",
    password: "Hasło powinno zawierać tylko znaki a-z, A-Z, 0-9",
    tooShort: "Za mało znaków",
    tooLong: "Za dużo znaków",
    required: "Wymagane",
  },
};

export const LoginFormMessages = {
  popup: {
    title: "HERE WE GO",
  },
  fieldLabels: {
    username: "nick",
    password: "hasło",
    signin: "zaloguj się",
    signup: "zarejestruj się",
    forgotpassword: "zapomniałem hasła",
  },
  fieldLabelsEN: {
    username: "username",
    password: "password",
    signin: "sign in",
    signup: "sign up",
    forgotpassword: "forgot password",
  },
  infoMessages: {
    success: "Login successful. Redirecting to home page..",
    error204: "Username and password do not match",
    error: "Username does not exists",
    errorNetwork: "Network ERROR. Try refresh!",
    emptyfields: "Please fill all fields",
  },
};

export const RegisterFormMessages = {
  popup: {
    title: "REJESTRACJA",
  },
  popupEN: {
    title: "REGISTRATION",
  },
  fieldLabels: {
    username: "Podaj nick",
    email: "Wpisz email",
    password: "Wpisz hasło",
    password2: "Potwierdź hasło",
    register: "zarejestruj się",
    login: "już mam konto",
  },
  fieldLabelsEN: {
    username: "Enter username",
    email: "Enter email",
    password: "Enter password",
    password2: "Confirm password",
    register: "register",
    login: "already have an account",
  },
  infoMessages: {
    success: "Hasło zostało zresetowane. Przekierywowanie na stronę główną...",
    errorPasswords: "Hasła się nie zgadzają",
    error: "Wystąpił błąd",
    errorNetwork: "Błąd sieci. Spróbuj odświeżyć!",
    emptyfileds: "Proszę wpisać hasło i potwierdzić je",
  },
  infoMessagesEN: {
    success: "Registration successful. Redirecting to home page..",
    errorPasswords: "Passwords do not match",
    error: "Some error ocurred",
    errorNetwork: "Network ERROR. Try refresh!",
    emptyfileds: "Please fill all fields",
  },
};

export const ResetFormMessages = {
  popup: {
    title: "ODZYSKIWANIE HASŁA",
  },
  popupEN: {
    title: "PASSWORD RECOVERY",
  },
  fieldLabels: {
    email: " Wpisz email",
    reset: "zresetuj hasło",
    login: "już mam konto",
  },
  fieldLabelsEN: {
    email: " Enter email",
    reset: "reset password",
    login: "already have an account",
  },
  infoMessages: {
    success: "Hasło zostało zresetowane. Przekierywowanie na stronę główną...",
    errorPasswords: "Hasła się nie zgadzają",
    error: "Wystąpił błąd",
    errorNetwork: "Błąd sieci. Spróbuj odświeżyć!",
    emptyfileds: "Proszę wpisać hasło i potwierdzić je",
    validMail: "Proszę wpisać poprawmy email",
  },
  infoMessagesEN: {
    success: "Verification code generated. Check your email. Redirecting to home..",
    errorPasswords: "Passwords do not match",
    error: "Some error ocurred",
    errorNetwork: "Network ERROR. Try refresh!",
    emptyfileds: "Please fill all fields",
    validMail: "Please enter valid email",
  },
};

export const ResetFormConfirmMessages = {
  headerText: "Wpisz nowe hasło",
  fieldLabels: {
    password1: "Wpisz nowe hasło",
    password2: "Powtórz nowe hasło",
  },
  resetPasswordButtonLabel: "Zapisz",
  snackBarMessages: {
    success: "Pomyślnie zaktualizowano dane! Trwa pzekierowanie...",
    passwordDiffers: "Hasła się różnią",
  },
  errorMessages: {
    validation: {
      password:
        "Hasło powinno mieć co najmniej jedną literę i cyfrę,\n i zawierać tylko znaki a-z, A-Z, 0-9",
      tooShort: "Za mało znaków",
      tooLong: "Za dużo znaków",
      required: "Wymagane",
    },
    invalidUrlError: {
      title: "Problem z linkiem!",
      message: "Niestety podany link nie jest poprawny",
      buttonMessage: "Przejdź do strony głównej",
    },
    connectionError: {
      title: "Nie można nawiązać połączenia z serwerem!",
      message: "Niestety coś poszło nie tak 😞, spróbuj ponownie za chwilę!",
      buttonMessage: "Odśwież stronę",
    },
  },
};

export const SetPasswordMessages = {
  popup: {
    title: "USTAW NOWE HASŁO",
  },
  popupEN: {
    title: "SET NEW PASSWORD",
  },
  fieldLabels: {
    password: " Hasło",
    password2: " Potwierdź hasło",
    submit: "wyślij",
    mainPage: "wróc na stronę główną",
  },
  fieldLabelsEN: {
    password: " Password",
    password2: " Confirm password",
    submit: "submit",
    mainPage: "go to main page",
  },
  infoMessages: {
    success: "Hasło zostało zresetowane. Przekierywowanie na stronę główną...",
    errorPasswords: "Hasła się nie zgadzają",
    error: "Wystąpił błąd",
    errorNetwork: "Błąd sieci. Spróbuj odświeżyć!",
    emptyfileds: "Proszę wpisać hasło i potwierdzić je",
  },
  infoMessagesEN: {
    success: "Reset password successful. Redirecting to home page..",
    errorPasswords: "Passwords do not match",
    error: "Some error ocurred",
    errorNetwork: "Network ERROR. Try refresh!",
    emptyfileds: "Please enter password and confirm it",
  },
};

export const HomeViewMessages = {
  fieldLabels: {
    searchButton: "Szukaj",
    fromButton: "Skąd?",
    toButton: "Dokąd?",
    routeButton: "TRASA",
    tripButton: "WYCIECZKA",
    learnMore: "DOWIEDZ SIĘ WIĘCEJ",
  },
  fieldLabelsEN: {
    searchButton: "Search",
    fromButton: "Where from?",
    toButton: "Where to?",
    routeButton: "ROUTE",
    tripButton: "TRIP",
    learnMore: "LEARN MORE",
  },
  infoMessages: {
    success: "Hasło zostało zresetowane. Przekierywowanie na stronę główną...",
    errorPasswords: "Hasła się nie zgadzają",
    errorFetch: "Nie można pobrać danych",
    error: "Wystąpił błąd",
    errorNetwork: "Błąd sieci. Spróbuj odświeżyć!",
    emptyfileds: "Proszę wpisać hasło i potwierdzić je",
  },
  infoMessagesEN: {
    success: "Reset password successful. Redirecting to home page..",
    errorPasswords: "Passwords do not match",
    errorFetch: "Could not fetch data",
    error: "Some error ocurred",
    errorNetwork: "Network ERROR. Try refresh!",
    emptyfileds: "Please enter password and confirm it",
  },
};

export const RouteViewMessages = {
  fetchError: {
    title: "Problem z pobieraniem danych z serwera",
    message:
      "Niestety wystąpił niespodziewany problem przy próbie komunikacji z serwerem. Spróbuj ponownie później",
    buttonMessage: "Zamknij",
  },
};

export const TravelViewMessages = {
  fetchError: {
    title: "Nie można pobrać danych",
    tryAgain: "Spróbuj ponownie.",
    homePage: "Wróc do strony głównej",
    notLogged: "Musisz być zalogowany, aby zobaczyć wycieczkę",
  },
  fetchErrorEN: {
    title: "Could not fetch data",
    tryAgain: "Try again.",
  },
  buttons: {
    join: "Dołącz",
    whatsapp: "Grupa WhatsApp",
    leave: "Opuść",
  },
  buttonsEN: {
    join: "Join",
    whatsapp: "WhatsApp group",
    leave: "Leave",
  },
  description: {
    organizer: "Organizator",
    length: "Długość",
    date: "Data",
    meetingPoint: "Miejsce spotkania",
    route: "Trasa",
    info: "Info organizatora",
  },
  descriptionEN: {
    organizer: "Organizer",
    length: "Length",
    date: "Date",
    meetingPoint: "Meeting point",
    route: "Route",
    info: "Organizer's info",
  },
  participant: {
    participant: "Uczestnicy",
  },
  participantEN: {
    participant: "Participants",
  },
};

export const SearchBarMessages = {
  placeholder: "Gdzie jedziemy?",
  advancedButton: "Więcej",
  markFieldLabel: "Ocena",
  difficultyFieldLabel: "Trudność",
  timeField: {
    label: "CZAS TRWANIA",
    from: "Minimalny",
    to: "Maksymalny",
  },
};

export const NavbarMessages = {
  infoMessages: {
    login: "Zaloguj się",
    logout: "Wyloguj",
    register: "Zarejestruj się",
    userInfo: "O użytkowniku",
    home: "Strona główna",
    fetchError: "Błąd pobierania danych.",
  },
};

export const TraceDescriptionMessages = {
  infoMessages: {
    activityType: "Rodzaj aktywności",
    averageMark: "Średnia ocena",
    time: "Czas trwania",
    difficulty: "Poziom trudności",
    location: "Lokalizacja",
    description: "Opis trasy",
    length: "Długość trasy",
  },
};

export const commentFieldMessages = {
  infoMessages: {
    addCommentAs: "Dodaj komentarz jako :",
    comment: "Komentarz",
    addComment: "Dodaj komentarz",
    loadError: "Błąd pobierania...",
    fetchingData: "Ładowanie danych",
  },
};

export const mapMessages = {
  infoMessages: {
    fetchingData: "Ładowanie danych",
  },
};
