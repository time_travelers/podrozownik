import React, { useState, useEffect } from "react";
import "./App.css";
import UserInfo from "./components/user_info/UserInfo";
import { makeStyles, ThemeProvider } from "@material-ui/core/styles";
import Navbar from "./components/Navbar/Navbar";
import Footer from "./components/Footer/Footer";
import theme from "./theme";
import LoginForm from "./components/LoginForm/LoginForm";
import RegistrationForm from "./components/RegistrationForm/RegistrationForm";
import ResetPasswordConfirm from "./components/ResetPasswordConfirm/ResetPassword";
import ResetPasswordForm from "./components/ResetPasswordForm/ResetPasswordForm";
import RouteList from "./components/RouteList/RouteList";
import HomeView from "./components/HomeView/HomeView";
import { ACCESS_TOKEN_NAME } from "./constants/apiConstants";
import { BrowserRouter as Router, Route } from "react-router-dom";
import "./App.css";
import RouteView from "./components/RouteView/RouteView";
import TravelView from "./components/TravelView/TravelView";

const useStyles = makeStyles(() => ({
  root: {
    minHeight: "calc(100vh - 80px - 19px)",
  },
}));

function App() {
  const [page, setPage] = useState("home");
  const [loggedUser, setLoggedUser] = useState(null);
  const classes = useStyles();

  function updateLoggedUser() {
    setLoggedUser(JSON.parse(localStorage.getItem(ACCESS_TOKEN_NAME)));
  }

  useEffect(() => {
    updateLoggedUser();
  }, [page]);

  return (
    <ThemeProvider theme={theme}>
      <div className="App">
        <Router>
          <Route exact path="/">
            {loggedUser && (
              <Navbar
                type="login"
                setPage={page => {
                  setPage(page);
                  updateLoggedUser();
                }}
              />
            )}
            {!loggedUser && (
              <Navbar
                type="logout"
                setPage={page => {
                  setPage(page);
                  updateLoggedUser();
                }}
              />
            )}
            <div className={classes.root}>
              {page === "home" && <HomeView setPage={setPage} />}
              {page === "register" && <RegistrationForm setPage={setPage} />}
              {page === "login" && <LoginForm setPage={setPage} />}
              {page === "reset-password" && <ResetPasswordForm setPage={setPage} />}
              {page === "list" && <RouteList setPage={setPage} />}
              {/^travel/.test(page) && (
                <TravelView setPage={setPage} travelId={page.replace(/^travel#/, "")} />
              )}
              {/^route/.test(page) && (
                <RouteView
                  userId={loggedUser ? loggedUser.id : null}
                  routeId={page.replace(/^route#/, "")}
                  setPage={setPage}
                />
              )}
              {loggedUser && page === "userinfo" && (
                <UserInfo userId={loggedUser.id} setPage={setPage} />
              )}
            </div>
          </Route>
          <Route
            path="/users/new-password"
            render={props => {
              <div>
                <Navbar type="empty" setPage={setPage} />
                <ResetPasswordConfirm {...props} />
              </div>;
            }}
          />
        </Router>
        <Footer />
      </div>
    </ThemeProvider>
  );
}

export default App;
