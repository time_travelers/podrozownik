import React from "react";
import App from "./App";
import { shallow } from "enzyme";
import toJson from "enzyme-to-json";

//render test
describe("rendering components", () => {
  it("test if navbar components render properly", () => {
    shallow(<App></App>);
  });
});

describe("snapshots", () => {
  it("App snapshots", () => {
    const tree = shallow(<App></App>);
    expect(toJson(tree)).toMatchSnapshot();
  });
});
