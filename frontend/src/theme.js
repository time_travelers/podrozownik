import { grey, red } from "@material-ui/core/colors";
import { createMuiTheme } from "@material-ui/core/styles";

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#e4e4e4",
      title: grey[300],
      subtitle: grey[500],
      content: grey[400],
      shadow: "#3b3939",
    },
    background: {
      routeTile: grey[700],
    },
    secondary: grey,
    error: red,
  },
  typography: {
    fontFamily: ["system-ui", "Montserrat", "Helvetica", "Arial", "sans-serif"],

    subtitle2: {
      color: "rgba(0, 0, 0, 0.6)",
    },
  },
});

export default theme;
