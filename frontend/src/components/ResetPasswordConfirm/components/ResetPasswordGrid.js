import React, { Component } from "react";
import PropTypes from "prop-types";
import Header from "../../user_info/Hooks/Header";
import { Button, Grid, withStyles, Snackbar } from "@material-ui/core";
import MuiAlert from "@material-ui/lab/Alert";
import fieldProps from "../../../constants/fieldProps";
import { Redirect } from "react-router-dom";

import { ResetFormConfirmMessages } from "../../../constants/messages";
import { Form, Field } from "react-final-form";
import { TextField } from "final-form-material-ui";
import { sendResetedPassword } from "../Api";
import styles from "../../user_info/components/FormStyles";

/**
 * Messages and lebels got from outer file
 */
const {
  headerText,
  fieldLabels,
  resetPasswordButtonLabel,
  snackBarMessages,
  errorMessages,
} = ResetFormConfirmMessages;

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

class ResetPasswordConfirmGrid extends Component {
  constructor(props) {
    super(props);
    this.state = { email: props.email, token: props.token, mounted: false, submitting: false };
    this.handleCloseError = this.handleCloseError.bind(this);
    this.resetPassword = this.resetPassword.bind(this);
    this.validate = this.validate.bind(this);
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({ mounted: true });
    }, 250);
  }

  validate(values) {
    const errors = {};
    ["password1", "password2"].forEach(fieldName => {
      const props = fieldProps["password"];
      const value = values[fieldName] || "";

      if (!value) {
        errors[fieldName] = errorMessages.validation.required;
      } else if (value.length < props.minLength) {
        errors[fieldName] = errorMessages.validation.tooShort;
      } else if (value.length > props.maxLength) {
        errors[fieldName] = errorMessages.validation.tooLong;
      } else if (fieldName !== "password2" && !props.isValid(value)) {
        errors[fieldName] = errorMessages.validation.password;
      }
    });
    return errors;
  }

  handleCloseError() {
    this.setState(state => ({ ...state, showSnackbar: false }));
  }

  render() {
    const { classes } = this.props;

    if (this.state.redirect) {
      return <Redirect push to="/" />;
    }

    function getClass(willUnmount, mounted) {
      if (willUnmount) {
        return classes.hide;
      } else if (!mounted) {
        return classes.init;
      } else {
        return classes.show;
      }
    }

    const fieldProps = {
      fullWidth: true,
      variant: "outlined",
      component: TextField,
      InputLabelProps: {
        shrink: true,
        className: classes.cssLabel,
      },
      InputProps: {
        classes: {
          root: classes.cssLabel,
          notchedOutline: classes.notchedOutline,
        },
      },
    };

    return (
      <div className={getClass(this.props.willUnmount, this.state.mounted)}>
        <Header title={headerText} />
        <Form
          onSubmit={this.resetPassword}
          validate={this.validate}
          render={({ handleSubmit }) => (
            <form onSubmit={handleSubmit} noValidate>
              <Grid container className={classes.form}>
                <Grid item style={{ minHeight: "95px" }} className={classes.buttonWidth}>
                  <Field
                    name="password1"
                    type="password"
                    label={fieldLabels.password1}
                    required
                    InputProps={{ autoComplete: "new-password" }}
                    {...fieldProps}
                  />
                </Grid>
                <Grid item style={{ minHeight: "70px" }} className={classes.buttonWidth}>
                  <Field
                    name="password2"
                    type="password"
                    label={fieldLabels.password2}
                    required
                    InputProps={{ autoComplete: "new-password" }}
                    {...fieldProps}
                  />
                </Grid>
                <Grid item className={classes.buttonWidth}>
                  <Button
                    variant="outlined"
                    type="submit"
                    color="primary"
                    fullWidth
                    className={classes.buttonStyle}
                  >
                    {resetPasswordButtonLabel}
                  </Button>
                </Grid>
              </Grid>
            </form>
          )}
        />
        <Snackbar
          open={this.state.showSnackbar}
          onClose={this.handleCloseError}
          autoHideDuration={4000}
        >
          <Alert severity={this.state.snackbarType}>{this.state.snackbarMessage}</Alert>
        </Snackbar>
      </div>
    );
  }

  resetPassword(values) {
    this.setState(state => ({ ...state, submitting: true }));
    const resetPassword = {
      password1: values["password1"],
      password2: values["password2"],
    };

    if (resetPassword.password1 !== resetPassword.password2) {
      this.setState(state => ({
        ...state,
        showSnackbar: true,
        snackbarType: "error",
        snackbarMessage: snackBarMessages.passwordDiffers,
        submitting: false,
      }));
      return;
    }

    sendResetedPassword(
      {
        email: this.state.email,
        token: this.state.token,
        password: resetPassword["password1"],
      },
      () => {
        this.setState(state => ({
          ...state,
          showSnackbar: true,
          snackbarType: "success",
          snackbarMessage: snackBarMessages.success,
          submitting: false,
        }));

        setTimeout(() => {
          this.setState(state => ({
            ...state,
            redirect: true,
          }));
        }, 3000);
      },
      error => {
        if (error.message) {
          this.setState(state => ({
            ...state,
            showSnackbar: true,
            snackbarType: "error",
            snackbarMessage: `HTTP Error: ${error.message}`,
            submitting: false,
          }));
        } else {
          const errorMessage = Object.keys(error).map(fieldKey => error[fieldKey].join(", "));
          this.setState(state => ({
            ...state,
            showSnackbar: true,
            snackbarType: "error",
            snackbarMessage: `HTTP Error: ${errorMessage}`,
            submitting: false,
          }));
        }
      }
    );
  }
}

ResetPasswordConfirmGrid.propTypes = {
  email: PropTypes.string,
  token: PropTypes.string,
  classes: PropTypes.object,
  willUnmount: PropTypes.bool,
};

export default withStyles(styles)(ResetPasswordConfirmGrid);
