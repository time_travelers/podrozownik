import axios from "axios";

export function sendResetedPassword(resetPasswordForm, onSuccess, onError) {
  axios
    .post("/users/reset-password/confirm/", resetPasswordForm)
    .then(response => {
      onSuccess(response);
    })
    .catch(error => {
      if (error.response) {
        onError(error.response.data);
      } else {
        onError(error);
      }
    });
}
