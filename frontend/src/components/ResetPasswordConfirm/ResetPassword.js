import { Grid, withStyles } from "@material-ui/core";
import PropTypes from "prop-types";
import React, { useState } from "react";
import { Redirect } from "react-router-dom";

import ErrorTile from "../ErrorTile";
import ResetPasswordGrid from "./components/ResetPasswordGrid";
import { ResetFormConfirmMessages } from "../../constants/messages";
import { styles } from "../user_info/UserInfoStyles";

function getEmailAndPassword({ search }) {
  const [email, token] = search
    .replace("?", "")
    .split("&")
    .map(param => param.split("=")[1]);
  if (email && token) {
    return {
      email,
      token,
      state: "resetPass",
      redirect: false,
      willUnmount: false,
    };
  } else {
    return {
      state: "error",
    };
  }
}

const { invalidUrlError } = ResetFormConfirmMessages.errorMessages;

function ResetPasswordConfirm(props) {
  const { classes } = props;
  const [currentState, setCurrentState] = useState(getEmailAndPassword(props.location));

  if (currentState.redirect) {
    return <Redirect push to="/" />;
  }

  return (
    <>
      <div className={classes.spacing}></div>
      <Grid container className={classes.tile}>
        <Grid item xs={1} md={2}></Grid>
        <Grid item xs={10} md={8} className={classes.paper}>
          <div className={classes.wraper}>
            {currentState.state === "resetPass" && (
              <ResetPasswordGrid
                willUnmount={currentState.willUnmount}
                email={currentState.email}
                token={currentState.token}
              />
            )}

            {currentState.state === "error" && (
              <ErrorTile
                title={invalidUrlError.title}
                message={invalidUrlError.message}
                buttonMessage={invalidUrlError.buttonMessage}
                onExit={() => setCurrentState(state => ({ ...state, redirect: true }))}
              />
            )}
          </div>
        </Grid>
        <Grid item xs={1} md={2}></Grid>
      </Grid>
      <div className={classes.spacing}></div>
    </>
  );
}

ResetPasswordConfirm.propTypes = {
  classes: PropTypes.object,
  history: PropTypes.object,
  location: PropTypes.object,
};

export default withStyles(styles)(ResetPasswordConfirm);
