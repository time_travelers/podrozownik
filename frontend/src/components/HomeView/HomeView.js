import React, { useEffect, useState } from "react";
import {
  Grid,
  Button,
  Card,
  CardActions,
  CardActionArea,
  CardContent,
  CardMedia,
  Typography,
  Container,
} from "@material-ui/core";
import MuiAlert from "@material-ui/lab/Alert";
import { Snackbar } from "@material-ui/core";
import { ThemeProvider } from "@material-ui/styles";
import { HomeViewMessages as messages } from "../../constants/messages";
import PropTypes from "prop-types";
import { customTheme, useStyles } from "./HomeViewStyles";
import SimpleSlider from "../SimpleSlider";
import { pullFeatured, pullTravels } from "./Api";
import { ACCESS_TOKEN_NAME } from "../../constants/apiConstants.js";
import SecretRouteInput from "./hooks/SecretRouteInput";

/**
 * Messages and lebels got from outer file
 */
const { fieldLabels, infoMessages } = messages;

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function HomeView(props) {
  const classes = useStyles();
  const [featured, setFeatured] = useState([]);
  const [travels, setTravels] = useState([]);
  const [userInfo, setUserInfo] = useState([]);
  const [openInput, setInputOpen] = useState(false);

  const [state, setState] = useState({
    from: "",
    to: "",
    showSnackbar: false,
    snackbarType: "error",
    snackbarMessage: "test",
  });

  useEffect(() => {
    fetchFeatured();
    fetchTravels();
  }, []);

  const toggleAlert = (type, message) => {
    setState(prevState => ({
      ...prevState,
      showSnackbar: true,
      snackbarType: type,
      snackbarMessage: message,
    }));
  };

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setState(prevState => ({ ...prevState, showSnackbar: false }));
  };

  const fetchFeatured = () => {
    checkUserInfo();
    pullFeatured(
      userData => {
        setFeatured(userData);
      },
      () => {
        toggleAlert("error", infoMessages.errorFetch);
      }
    );
  };

  const fetchTravels = () => {
    pullTravels(
      userData => {
        setTravels(userData);
      },
      () => {
        toggleAlert("error", infoMessages.errorFetch);
      }
    );
  };

  function goToRoute(routeid) {
    props.setPage(`route#${routeid}`);
  }

  function goToTravel(travelID) {
    props.setPage(`travel#${travelID}`);
  }

  function checkUserInfo() {
    if (JSON.parse(localStorage.getItem(ACCESS_TOKEN_NAME))) {
      setUserInfo(JSON.parse(localStorage.getItem(ACCESS_TOKEN_NAME)));
    }
  }

  return (
    <div className={classes.root}>
      <SecretRouteInput openInput={openInput} setInputOpen={setInputOpen} setPage={props.setPage} />
      <Container maxWidth="lg">
        <Grid container spacing={3}>
          <Snackbar open={state.showSnackbar} onClose={handleClose} autoHideDuration={5000}>
            <Alert severity={state.snackbarType}>{state.snackbarMessage}</Alert>
          </Snackbar>
          <Grid item xs={12}>
            <SimpleSlider featured={featured} />
          </Grid>
          <div style={{ padding: "12px" }}>
            <h1 className={classes.header1}>
              Polecane trasy
              <Button className={classes.header2} onClick={() => props.setPage("list")}>
                <h5>Zobacz wszytskie trasy</h5>
              </Button>
            </h1>

            <Grid item xs={12}>
              <Grid container spacing={3} className={classes.cardContainer}>
                {featured.slice(0, 6).map((item, index) => {
                  return (
                    <Grid item xs={12} sm={6} md={4} key={index}>
                      <ThemeProvider theme={customTheme}>
                        <Card className={classes.card} key={item}>
                          <CardActionArea>
                            <CardMedia
                              component="img"
                              alt="Image"
                              height="140"
                              image={item.photos[0].link}
                              title={item.title}
                            />
                            <CardContent onClick={() => goToRoute(item.id)}>
                              <Typography gutterBottom variant="h5" component="h2">
                                {item.title}
                              </Typography>
                              <Typography variant="body2" color="textSecondary" component="p">
                                {item.short_description}
                              </Typography>
                            </CardContent>
                          </CardActionArea>
                          <CardActions>
                            <Button size="small" color="default" onClick={() => goToRoute(item.id)}>
                              {fieldLabels.learnMore}
                            </Button>
                          </CardActions>
                        </Card>
                      </ThemeProvider>
                    </Grid>
                  );
                })}
              </Grid>
            </Grid>
          </div>
          {userInfo && (
            <div>
              <h1 className={classes.header1}>
                Polecane wycieczki
                <Button className={classes.header2} onClick={() => setInputOpen(true)}>
                  <h5>Dołącz do prywatnej wycieczki</h5>
                </Button>
              </h1>

              <Grid item xs={12}>
                <Grid container spacing={3} className={classes.cardContainer}>
                  {travels.slice(0, 6).map((item, index) => {
                    if (!item.is_private) {
                      return (
                        <Grid item xs={12} sm={6} md={4} key={index}>
                          <ThemeProvider theme={customTheme}>
                            <Card className={classes.card} key={item}>
                              <CardActionArea>
                                <CardMedia
                                  component="img"
                                  alt="Image"
                                  height="140"
                                  image={item.photos[0]}
                                  title={item.title}
                                />
                                <CardContent onClick={() => goToTravel(item.secret_id)}>
                                  <Typography gutterBottom variant="h5" component="h2">
                                    {item.title}
                                  </Typography>
                                  <Typography variant="body2" color="textSecondary" component="p">
                                    {item.description}
                                  </Typography>
                                </CardContent>
                              </CardActionArea>
                              <CardActions>
                                <Button
                                  size="small"
                                  color="default"
                                  onClick={() => goToTravel(item.secret_id)}
                                >
                                  {fieldLabels.learnMore}
                                </Button>
                              </CardActions>
                            </Card>
                          </ThemeProvider>
                        </Grid>
                      );
                    }
                  })}
                </Grid>
              </Grid>
            </div>
          )}
          {userInfo && (
            <div>
              <h1 className={classes.header1}>Twoje wycieczki</h1>

              <Grid item xs={12}>
                <Grid container spacing={3} className={classes.cardContainer}>
                  {travels
                    .filter(item => {
                      return (
                        item.participants.find(participant => participant.id === userInfo.id) !==
                        undefined
                      );
                    })
                    .map((travel, index) => {
                      return (
                        <Grid item xs={12} sm={6} md={4} key={index}>
                          <ThemeProvider theme={customTheme}>
                            <Card className={classes.card} key={travel}>
                              <CardActionArea>
                                <CardMedia
                                  component="img"
                                  alt="Image"
                                  height="140"
                                  image={travel.photos[0]}
                                  title={travel.title}
                                />
                                <CardContent onClick={() => goToTravel(travel.secret_id)}>
                                  <Typography gutterBottom variant="h5" component="h2">
                                    {travel.title}
                                  </Typography>
                                  <Typography variant="body2" color="textSecondary" component="p">
                                    {travel.description}
                                  </Typography>
                                </CardContent>
                              </CardActionArea>
                              <CardActions>
                                <Button
                                  size="small"
                                  color="default"
                                  onClick={() => goToTravel(travel.secret_id)}
                                >
                                  {fieldLabels.learnMore}
                                </Button>
                              </CardActions>
                            </Card>
                          </ThemeProvider>
                        </Grid>
                      );
                    })}
                  ;
                </Grid>
              </Grid>
            </div>
          )}
        </Grid>
      </Container>
    </div>
  );
}

HomeView.propTypes = {
  setPage: PropTypes.func,
};

export default HomeView;
