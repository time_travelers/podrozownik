import React, { useState } from "react";
import PropTypes from "prop-types";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  TextField,
  withStyles,
} from "@material-ui/core";

const styles = {
  paper: {
    backgroundColor: "#737373",
  },
};

function SecretRouteInput(props) {
  const [routeCode, setRouteCode] = useState("");
  const { classes } = props;

  function handleClose(event) {
    props.setInputOpen(false);
    console.log(props, event);
  }

  function handleGo() {
    props.setInputOpen(false);
    props.setPage(`travel#${routeCode}`);
  }

  return (
    <Dialog
      open={props.openInput}
      onClose={handleClose}
      classes={{ ...classes }}
      aria-labelledby="form-dialog-title"
    >
      <DialogTitle id="form-dialog-title" style={{ color: "white" }}>
        Znajdź ukrytą trasę
      </DialogTitle>
      <DialogContent>
        <DialogContentText color="primary">
          Aby przejść do prywatnej wycieczki wpisz kod zaproszeniowy poniżej.
        </DialogContentText>
        <TextField
          autoFocus
          margin="dense"
          id="name"
          label="Kod do trasy"
          color="primary"
          onChange={value => setRouteCode(value.target.value)}
          fullWidth
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose} color="primary">
          Anuluj
        </Button>
        <Button onClick={handleGo} color="primary">
          Przejdź
        </Button>
      </DialogActions>
    </Dialog>
  );
}

SecretRouteInput.propTypes = {
  openInput: PropTypes.bool,
  setInputOpen: PropTypes.func,
  setPage: PropTypes.func,
  classes: PropTypes.object,
};

export default withStyles(styles)(SecretRouteInput);
