import axios from "axios";

export function pullFeatured(onSuccess, onError) {
  axios
    .get("/route")
    .then(response => {
      onSuccess(response.data);
    })
    .catch(error => {
      onError(error);
    });
}

export function pullTravels(onSuccess, onError) {
  axios
    .get("/travels/")
    .then(response => {
      onSuccess(response.data);
    })
    .catch(error => {
      onError(error);
    });
}
