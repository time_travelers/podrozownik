import { createMuiTheme } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

export const customTheme = createMuiTheme({
  palette: {
    type: "dark",
  },
});

export const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  card: {
    margin: "auto",
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: "black",
  },
  button: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: "black",
    width: "100%",
  },
  toggle: {
    padding: theme.spacing(3),
    textAlign: "center",
    color: "white",
    width: "100%",
    display: "inline-block",
  },
  togglebutton: {
    backgroundColor: "white",
    textAlign: "center",
    color: "black",
    "font-size": "1.5rem !important",
    width: "50%",
    padding: "auto 20px",
    "&:hover": {
      backgroundColor: "grey !important",
      color: "white" + " !important",
    },
  },
  togglebuttonselected: {
    backgroundColor: "grey !important",
    color: "white !important",
    "font-size": "1.5rem !important",
    boxShadow: "0 0 0 0.2rem rgba(0,0,0,.25) !important",
  },
  header1: {
    color: "white",
    opacity: "0.7",
    paddingLeft: "36px",
  },
  header2: {
    position: "relative",
    color: "white",
    opacity: "0.5",
    left: "10px",
    top: "5px",
    "&:hover": {
      backgroundColor: "rgb(0,0,0,0) !important",
      opacity: "1",
    },
    [theme.breakpoints.down("xs")]: {
      left: "30px",
    },
  },
  margin: {
    margin: theme.spacing(1),
    "flex-direction": "row",
    width: "30%",
  },
  marginSM: {
    margin: theme.spacing(1),
    "flex-direction": "row",
    width: "auto",
    position: "relative",
    display: "block",
  },
  cardContainer: {
    padding: "24px",
  },
}));
