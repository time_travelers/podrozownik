import React, { useState } from "react";
import "./ResetPasswordForm.css";
import PropTypes from "prop-types";
import Popup from "../Popup/Popup";
import { Snackbar } from "@material-ui/core";
import MuiAlert from "@material-ui/lab/Alert";
import { ResetFormMessages as messages } from "../../constants/messages";
import { pushResetData } from "./Api";

/**
 * Messages and lebels got from outer file
 */
const { popup, fieldLabels, infoMessages } = messages;

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function ResetPasswordForm(props) {
  const [state, setState] = useState({
    email: "",
    successMessage: null,
    showSnackbar: false,
    snackbarType: "error",
    snackbarMessage: "test",
  });
  const toggleAlert = (type, message) => {
    setState(prevState => ({
      ...prevState,
      showSnackbar: true,
      snackbarType: type,
      snackbarMessage: message,
    }));
  };
  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setState(prevState => ({ ...prevState, showSnackbar: false }));
  };
  const handleChange = e => {
    const { id, value } = e.target;
    setState(prevState => ({
      ...prevState,
      [id]: value,
    }));
  };
  const sendDetailsToServer = () => {
    if (state.email.length) {
      const payload = {
        email: state.email,
      };
      pushResetData(
        payload,
        response => {
          if (response.status === 200) {
            toggleAlert("success", infoMessages.success);
            setTimeout(() => {
              redirectToHome();
            }, 5000);
          } else {
            toggleAlert("error", infoMessages.error);
          }
        },
        () => {
          toggleAlert("error", infoMessages.errorNetwork);
        }
      );
    } else {
      toggleAlert("warning", infoMessages.validMail);
    }
  };
  const redirectToHome = () => {
    props.setPage("");
  };
  const redirectToLogin = () => {
    props.setPage("login");
  };
  const handleSubmitClick = e => {
    e.preventDefault();
    sendDetailsToServer();
  };
  return (
    <Popup
      content={
        <>
          <div className="card login-card hv-center">
            <div className="title-card-register">
              <h1>{popup.title}</h1>
            </div>
            <form>
              <div>
                <div className="form-group form-group-reset text-left">
                  <input
                    type="email"
                    className="form-control"
                    id="email"
                    aria-describedby="emailHelp"
                    placeholder={fieldLabels.email}
                    value={state.email}
                    onChange={handleChange}
                  />
                </div>
                <div className="btn-1-grid">
                  <button
                    type="submit"
                    className="btn btn-1 form-group form-group-reset"
                    onClick={handleSubmitClick}
                  >
                    {fieldLabels.reset}
                  </button>
                  <button
                    type="submit"
                    className="btn btn-1 form-group form-group-reset"
                    onClick={() => redirectToLogin()}
                  >
                    {fieldLabels.login}
                  </button>
                </div>
              </div>
            </form>
          </div>
          <Snackbar open={state.showSnackbar} onClose={handleClose} autoHideDuration={5000}>
            <Alert severity={state.snackbarType}>{state.snackbarMessage}</Alert>
          </Snackbar>
        </>
      }
      handleClose={() => {
        redirectToHome();
      }}
    />
  );
}

ResetPasswordForm.propTypes = {
  setPage: PropTypes.func,
};

export default ResetPasswordForm;
