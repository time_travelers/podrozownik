import axios from "axios";

export function pushResetData(payload, onSuccess, onError) {
  axios
    .post("/users/reset-password/", payload)
    .then(response => {
      onSuccess(response);
    })
    .catch(error => {
      onError(error);
    });
}
