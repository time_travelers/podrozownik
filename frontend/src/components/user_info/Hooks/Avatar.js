import React from "react";
import Grid from "@material-ui/core/Grid";
import Avatar from "@material-ui/core/Avatar";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import PropTypes from "prop-types";
import { userInfoMessages } from "../../../constants/messages";

const useStyles = makeStyles(() => ({
  gridStyle: {
    marginBottom: "30px",
  },
  avatarPhoto: {
    width: "163px",
    height: "163px",
    display: "flex",
    margin: "auto",
    borderRadius: "50%",
    boxShadow: "2px 2px 7px 0px black",
  },
  button: {
    display: "flex",
    margin: "10px auto 10px",
    width: "90%",
    maxWidth: "380px",
    marginTop: "11px",
  },
}));

function AvatarGrid(props) {
  const classes = useStyles(props);
  const { firstName, link } = props;

  function handleEditProfile() {
    props.setEditable();
  }

  return (
    <Grid item xs={12} md={6} className={classes.gridStyle}>
      <Avatar alt={firstName} src={link} className={classes.avatarPhoto} />
      <Button variant="outlined" className={classes.button} color="primary">
        {userInfoMessages.infoView.uploadPhotoButton}
      </Button>
      <Button
        variant="outlined"
        className={classes.button}
        onClick={handleEditProfile}
        color="primary"
      >
        {userInfoMessages.infoView.editProfileButton}
      </Button>
    </Grid>
  );
}

AvatarGrid.propTypes = {
  firstName: PropTypes.string,
  link: PropTypes.string,
  setEditable: PropTypes.func,
};

export default AvatarGrid;
