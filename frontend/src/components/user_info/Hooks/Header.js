import React from "react";
import PropTypes from "prop-types";
import { Typography, makeStyles } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  title: {
    textAlign: "center",
    padding: "11px 40px 11px 40px",
    [theme.breakpoints.down("sm")]: {
      fontSize: "1.27em",
    },
    [theme.breakpoints.up("sm")]: {
      fontSize: "1.7em",
    },
    [theme.breakpoints.up("md")]: {
      fontSize: "2em",
    },
  },
}));

function HeaderContainer(props) {
  const classes = useStyles(props);
  return <Typography className={classes.title}>{props.title}</Typography>;
}

HeaderContainer.propTypes = {
  title: PropTypes.string,
};

export default HeaderContainer;
