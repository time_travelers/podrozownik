import React from "react";
import TextField from "@material-ui/core/TextField";
import { makeStyles } from "@material-ui/core/styles";
import { userInfoMessages } from "../../../constants/messages";
import PropTypes from "prop-types";

const useStyles = makeStyles(theme => ({
  field: {
    width: "90%",
    maxWidth: "380px",
    marginTop: "11px",
  },
  cssLabel: {
    color: theme.palette.primary.main,
  },
  notchedOutline: {
    borderWidth: "1px !important",
    borderColor: `${theme.palette.primary.main} !important`,
    boxShadow: `3px 3px ${theme.palette.primary.shadow}`,
  },
}));

function Field(props) {
  const { editable, fieldName, fieldContent, type } = props;
  const classes = useStyles(props);
  const autoComplete = props.autoComplete || "off";

  const { fieldLabels } = userInfoMessages;
  return (
    <TextField
      label={fieldLabels[fieldName]}
      variant="outlined"
      className={props.className}
      type={type || ""}
      InputLabelProps={{
        shrink: fieldContent ? true : false,
        className: classes.cssLabel,
      }}
      InputProps={{
        readOnly: !editable,
        autoComplete: autoComplete,
        classes: {
          root: classes.cssLabel,
          notchedOutline: classes.notchedOutline,
        },
      }}
      defaultValue={fieldContent}
      fullWidth
    />
  );
}

Field.propTypes = {
  fieldName: PropTypes.string,
  fieldContent: PropTypes.string,
  type: PropTypes.string,
  autoComplete: PropTypes.string,
  className: PropTypes.string,
  editable: PropTypes.bool,
};

export default Field;
