import React from "react";
import PropTypes from "prop-types";
import Grid from "@material-ui/core/Grid";

import Field from "./Field";

function FormContainer(props) {
  const fields = ["email", "username", "firstName", "lastName"];

  return (
    <Grid item xs={12} md={6}>
      {fields.map((field, index) => (
        <Field
          key={index}
          editable={props.editable}
          fieldName={field}
          fieldContent={props[field]}
        />
      ))}
    </Grid>
  );
}

FormContainer.propTypes = {
  editable: PropTypes.bool,
};

export default FormContainer;
