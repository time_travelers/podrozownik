import React, { Component } from "react";
import PropTypes from "prop-types";
import Header from "../Hooks/Header";
import { Button, Grid, withStyles, Snackbar } from "@material-ui/core";
import MuiAlert from "@material-ui/lab/Alert";
import fieldProps from "../../../constants/fieldProps";

import { userInfoMessages as messages } from "../../../constants/messages";
import { Form, Field } from "react-final-form";
import { TextField } from "final-form-material-ui";
import { sendNewPassword } from "../Api";
import styles from "./FormStyles";

/**
 * Messages and lebels got from outer file
 */
const { editPasswordView, editProfileView, fieldLabels } = messages;

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

class EditPassword extends Component {
  constructor(props) {
    super(props);
    this.state = { mounted: false, submitting: false };
    this.updatePassword = this.updatePassword.bind(this);
    this.validate = this.validate.bind(this);
    this.handleCloseError = this.handleCloseError.bind(this);
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({ mounted: true });
    }, 250);
  }

  updatePassword(values) {
    this.setState(state => ({ ...state, submitting: true }));
    const newPasswordForm = {
      oldPassword: values["oldPassword"],
      password1: values["password1"],
      password2: values["password2"],
    };

    if (newPasswordForm.password1 !== newPasswordForm.password2) {
      this.setState(state => ({
        ...state,
        showSnackbar: true,
        snackbarType: "error",
        snackbarMessage: editProfileView.messages.passwordDiffers,
        submitting: false,
      }));
      return;
    }

    sendNewPassword(
      {
        old_password: values["oldPassword"],
        new_password: values["password1"],
      },
      () =>
        this.setState(state => ({
          ...state,
          showSnackbar: true,
          snackbarType: "success",
          snackbarMessage: editProfileView.messages.success,
          submitting: false,
        })),
      error =>
        this.setState(state => ({
          ...state,
          showSnackbar: true,
          snackbarType: "error",
          snackbarMessage: `HTTP Error: ${error.response}`,
          submitting: false,
        }))
    );
  }

  validate(values) {
    const errors = {};
    ["oldPassword", "password1", "password2"].forEach(fieldName => {
      const props = fieldProps["password"];
      const value = values[fieldName] || "";

      if (!value) {
        errors[fieldName] = messages.validation.required;
      } else if (value.length < props.minLength) {
        errors[fieldName] = messages.validation.tooShort;
      } else if (value.length > props.maxLength) {
        errors[fieldName] = messages.validation.tooLong;
      } else if (!props.isValid(value)) {
        errors[fieldName] = messages.validation.password;
      }
    });
    return errors;
  }

  handleCloseError() {
    this.setState(state => ({ ...state, showSnackbar: false }));
  }

  render() {
    const { classes } = this.props;

    function getClass(willUnmount, mounted) {
      if (willUnmount) {
        return classes.hide;
      } else if (!mounted) {
        return classes.init;
      } else {
        return classes.show;
      }
    }

    const fieldProps = {
      fullWidth: true,
      variant: "outlined",
      component: TextField,
      InputLabelProps: {
        shrink: true,
        className: classes.cssLabel,
      },
      InputProps: {
        classes: {
          root: classes.cssLabel,
          notchedOutline: classes.notchedOutline,
        },
      },
    };

    return (
      <div className={getClass(this.props.willUnmount, this.state.mounted)}>
        <Header title={editPasswordView.header} />
        <Form
          onSubmit={this.updatePassword}
          initialValues={this.props.userId}
          validate={this.validate}
          render={({ handleSubmit }) => (
            <form onSubmit={handleSubmit} noValidate>
              <Grid container className={classes.form}>
                <Grid item className={classes.buttonWidth}>
                  <Field
                    name="oldPassword"
                    type="password"
                    label={fieldLabels.oldPassword}
                    required
                    InputProps={{ autoComplete: "current-password" }}
                    {...fieldProps}
                  />
                </Grid>
                <Grid item className={classes.buttonWidth}>
                  <Field
                    name="password1"
                    type="password"
                    label={fieldLabels.newPassword}
                    required
                    InputProps={{ autoComplete: "new-password" }}
                    {...fieldProps}
                  />
                </Grid>
                <Grid item className={classes.buttonWidth}>
                  <Field
                    name="password2"
                    type="password"
                    label={fieldLabels.repeatNew}
                    required
                    InputProps={{ autoComplete: "new-password" }}
                    {...fieldProps}
                  />
                </Grid>
                <Grid item className={classes.buttonWidth}>
                  <Button
                    variant="outlined"
                    type="submit"
                    color="primary"
                    fullWidth
                    className={classes.buttonStyle}
                  >
                    {editPasswordView.saveDataButton}
                  </Button>
                </Grid>
              </Grid>
            </form>
          )}
        />
        <Snackbar
          open={this.state.showSnackbar}
          onClose={this.handleCloseError}
          autoHideDuration={4000}
        >
          <Alert severity={this.state.snackbarType}>{this.state.snackbarMessage}</Alert>
        </Snackbar>
      </div>
    );
  }
}

EditPassword.propTypes = {
  userId: PropTypes.number,
  editPassword: PropTypes.func,
  classes: PropTypes.object,
  willUnmount: PropTypes.bool,
};

export default withStyles(styles)(EditPassword);
