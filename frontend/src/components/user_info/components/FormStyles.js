const styles = theme => ({
  form: {
    minWidth: "50%",
    maxWidth: "90%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    margin: "auto",
  },

  formField: {
    display: "contents",
  },

  field: {
    width: "90%",
    maxWidth: "380px",
    marginTop: "11px",
  },

  cssLabel: {
    color: theme.palette.primary.main,
  },

  notchedOutline: {
    borderWidth: "1px !important",
    borderColor: `${theme.palette.primary.main} !important`,
    boxShadow: `3px 3px ${theme.palette.primary.shadow}`,
  },

  buttonWidth: {
    width: "90%",
    maxWidth: "380px",
    marginTop: "11px",
  },

  show: {
    opacity: "1",
    transition: "opacity .25s",
  },

  hide: {
    opacity: "0",
    transition: "opacity .25s",
  },

  init: {
    opacity: "0",
  },
});

export default styles;
