import React, { Component } from "react";
import { Grid, withStyles } from "@material-ui/core";
import AvatarGrid from "../Hooks/Avatar";
import PropTypes from "prop-types";
import Header from "../Hooks/Header";
import { userInfoMessages } from "../../../constants/messages";
import Field from "../Hooks/Field";

const styles = theme => ({
  form: {
    flexDirection: "column",
    alignItems: "center",
    display: "flex",
  },
  outline: {
    borderColor: theme.palette.primary.main,
    boxShadow: `3px 3px ${theme.palette.primary.shadow}`,
  },
  input: {
    color: theme.palette.primary.main,
  },
  buttonWidth: {
    width: "90%",
    [theme.breakpoints.down("md")]: {
      maxWidth: "380px",
    },
    "&:not(:first-child) ": {
      marginTop: "11px",
    },
  },

  show: {
    opacity: "1",
    transition: "opacity .25s",
  },

  hide: {
    opacity: "0",
    transition: "opacity .25s",
  },

  init: {
    opacity: "0",
  },
});

const { header } = userInfoMessages.infoView;

const fields = ["email", "username", "firstName", "lastName"];

class DataInfo extends Component {
  constructor(props) {
    super(props);
    this.state = { mounted: false };
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({ mounted: true });
    }, 250);
  }

  render() {
    const { classes } = this.props;
    const { firstName, link } = this.props.userData;

    function getClass(willUnmount, mounted) {
      if (willUnmount) {
        return classes.hide;
      } else if (!mounted) {
        return classes.init;
      } else {
        return classes.show;
      }
    }

    return (
      <div className={getClass(this.props.willUnmount, this.state.mounted)}>
        <Header title={header} />
        <Grid container spacing={1} wrap="wrap-reverse">
          <Grid item xs={12} md={6} className={classes.form}>
            {fields.map((field, index) => (
              <Field
                key={index}
                className={classes.buttonWidth}
                editable={false}
                fieldName={field}
                fieldContent={this.props.userData[field]}
              />
            ))}
          </Grid>
          <AvatarGrid
            firstName={firstName}
            link={link}
            setEditable={this.props.editData}
          />
        </Grid>
      </div>
    );
  }
}

DataInfo.propTypes = {
  userData: PropTypes.shape({
    email: PropTypes.string,
    username: PropTypes.string,
    firstName: PropTypes.string,
    lastName: PropTypes.string,
    link: PropTypes.string,
  }),
  editData: PropTypes.func,
  classes: PropTypes.object,
  willUnmount: PropTypes.bool,
};

export default withStyles(styles)(DataInfo);
