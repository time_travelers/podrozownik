import React, { Component } from "react";
import PropTypes from "prop-types";
import HeaderContainer from "../Hooks/Header";
import { Button, Grid, withStyles, Snackbar } from "@material-ui/core";
import MuiAlert from "@material-ui/lab/Alert";
import fieldProps from "../../../constants/fieldProps";

import { userInfoMessages as messages } from "../../../constants/messages";
import { Form, Field } from "react-final-form";
import { TextField } from "final-form-material-ui";
import { sendUserData } from "../Api";
import styles from "./FormStyles";

/**
 * Messages and lebels got from outer file
 */
const { editProfileView, fieldLabels } = messages;

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

class EditInfo extends Component {
  constructor(props) {
    super(props);
    this.state = { mounted: false, submitting: false };
    this.handleEditPassword = this.handleEditPassword.bind(this);
    this.updateUserData = this.updateUserData.bind(this);
    this.validate = this.validate.bind(this);
    this.handleCloseError = this.handleCloseError.bind(this);
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({ mounted: true });
    }, 250);
  }

  async handleEditPassword() {
    await this.props.editPassword();
  }

  updateUserData(values) {
    this.setState(state => ({ ...state, submitting: true }));

    const newUser = { email: values["email"], username: values["username"] };
    newUser["firstName"] = values["firstName"] || "";
    newUser["lastName"] = values["lastName"] || "";

    if (!Object.keys(newUser).some(key => this.props.userData[key] !== values[key])) {
      this.setState(state => ({
        ...state,
        showSnackbar: true,
        snackbarType: "error",
        snackbarMessage: editProfileView.messages.noChangesError,
        submitting: false,
      }));
      return;
    }

    sendUserData(
      values.id,
      newUser,
      () => {
        this.setState(state => ({
          ...state,
          showSnackbar: true,
          snackbarType: "success",
          snackbarMessage: editProfileView.messages.success,
          submitting: false,
        }));
        this.props.updateItems(newUser);
      },
      error =>
        this.setState(state => ({
          ...state,
          showSnackbar: true,
          snackbarType: "error",
          snackbarMessage: `HTTP Error: ${error.response}`,
          submitting: false,
        }))
    );
  }

  validate(values) {
    const errors = {};
    ["email", "username", "firstName", "lastName"].forEach(fieldName => {
      const props = fieldProps[fieldName];
      const value = values[fieldName] || "";

      if (props.required && !value) {
        errors[fieldName] = messages.validation.required;
      } else if (props.minLength !== null && value.length < props.minLength) {
        errors[fieldName] = messages.validation.tooShort;
      } else if (props.maxLength !== null && value.length > props.maxLength) {
        errors[fieldName] = messages.validation.tooLong;
      } else if (!props.isValid(value)) {
        errors[fieldName] = messages.validation[fieldName];
      }
    });
    return errors;
  }

  handleCloseError() {
    this.setState(state => ({ ...state, showSnackbar: false }));
  }

  render() {
    const { classes } = this.props;

    function getClass(willUnmount, mounted) {
      if (willUnmount) {
        return classes.hide;
      } else if (!mounted) {
        return classes.init;
      } else {
        return classes.show;
      }
    }

    const fieldProps = {
      fullWidth: true,
      variant: "outlined",
      component: TextField,
      InputLabelProps: {
        shrink: true,
        className: classes.cssLabel,
      },
      InputProps: {
        classes: {
          root: classes.cssLabel,
          notchedOutline: classes.notchedOutline,
        },
      },
    };

    return (
      <div className={getClass(this.props.willUnmount, this.state.mounted)}>
        <HeaderContainer title={editProfileView.header} />
        <Form
          onSubmit={this.updateUserData}
          initialValues={this.props.userData}
          validate={this.validate}
          render={({ handleSubmit }) => (
            <form onSubmit={handleSubmit} noValidate>
              <Grid container className={classes.form}>
                <Grid item className={classes.buttonWidth}>
                  <Field
                    name="email"
                    type="email"
                    label={fieldLabels.email}
                    required
                    InputProps={{ autoComplete: "email" }}
                    {...fieldProps}
                  />
                </Grid>
                <Grid item className={classes.buttonWidth}>
                  <Field
                    name="username"
                    type="text"
                    label={fieldLabels.username}
                    required
                    {...fieldProps}
                  />
                </Grid>
                <Grid item className={classes.buttonWidth}>
                  <Field
                    name="firstName"
                    type="text"
                    label={fieldLabels.firstName}
                    InputProps={{ autoComplete: "fname" }}
                    {...fieldProps}
                  />
                </Grid>
                <Grid item className={classes.buttonWidth}>
                  <Field
                    name="lastName"
                    type="text"
                    label={fieldLabels.lastName}
                    InputProps={{ autoComplete: "lname" }}
                    {...fieldProps}
                  />
                </Grid>

                <Grid item style={{ marginTop: 16 }} className={classes.buttonWidth}>
                  <Button
                    variant="outlined"
                    color="primary"
                    type="submit"
                    disabled={this.state.submitting}
                    fullWidth
                  >
                    {editProfileView.saveDataButton}
                  </Button>
                </Grid>
                <Grid item className={classes.buttonWidth}>
                  <Button
                    variant="outlined"
                    className={classes.buttonStyle}
                    onClick={this.handleEditPassword}
                    color="primary"
                    fullWidth
                  >
                    {editProfileView.changePasswordButton}
                  </Button>
                </Grid>
              </Grid>
            </form>
          )}
        />
        <Snackbar
          open={this.state.showSnackbar}
          onClose={this.handleCloseError}
          autoHideDuration={4000}
        >
          <Alert severity={this.state.snackbarType}>{this.state.snackbarMessage}</Alert>
        </Snackbar>
      </div>
    );
  }
}

EditInfo.propTypes = {
  userData: PropTypes.shape({
    email: PropTypes.string,
    username: PropTypes.string,
    firstName: PropTypes.string,
    lastName: PropTypes.string,
    avatar_link: PropTypes.string,
  }),
  editPassword: PropTypes.func,
  classes: PropTypes.object,
  updateItems: PropTypes.func,
  willUnmount: PropTypes.bool,
};

export default withStyles(styles)(EditInfo);
