import React from "react";
import { shallow } from "enzyme";
import toJson from "enzyme-to-json";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import axios from "axios";
import { ACCESS_TOKEN_NAME } from "../../constants/apiConstants";

import UserInfo from "./UserInfo";

const getUser = {
  data: {
    id: 0,
    username: "Prosacco",
    firstName: "Zachary",
    lastName: "Rippin",
    email: "Larue29@yahoo.com",
    user_permissions: "user",
    avatar_link:
      "https://gravatar.com/avatar/96ad06562a3c2c7175786a63c9c0c3fc?s=400&d=robohash&r=x",
    userStatus: 0,
  },
};

jest.mock("axios");

describe("User info tests", () => {
  let container = null;
  beforeAll(() => {
    localStorage.setItem(ACCESS_TOKEN_NAME, JSON.stringify({ token: 1, id: 0 }));
  });

  beforeEach(() => {
    container = document.createElement("div");
    document.body.appendChild(container);
  });

  afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
  });

  it("test if navbar components render properly", async () => {
    axios.get.mockImplementationOnce(() => Promise.resolve(getUser));
    await act(async () => render(<UserInfo userId={0} />, container));

    const userModel = getUser.data;
    const fields = document.querySelectorAll("input");
    expect(fields[0].value).toBe(userModel.email);
    expect(fields[1].value).toBe(userModel.username);
    expect(fields[2].value).toBe(userModel.firstName);
    expect(fields[3].value).toBe(userModel.lastName);

    expect(document.querySelector("img").src).toContain(userModel.avatar_link);
  });

  it("UserInfo snapshots", () => {
    const tree = shallow(<UserInfo userId={0} />);
    expect(toJson(tree)).toMatchSnapshot();
  });
});
