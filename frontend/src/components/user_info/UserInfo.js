import { CircularProgress, IconButton, Grid, withStyles } from "@material-ui/core";
import PropTypes from "prop-types";
import React, { Component } from "react";

import ErrorTile from "../ErrorTile";
import DataInfo from "./components/DataInfo";
import EditInfo from "./components/EditInfo";
import EditPasswordGrid from "./components/EditPassword";
import { userInfoMessages } from "../../constants/messages";
import { styles } from "./UserInfoStyles";
import { fetchUserData } from "./Api";

import ArrowBackIcon from "@material-ui/icons/ArrowBack";

class UserInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: {},
      current_state: "",
      willUnmount: false,
    };
    this.setPage = props.setPage;
    this.fetchData = this.fetchData.bind(this);
    this.setViewState = this.setViewState.bind(this);
    this.handleExit = this.handleExit.bind(this);
    this.updateItems = this.updateItems.bind(this);
  }

  componentDidMount() {
    this.fetchData();
  }

  updateItems(newItems) {
    if (this.state.items !== newItems) {
      this.setState({ ...this.state, items: { ...this.state.items, ...newItems } });
    }
  }

  render() {
    const { classes } = this.props;
    const errorMessages = userInfoMessages.errorMessages.parseError;
    const { current_state } = this.state;

    return (
      <>
        <div className={classes.spacing}></div>
        <Grid container className={classes.tile}>
          <Grid item xs={1} md={2}></Grid>
          <Grid item xs={10} md={8} className={classes.paper}>
            <IconButton className={classes.buttonBack} onClick={this.handleExit} color="primary">
              <ArrowBackIcon />
            </IconButton>
            <div className={classes.wraper}>
              {current_state === "loading" && (
                <div className={classes.progres}>
                  <CircularProgress className={classes.circle} />
                </div>
              )}

              {current_state === "error" && (
                <ErrorTile
                  title={errorMessages.title}
                  message={errorMessages.message}
                  buttonMessage={errorMessages.buttonMessage}
                  onExit={this.fetchData}
                />
              )}

              {current_state === "infoView" && (
                <DataInfo
                  userData={this.state.items}
                  willUnmount={this.state.willUnmount}
                  editData={() => this.setViewState("editInfo")}
                />
              )}

              {current_state === "editInfo" && (
                <EditInfo
                  userData={this.state.items}
                  updateItems={this.updateItems}
                  willUnmount={this.state.willUnmount}
                  editPassword={() => this.setViewState("editPass")}
                />
              )}

              {current_state === "editPass" && (
                <EditPasswordGrid userId={this.props.userId} willUnmount={this.state.willUnmount} />
              )}
            </div>
          </Grid>
          <Grid item xs={1} md={2}></Grid>
        </Grid>
        <div className={classes.spacing}></div>
      </>
    );
  }

  fetchData() {
    fetchUserData(
      this.props.userId,
      userData =>
        this.setState({
          current_state: "infoView",
          items: userData,
        }),
      error =>
        this.setState({
          current_state: "error",
          message: error,
        })
    );
  }

  setViewState(newState) {
    this.setState(state => ({
      ...state,
      current_state: newState,
      willUnmount: false,
    }));
  }

  handleExit() {
    if (this.state.current_state === "editInfo") {
      this.setViewState("infoView");
    } else if (this.state.current_state === "editPass") {
      this.setViewState("editInfo");
    } else {
      this.setPage("home");
    }
  }
}

UserInfo.propTypes = {
  userId: PropTypes.number.isRequired,
  setPage: PropTypes.func,
  classes: PropTypes.object,
};

export default withStyles(styles)(UserInfo);
