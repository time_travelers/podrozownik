export const styles = theme => ({
  tile: {
    minHeight: "calc(100vh - 99px)", // 100vh - Height of navbar
  },

  paper: {
    minHeight: "50%",
    minWidth: "275px",
    boxSizing: "border-box",

    borderRadius: "20px",
    backgroundColor: "#5B5B5B",
    boxShadow: "5px 5px #00000030",
    padding: "30px",
    color: "white",
    marginTop: "max(calc(25vh - 200px), 20px);",
    marginBottom: "max(calc(25vh - 200px), 40px);",

    [theme.breakpoints.down("lg")]: {
      marginTop: "max(calc(25vh - 200px), 100px);",
      marginBottom: "max(calc(50vh - 200px), 40px);",
    },
  },

  spacing: {
    display: "block",
  },

  wraper: {
    display: "flex",
    flexDirection: "column",
    height: "100%",
    justifyContent: "space-evenly",
  },

  progres: {
    position: "absolute",
    left: "50%",
    top: "50%",
    transform: "translate(-50px, -50px)",
  },

  circle: {
    width: "100px !important",
    height: "100px !important",
  },

  buttonBack: {
    position: "absolute",
    zIndex: "1",
  },

  show: {
    opacity: "1",
    transition: "opacity .5s",
  },

  hide: {
    opacity: "0",
    transition: "opacity .5s, height 0 .5s",
  },
});
