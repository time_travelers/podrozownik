import { ACCESS_TOKEN_NAME } from "../../constants/apiConstants";
import axios from "axios";

const getHeaders = () => {
  const access_token = JSON.parse(localStorage.getItem(ACCESS_TOKEN_NAME));
  return {
    headers: {
      Authorization: `Token ${access_token.token}`,
    },
  };
};

export function fetchUserData(userId, onSuccess, onError) {
  axios
    .get(`/users/${userId}`, getHeaders())
    .then(response => {
      onSuccess(response.data);
    })
    .catch(error => {
      onError(error);
    });
}

export function sendUserData(userId, newUser, onSuccess, onError) {
  axios
    .put(`/users/${userId}`, newUser, getHeaders())
    .then(response => {
      onSuccess(response.data);
    })
    .catch(error => {
      onError(error);
    });
}

export function sendNewPassword(newPasswordForm, onSuccess, onError) {
  axios
    .put(`/users/change-password/`, newPasswordForm, getHeaders())
    .then(() => {
      onSuccess();
    })
    .catch(error => {
      onError(error);
    });
}
