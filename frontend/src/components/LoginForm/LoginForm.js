import React, { useState } from "react";
import "./LoginForm.css";
import { ACCESS_TOKEN_NAME } from "../../constants/apiConstants";
import { FaMotorcycle, FaAt, FaKey } from "react-icons/fa";
import PropTypes from "prop-types";
import Popup from "../Popup/Popup";
import { Snackbar } from "@material-ui/core";
import MuiAlert from "@material-ui/lab/Alert";
import { LoginFormMessages as messages } from "../../constants/messages";
import { pushLoginData } from "./Api";

/**
 * Messages and lebels got from outer file
 */
const { popup, fieldLabels, infoMessages } = messages;

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function LoginForm(props) {
  const [state, setState] = useState({
    username: "",
    password: "",
    showSnackbar: false,
    snackbarType: "error",
    snackbarMessage: "test",
  });
  const toggleAlert = (type, message) => {
    setState(prevState => ({
      ...prevState,
      showSnackbar: true,
      snackbarType: type,
      snackbarMessage: message,
    }));
  };
  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setState(prevState => ({ ...prevState, showSnackbar: false }));
  };
  const handleChange = e => {
    const { id, value } = e.target;
    setState(prevState => ({
      ...prevState,
      [id]: value,
    }));
  };
  const handleSubmitClick = e => {
    e.preventDefault();
    if (state.username.length && state.password.length) {
      const payload = {
        username: state.username,
        password: state.password,
      };
      pushLoginData(
        payload,
        response => {
          if (response.status === 200) {
            toggleAlert("success", infoMessages.success);
            localStorage.setItem(ACCESS_TOKEN_NAME, JSON.stringify(response.data));
            redirectToHome();
          } else if (response.code === 204) {
            toggleAlert("error", infoMessages.error204);
          } else {
            toggleAlert("error", infoMessages.error);
          }
        },
        () => {
          toggleAlert("error", infoMessages.errorNetwork);
        }
      );
    } else {
      toggleAlert("warning", infoMessages.emptyfields);
    }
  };
  const redirectToHome = () => {
    setTimeout(() => {
      props.setPage("home");
    }, 500);
  };
  const redirectToRegister = () => {
    props.setPage("register");
  };
  const redirectToReset = () => {
    props.setPage("reset-password");
  };

  return (
    <Popup
      content={
        <>
          <div className="card login-card hv-center">
            <div className="title-card">
              <h1>{popup.title}</h1> <FaMotorcycle></FaMotorcycle>
            </div>
            <form>
              <div className="form-group">
                <label htmlFor="exampleInputUsername">
                  <FaAt></FaAt>
                </label>
                <input
                  type="username"
                  className="form-control"
                  id="username"
                  aria-describedby="usernameHelp"
                  placeholder={fieldLabels.username}
                  value={state.username}
                  onChange={handleChange}
                />
              </div>
              <div className="form-group text-left">
                <label htmlFor="exampleInputPassword1">
                  <FaKey></FaKey>
                </label>
                <input
                  type="password"
                  className="form-control"
                  id="password"
                  placeholder={fieldLabels.password}
                  value={state.password}
                  onChange={handleChange}
                />
              </div>
              <div className="btn-1-grid">
                <button type="submit" className="btn btn-1 form-group" onClick={handleSubmitClick}>
                  {fieldLabels.signin}
                </button>
              </div>
            </form>
            <div className="btn-2-grid">
              <button
                type="submit"
                className="btn btn-1 form-group-2"
                onClick={() => redirectToRegister()}
              >
                {fieldLabels.signup}
              </button>
              <button
                type="submit"
                className="btn btn-1 form-group-2"
                onClick={() => redirectToReset()}
              >
                {fieldLabels.forgotpassword}
              </button>
            </div>
          </div>
          <Snackbar open={state.showSnackbar} onClose={handleClose} autoHideDuration={5000}>
            <Alert severity={state.snackbarType}>{state.snackbarMessage}</Alert>
          </Snackbar>
        </>
      }
      handleClose={() => {
        redirectToHome();
      }}
    />
  );
}

LoginForm.propTypes = {
  setPage: PropTypes.func,
};

export default LoginForm;
