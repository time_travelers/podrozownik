import axios from "axios";

export function pushLoginData(payload, onSuccess, onError) {
  axios
    .post("/users/login/", payload)
    .then(response => {
      onSuccess(response);
    })
    .catch(error => {
      onError(error);
    });
}
