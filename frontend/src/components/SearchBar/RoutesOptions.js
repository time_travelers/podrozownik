import React from "react";
import PropTypes from "prop-types";
import { Grid, withStyles, Paper, Divider, Button } from "@material-ui/core";
import { Form } from "react-final-form";
import MarkField from "./Hooks/MarkField";
import DifficultyField from "./Hooks/DifficultyField";
import TimeField from "./Hooks/TimeField";

const styles = theme => ({
  root: {
    color: theme.palette.primary.main,
  },
  advancedOptions: {
    backgroundColor: theme.palette.background.routeTile,
    marginTop: "10px",
    padding: "12px",
  },
});

function RoutesOptions(props) {
  const { classes } = props;

  const updateSearchProps = (searchPropName, value) => {
    props.setSearchProps(previousProperties => {
      const newProps = { ...previousProperties };
      if (value) newProps[searchPropName] = value;
      return newProps;
    });
  };

  return (
    <Form
      onSubmit={props.search}
      onBlur={props.onBlur}
      initialValues={props.searchProps}
      render={({ handleSubmit }) => (
        <form onSubmit={handleSubmit} noValidate>
          <Paper className={classes.advancedOptions}>
            <Grid container spacing={2} justify="center" alignItems="center">
              <MarkField values={props.searchProps} update={updateSearchProps} />
              <DifficultyField values={props.searchProps} update={updateSearchProps} />
              <Divider />
              <TimeField values={props.searchProps} update={updateSearchProps} />
              <Grid item xs={10} sm={7} className={classes.root}>
                <Button variant="outlined" color="primary" fullWidth type="submit">
                  Szukaj
                </Button>
              </Grid>
            </Grid>
          </Paper>
        </form>
      )}
    />
  );
}

RoutesOptions.propTypes = {
  classes: PropTypes.object,
  searchProps: PropTypes.object,
  setSearchProps: PropTypes.func,
  onBlur: PropTypes.func,
  search: PropTypes.func,
};

export default withStyles(styles)(RoutesOptions);
