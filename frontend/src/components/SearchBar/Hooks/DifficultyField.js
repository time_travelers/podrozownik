import React from "react";
import PropTypes from "prop-types";
import { Typography, Grid, withStyles } from "@material-ui/core";
import Rating from "@material-ui/lab/Rating";
import { Field } from "react-final-form";
import { SearchBarMessages } from "../../../constants/messages";
import ReportProblemIcon from "@material-ui/icons/ReportProblem";
import styles from "./FieldStyles";

function DifficultyField(props) {
  const { classes, update, values } = props;

  const DifficultyChooser = () => (
    <Rating
      name="size-medium-diff"
      value={values.difficulty}
      icon={<ReportProblemIcon fontSize="inherit" />}
      onChange={(event, newValue) => {
        update("difficulty", newValue);
      }}
    />
  );

  return (
    <Grid item xs={12} sm={8} className={classes.root}>
      <Typography component="legend">{SearchBarMessages.difficultyFieldLabel}</Typography>
      <Field name="difficulty" component={DifficultyChooser} />
    </Grid>
  );
}

DifficultyField.propTypes = {
  classes: PropTypes.object,
  values: PropTypes.object,
  update: PropTypes.func,
};

export default withStyles(styles)(DifficultyField);
