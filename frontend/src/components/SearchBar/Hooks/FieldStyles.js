export default theme => ({
  root: {
    color: theme.palette.primary.main,
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-around",
  },
  timeFieldRow: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  legend: {
    color: theme.palette.primary.subtitle,
    fontWeight: "700",
  },
  numberInput: {
    color: theme.palette.primary.main,
    maxWidth: "120px",
  },
});
