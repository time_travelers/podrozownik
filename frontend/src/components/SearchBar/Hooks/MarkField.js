import React from "react";
import PropTypes from "prop-types";
import { Typography, Grid, withStyles } from "@material-ui/core";
import Rating from "@material-ui/lab/Rating";
import { Field } from "react-final-form";
import { SearchBarMessages } from "../../../constants/messages";
import styles from "./FieldStyles";

function MarkField(props) {
  const { classes, update, values } = props;

  const MarkChooser = () => (
    <Rating
      name="size-medium-mark"
      value={values.mark}
      onChange={(event, newValue) => {
        update("mark", newValue);
      }}
    />
  );

  return (
    <Grid item xs={12} sm={8} className={classes.root}>
      <Typography component="legend">{SearchBarMessages.markFieldLabel}</Typography>
      <Field name="mark" component={MarkChooser} />
    </Grid>
  );
}

MarkField.propTypes = {
  classes: PropTypes.object,
  update: PropTypes.func,
  values: PropTypes.object,
};

export default withStyles(styles)(MarkField);
