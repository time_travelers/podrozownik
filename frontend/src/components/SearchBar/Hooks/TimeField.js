import React from "react";
import PropTypes from "prop-types";
import { Typography, Grid, withStyles } from "@material-ui/core";
import { SearchBarMessages } from "../../../constants/messages";
import styles from "./FieldStyles";
import DurationInput from "./DurationInput";

function TimeField(props) {
  const { classes, update, values } = props;

  return (
    <Grid item xs={10} sm={6} className={classes.root}>
      <Grid container direction="column">
        <Grid item>
          <Typography component="legend" className={classes.legend}>
            {SearchBarMessages.timeField.label}
          </Typography>
        </Grid>
        <Grid item className={classes.timeFieldRow}>
          <Typography component="legend">{SearchBarMessages.timeField.from}</Typography>
          <DurationInput
            updateDuration={newDuration => update("timeFrom", newDuration)}
            duration={values.timeFrom}
          />
        </Grid>
        <Grid item className={classes.timeFieldRow}>
          <Typography component="legend">{SearchBarMessages.timeField.to}</Typography>
          <DurationInput
            updateDuration={newDuration => update("timeTo", newDuration)}
            duration={values.timeTo}
          />
        </Grid>
      </Grid>
      <div></div>
    </Grid>
  );
}

TimeField.propTypes = {
  classes: PropTypes.object,
  update: PropTypes.func,
  values: PropTypes.object,
};

export default withStyles(styles)(TimeField);
