import React, { useState } from "react";
import PropTypes from "prop-types";
import { Typography, Grid, withStyles } from "@material-ui/core";

const styles = {
  numberInput: {
    width: "100%",
    backgroundColor: "grey",
    border: "none",
    borderRadius: "3px",
    textAlign: "center",
    outline: "none",
    color: "white",

    boxShadow: "1px 1px 5px 0px #3e3e3e",
  },
  inputContainer: {
    maxWidth: "120px",
  },
};

function DurationInput(props) {
  const { classes, updateDuration, duration } = props;
  const [inputFocused, setInputFocused] = useState("");

  function setNewDuration(field, newValue) {
    const value = newValue === "" ? "" : parseInt(newValue);
    const newDuration = {
      d: field == "d" ? value : duration.d,
      h: field == "h" ? value : duration.h,
    };
    updateDuration(newDuration);
  }

  const unfocus = (field, name) => {
    setInputFocused("");
    field.target.value === "" && setNewDuration(name, 0);
  };
  return (
    <Grid container className={classes.inputContainer} justify="space-between" alignItems="center">
      <Grid item xs={4}>
        <input
          type="number"
          min={0}
          max={100}
          step={1}
          value={duration.d}
          className={classes.numberInput}
          style={{
            border: inputFocused == "days" ? "1px solid #b7b7b7" : "none",
          }}
          onChange={value => setNewDuration("d", value.target.value)}
          onFocus={() => setInputFocused("days")}
          onBlur={field => unfocus(field, "d")}
        />
      </Grid>
      <Grid item xs={1}>
        <Typography>d</Typography>
      </Grid>
      <Grid item xs={4}>
        <input
          type="number"
          min={0}
          max={23}
          step={1}
          name="hours"
          value={duration.h}
          className={classes.numberInput}
          style={{
            border: inputFocused == "hours" ? "1px solid #b7b7b7" : "none",
          }}
          onChange={value => setNewDuration("h", value.target.value)}
          onFocus={() => setInputFocused("hours")}
          onBlur={field => unfocus(field, "h")}
        />
      </Grid>
      <Grid item xs={1}>
        <Typography>h</Typography>
      </Grid>
    </Grid>
  );
}

DurationInput.propTypes = {
  classes: PropTypes.object,
  updateDuration: PropTypes.func,
  duration: PropTypes.shape({
    d: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    h: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  }),
};

export default withStyles(styles)(DurationInput);
