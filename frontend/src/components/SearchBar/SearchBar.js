import React, { useEffect, useRef, useState } from "react";
import PropTypes from "prop-types";
import { Collapse, Divider, IconButton, InputBase, withStyles } from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
import LocationSearchingIcon from "@material-ui/icons/LocationSearching";
import { SearchBarMessages } from "../../constants/messages";
import RoutesOptions from "./RoutesOptions";

const styles = theme => ({
  root: {
    width: "100%",
    padding: "2px 4px",
    display: "flex",
    alignItems: "center",
    borderRadius: "5px",
    backgroundColor: theme.palette.background.routeTile,
  },

  locationIcon: {
    color: theme.palette.primary.main,
    margin: "0 5px 0 15px",
    [theme.breakpoints.down("xs")]: {
      display: "none",
    },
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
    "&> input": {
      color: theme.palette.primary.main,
      [theme.breakpoints.down("xs")]: {
        fontSize: "13px",
      },
    },
  },
  iconButton: {
    padding: 10,
  },
  divider: {
    height: 28,
    margin: 4,
  },
  advancedBtn: {
    minWidth: "auto",
  },
  advancedBtnLabel: {
    font: "12px normal",
    fontFamily: "system-ui, Montserrat, Helvetica, Arial, sans-serif",
    width: "auto",

    [theme.breakpoints.down("xs")]: {
      display: "none",
    },
  },

  advancedOptions: {
    backgroundColor: theme.palette.background.routeTile,
    marginTop: "10px",
    height: "200px",
    width: "100%",
    padding: "12px",
  },
});

function SearchBar(props) {
  const { classes } = props;
  const [focused, setFocused] = useState(false);
  const [searchProps, setSearchProps] = useState({
    tickets: "",
    mark: 1,
    difficulty: 4,
    timeFrom: { d: 0, h: 0 },
    timeTo: { d: 31, h: 0 },
  });

  function focusSearch() {
    props.setRoutesVisible(false);
    setFocused(true);
  }

  function blurSearch() {
    props.setRoutesVisible(true);
    setFocused(false);
  }

  function handleSearch() {
    const newRouteList = props.allRoutes
      .filter(route => {
        // Filter by ticket
        let matches = false;
        const routeJson = JSON.stringify(route).toLowerCase();
        let ticketRegex;
        searchProps.tickets
          .replace(/[.,]/g, "")
          .toLowerCase()
          .split(" ")
          .forEach(ticket => {
            ticketRegex = new RegExp(ticket);
            matches = matches || ticket == "" || ticketRegex.test(routeJson);
          });
        return matches;
      })
      .filter(({ features }) => {
        let matches = true;
        const { mark, difficulty, timeFrom, timeTo } = searchProps;
        matches = matches && features.mark >= mark;
        matches = matches && features.difficulty <= difficulty;

        // Parse duration
        const durationProps = features.duration
          .replace(/ /g, "") // Remove spaces
          .split(/d|days|h|hours|min|minutes/)
          .map(dur => parseInt(dur));

        const hoursFrom = timeFrom.d * 24 + timeFrom.h;
        const hoursTo = timeTo.d * 24 + timeTo.h;
        matches = matches && durationProps[0] >= hoursFrom && durationProps[0] <= hoursTo;

        return matches;
      });
    props.setRoutes(newRouteList);
    blurSearch();
  }

  const searchBarRef = useRef(null);
  addBlurEffect(searchBarRef, handleSearch, blurSearch);

  return (
    <div style={{ width: "100%" }} ref={searchBarRef}>
      <div className={classes.root}>
        <LocationSearchingIcon className={classes.locationIcon} />
        <InputBase
          className={classes.input}
          placeholder={SearchBarMessages.placeholder}
          inputProps={{ "aria-label": "search route" }}
          value={searchProps.tickets}
          onChange={field =>
            setSearchProps(prevstate => ({
              ...prevstate,
              tickets: field.target.value,
            }))
          }
          onFocus={focusSearch}
        />
        <Divider className={classes.divider} orientation="vertical" />
        <IconButton
          type="submit"
          color="primary"
          className={classes.iconButton}
          aria-label="search"
        >
          <SearchIcon />
        </IconButton>
      </div>

      <Collapse in={focused} style={{ transitionDelay: focused ? "150ms" : "0ms" }}>
        <RoutesOptions
          searchProps={searchProps}
          setSearchProps={setSearchProps}
          search={handleSearch}
        />
      </Collapse>
    </div>
  );
}

function addBlurEffect(searchBarRef, handleSearch, blurSearch) {
  useEffect(() => {
    function handleClickOutside(event) {
      if (searchBarRef.current && !searchBarRef.current.contains(event.target)) {
        blurSearch();
      }
    }

    // Bind the event listener
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      // Unbind the event listener on clean up
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [searchBarRef]);

  const handleBlurOrSearchKey = event => {
    // Enter
    if (event.keyCode === 13) {
      event.preventDefault();
      document.activeElement.blur();
      handleSearch();
    }

    // Escape
    if (event.keyCode === 27) {
      event.preventDefault();
      document.activeElement.blur();
      blurSearch();
    }
  };

  useEffect(() => {
    window.addEventListener("keydown", handleBlurOrSearchKey);
    return () => {
      window.removeEventListener("keydown", handleBlurOrSearchKey);
    };
  }, [handleBlurOrSearchKey]);
}

SearchBar.propTypes = {
  setPage: PropTypes.func,
  classes: PropTypes.object,
  allRoutes: PropTypes.array,
  setRoutes: PropTypes.func,
  setRoutesVisible: PropTypes.func,
};

export default withStyles(styles)(SearchBar);
