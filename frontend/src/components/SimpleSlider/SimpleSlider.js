import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core";
import AwesomeSlider from "react-awesome-slider";
import withAutoplay from "react-awesome-slider/dist/autoplay";
import "./Slider.module.css";

const useStyles = makeStyles(() => ({
  slider: {
    width: "96%",
    margin: "auto",
    height: "auto",
  },
  sliderDiv: {
    maxWidth: "900px",
    margin: "20px auto 50px",
  },
}));

function SimpleSlider(props) {
  const featuredRoutes = props.featured;
  const classes = useStyles(props);

  const AutoplaySlider = withAutoplay(AwesomeSlider);
  const images = featuredRoutes.map((item, index) => ({
    id: index,
    url: item.photos[0].link,
  }));

  return (
    <div className={classes.sliderDiv}>
      <AutoplaySlider
        play={true}
        cancelOnInteraction={false}
        interval={3000}
        buttons={true}
        mobileTouch={true}
        infinite={true}
        className={classes.slider}
      >
        {images.map(item => (
          <div key={item.id} data-src={item.url} />
        ))}
      </AutoplaySlider>
    </div>
  );
}

SimpleSlider.propTypes = {
  featured: PropTypes.array,
};

export default SimpleSlider;
