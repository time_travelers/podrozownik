import React from "react";
import App from "../../App";
import Navbar from "./Navbar";
import { shallow, mount } from "enzyme";
import toJson from "enzyme-to-json";

//render test
describe("rendering components", () => {
  it("test if app components render properly", () => {
    shallow(<App></App>);
  });

  it("test if navbar components render properly", () => {
    shallow(<Navbar />);
  });

  it("includes one h1 tag", () => {
    const wrapper = shallow(<Navbar />);

    expect(wrapper.find("h1").length).toEqual(1);
  });

  it("includes one nav tag ", () => {
    const wrapper = shallow(<Navbar />);
    expect(wrapper.find("nav").length).toEqual(1);
  });

  it("check if props passes to navbar", () => {
    const wrapper = mount(<Navbar test1="a" test2="b" test3="c" />);

    expect(wrapper.prop("test1")).toEqual("a");
    expect(wrapper.prop("test2")).toEqual("b");
    expect(wrapper.prop("test3")).toEqual("c");
  });
});

describe("snapshots", () => {
  it("Navbar snapshots", () => {
    const tree = shallow(<Navbar />);
    expect(toJson(tree)).toMatchSnapshot();
  });
});
