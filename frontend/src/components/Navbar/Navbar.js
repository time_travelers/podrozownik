import React, { useState, useEffect } from "react";
import "./Navbar.css";
import PropTypes from "prop-types";
import Dropdown from "./Dropdown";
import DropdownMobile from "./DropdownMobile";
import { fetchUserData } from "./Api";
import { Redirect } from "react-router-dom";
import { ACCESS_TOKEN_NAME } from "../../constants/apiConstants";

function Navbar(props) {
  const [width, setWidth] = useState(window.innerWidth);
  const [state, setState] = useState(false);
  const [errorMsg, setErrorMsg] = useState(false);
  const [userData, setUserData] = useState("");
  const [redirect, setRedirect] = useState(false);
  const [avatarLink, setAvatarLink] = useState(false);
  const navbarType = props.type;

  useEffect(() => {
    if (navbarType === "login") {
      const loggedUser = JSON.parse(localStorage.getItem(ACCESS_TOKEN_NAME));
      fetchUserData(
        loggedUser.id,
        data => {
          setAvatarLink(data.link);
          setUserData(data.username);
          setErrorMsg(false);
        },
        error => {
          setErrorMsg(error);
        }
      );
    }
  }, []);

  const updateWidth = () => {
    setWidth(window.innerWidth);
  };

  useEffect(() => {
    window.addEventListener("resize", updateWidth);
    return () => window.removeEventListener("resize", updateWidth);
  });

  if (redirect) {
    return <Redirect push to="/" />;
  }

  return (
    <nav className="NavbarItems">
      <h1
        className="navbar-logo"
        onClick={() => (navbarType === "reset" ? setRedirect(true) : props.setPage("home"))}
      >
        Podróżownik
      </h1>

      {(navbarType === "login" || navbarType === "logout") && (
        <div
          className="menu-icon"
          data-test="menu-icon-test"
          id="menu-icon-id"
          onClick={() => setState(!state)}
        >
          <i className={state ? "fas fa-times" : "fas fa-bars"}> </i>
        </div>
      )}

      <ul className={state ? "nav-menu active" : "nav-menu"}>
        {width <= 960 && (
          <DropdownMobile
            type={navbarType}
            setPage={props.setPage}
            errorMsg={errorMsg}
            personalData={userData}
            link={avatarLink ? avatarLink : ""}
            state={setState}
          />
        )}

        {width > 960 && (
          <Dropdown
            type={navbarType}
            setPage={props.setPage}
            personalData={userData}
            link={avatarLink ? avatarLink : ""}
            errorMsg={errorMsg}
          />
        )}
      </ul>
    </nav>
  );
}

Navbar.propTypes = {
  type: PropTypes.string,
  setPage: PropTypes.func,
};

export default Navbar;
