import axios from "axios";
import { ACCESS_TOKEN_NAME } from "../../constants/apiConstants.js";

export function fetchUserData(userId, onSuccess, onError) {
  const token = JSON.parse(localStorage.getItem(ACCESS_TOKEN_NAME));

  axios
    .get(`/users/${userId}`, {
      headers: {
        Authorization: `Token ${token.token}`,
      },
    })
    .then(response => {
      onSuccess(response.data);
    })
    .catch(() => {
      onError(true);
    });
}

export function logoutUser(onSuccess, onError) {
  axios.post(`/users/logout/`).then(onSuccess).catch(onError);
}
