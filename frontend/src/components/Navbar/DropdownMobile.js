import Button from "@material-ui/core/Button";
import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import Paper from "@material-ui/core/Paper";
import { red } from "@material-ui/core/colors";
import { useTheme } from "@material-ui/core";
import { ThemeProvider } from "@material-ui/styles";
import { MenuItems } from "./MenuItems";
import PropTypes from "prop-types";
import ErrorIcon from "@material-ui/icons/Error";
import { NavbarMessages } from "../../constants/messages";

const useStyles = makeStyles({
  root: {
    marginTop: "40px",
    position: "relative",
    width: "100%",
    background: "linear-gradient(90deg, rgb(218, 255, 118) 0%, rgb(149, 207, 12) 100%)",
    right: "0",
    borderRadius: "10px",
  },
  avatarLarge: {
    marginTop: "10px",
    position: "relative",
    backgroundColor: red[500],
    width: "80px",
    height: "80px",
  },
  avatarSmall: {
    marginTop: "10px",
    position: "relative",
    backgroundColor: red[500],
    width: "60px",
    height: "60px",
  },
  rowContainerSmall: {
    display: "flex",
    padding: "0px 10% 10px 5%",
  },
  dropButton: {
    marginBottom: "20px",
    height: "50px",
    width: "100%",
  },
  rowContainer: {
    display: "flex",
    padding: "5px 10px 15px 15px",
  },
  fullNameLarge: {
    marginTop: "10%",
    marginLeft: "20px",
    fontSize: "22px",
    wordBreak: "break-all",
  },
  errorMsg: {
    margin: "10% 10% 10% 10%",
    fontSize: "17px",
    width: "100%",
    textAlign: "center",
    wordBreak: "break-word",
  },
  fullNameSmall: {
    marginTop: "30px",
    marginLeft: "20px",
    fontSize: "16px",
    wordBreak: "break-all",
  },
  errorIcon: {
    position: "relative",
    top: "5px",
  },
  paper: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    background: "transparent",
    marginTop: "0px",
  },
  buttonMobile: {
    padding: "auto",
    height: "50px",
    width: "100%",
    "&:first-child": {
      marginTop: "20px",
    },
    marginBottom: "20px",
  },
});

function Dropdown(props) {
  const classes = useStyles();
  const [width, setWidth] = useState(window.innerWidth);

  const theme = useTheme();

  const updateWidth = () => {
    setWidth(window.innerWidth);
  };

  React.useEffect(() => {
    window.addEventListener("resize", updateWidth);
    return () => window.removeEventListener("resize", updateWidth);
  });

  const messages = NavbarMessages.infoMessages;

  return (
    <ThemeProvider theme={theme}>
      {props.type == "login" && !props.errorMsg && (
        <>
          <Container maxWidth="xs">
            <div className={classes.root}>
              <div className={width < 400 ? classes.rowContainerSmall : classes.rowContainer}>
                <Avatar
                  src={props.link}
                  className={width < 400 ? classes.avatarSmall : classes.avatarLarge}
                >
                  {get_initials(props.personalData)}
                </Avatar>

                <Typography className={width < 400 ? classes.fullNameSmall : classes.fullNameLarge}>
                  {props.personalData}
                </Typography>
              </div>
            </div>
          </Container>
          <Container maxWidth="xs">
            <Paper className={classes.paper}>
              {MenuItems.map((item, index) => {
                if (item.condition(true) && item.viewMain) {
                  return (
                    <Button
                      key={index}
                      className={classes.buttonMobile}
                      variant="contained"
                      color="secondary"
                      onClick={() => {
                        item.action(props.setPage);
                        props.state(false);
                      }}
                    >
                      {item.title}
                    </Button>
                  );
                }
              })}
            </Paper>
          </Container>
        </>
      )}
      {props.type == "logout" && (
        <Container maxWidth="xs">
          <Paper className={classes.paper}>
            {MenuItems.map((item, index) => {
              if (item.condition(false) && item.viewMain) {
                return (
                  <Button
                    key={index}
                    className={classes.buttonMobile}
                    variant="contained"
                    color="secondary"
                    onClick={() => {
                      item.action(props.setPage);
                      props.state(false);
                    }}
                  >
                    {item.title}
                  </Button>
                );
              }
            })}
          </Paper>
        </Container>
      )}

      {props.errorMsg && props.type == "login" && (
        <>
          <Container maxWidth="xs">
            <div className={classes.root}>
              <p className={width < 400 ? classes.rowContainerSmall : classes.rowContainer}>
                <Typography className={classes.errorMsg}>
                  <ErrorIcon className={classes.errorIcon}></ErrorIcon>
                  {messages.fetchError}
                </Typography>
              </p>
            </div>
          </Container>
        </>
      )}
    </ThemeProvider>
  );
}

function get_initials(str) {
  let acronym = str.split(/\s/).reduce((response, word) => (response += word.slice(0, 1)), "");
  return acronym;
}

Dropdown.propTypes = {
  personalData: PropTypes.string,
  type: PropTypes.string,
  setPage: PropTypes.func,
  state: PropTypes.func,
  errorMsg: PropTypes.bool,
  link: PropTypes.string,
};

export default Dropdown;
