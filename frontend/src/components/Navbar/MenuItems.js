import { ACCESS_TOKEN_NAME } from "../../constants/apiConstants";

export const MenuItems = [
  {
    title: "Strona główna",
    condition: () => true,
    action: setPage => setPage("home"),
    cName: "nav-links",
    viewMain: false,
  },
  {
    title: "O użytkowniku",
    condition: isLoggedIn => (isLoggedIn ? true : false),
    action: setPage => setPage("userinfo"),
    cName: "nav-links-mobile",
    viewMain: true,
  },
  {
    title: "Zaloguj",
    condition: isLoggedIn => (isLoggedIn ? false : true),
    action: setPage => setPage("login"),
    cName: "nav-links-mobile",
    viewMain: true,
  },
  {
    title: "Wyloguj",
    condition: isLoggedIn => (isLoggedIn ? true : false),
    action: setPage => {
      localStorage.removeItem(ACCESS_TOKEN_NAME);
      setPage("home");
    },
    cName: "nav-links-mobile",
    viewMain: true,
  },
  {
    title: "Zarejestruj",
    condition: isLoggedIn => (isLoggedIn ? false : true),
    action: setPage => setPage("register"),
    cName: "nav-links-mobile",
    viewMain: true,
  },
];
