import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import clsx from "clsx";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import Collapse from "@material-ui/core/Collapse";
import Avatar from "@material-ui/core/Avatar";
import IconButton from "@material-ui/core/IconButton";
import { red } from "@material-ui/core/colors";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import { useTheme } from "@material-ui/core";
import { ThemeProvider } from "@material-ui/styles";
import { MenuItems } from "./MenuItems";
import PropTypes from "prop-types";
import ErrorIcon from "@material-ui/icons/Error";
import { NavbarMessages } from "../../constants/messages";

const useStyles = makeStyles(theme => ({
  root: {
    margin: "16px 10px 16px 10px",
    position: "relavtive",
    width: "230px",
    background: "transparent",
    right: "0",
    boxShadow: "0 3px 5px 2px gray ",
  },
  expand: {
    transform: "rotate(0deg)",
    marginLeft: "auto",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: "rotate(180deg)",
  },
  avatar: {
    backgroundColor: red[500],
    marginBottom: "10px",
    height: "35px",
    width: "35px",
    fontSize: "15px",
  },
  dropButton: {
    margin: "10px 10px 10px 10px",
    position: "center",
    width: "calc(100% - 20px)",
  },
  content: {
    position: "absolute",
    zIndex: "10",
    width: "230px",
    right: 0,
    marginRight: "67px",
    top: "60px",
  },
  header: {
    padding: "5px 0px 5px 10px",
  },
  fullName: {
    textAlign: "center",
    fontSize: "15px",
    marginBottom: "5%",
    margin: "auto",
    overflow: "hidden",
  },
  errorMsg: {
    textAlign: "center",
    fontSize: "15px",
    marginRight: "15px",
  },
  buttonBackground: {
    background: "linear-gradient(90deg, rgb(166 219 39) 0%, rgb(149, 207, 12) 100%)",
    width: "230px",
    boxShadow: "0 3px 5px 2px gray ",
  },
  link_buttons: {
    margin: "16px 10px 16px 10px",
    background: "transparent",
    borderRadius: 3,
    border: 0,
    fontWeight: "bolder",
    color: "black",
    height: "60%",
    width: "150px",
    boxShadow: "0 3px 5px 2px gray ",
  },
  errorIcon: {
    position: "relative",
    top: "5px",
  },
}));

function Dropdown(props) {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  const messages = NavbarMessages.infoMessages;
  const theme = useTheme();

  return (
    <ThemeProvider theme={theme}>
      {props.type == "login" && !props.errorMsg && (
        <Card className={classes.root}>
          <CardHeader
            className={classes.header}
            avatar={
              <Avatar src={props.link} className={classes.avatar}>
                {get_initials(props.personalData)}
              </Avatar>
            }
            action={
              <>
                <IconButton
                  className={clsx(classes.expand, {
                    [classes.expandOpen]: expanded,
                  })}
                  onClick={handleExpandClick}
                  aria-expanded={expanded}
                >
                  <ExpandMoreIcon />
                </IconButton>
              </>
            }
            title={<Typography className={classes.fullName}> {props.personalData}</Typography>}
          />

          <Collapse in={expanded} timeout="auto" unmountOnExit>
            <CardContent className={classes.content}>
              <Paper className={classes.buttonBackground}>
                {MenuItems.map((item, index) => {
                  if (item.condition(true) && item.viewMain) {
                    return (
                      <Button
                        key={index}
                        className={classes.dropButton}
                        variant="contained"
                        color="secondary"
                        onClick={() => item.action(props.setPage)}
                      >
                        {item.title}
                      </Button>
                    );
                  }
                })}
              </Paper>
            </CardContent>
          </Collapse>
        </Card>
      )}
      {props.type == "logout" && (
        <>
          {MenuItems.map((item, index) => {
            if (item.condition(false) && item.viewMain) {
              return (
                <Button
                  key={index}
                  variant="outlined"
                  className={classes.link_buttons}
                  onClick={() => item.action(props.setPage)}
                >
                  {item.title}
                </Button>
              );
            }
          })}
        </>
      )}
      {props.errorMsg && props.type == "login" && (
        <>
          <Card className={classes.root}>
            <CardHeader
              className={classes.header}
              title={
                <>
                  <Typography className={classes.errorMsg}>
                    <ErrorIcon className={classes.errorIcon}></ErrorIcon>
                    {messages.fetchError}
                  </Typography>
                </>
              }
            />
          </Card>
        </>
      )}
    </ThemeProvider>
  );
}

function get_initials(str) {
  let acronym = str.split(/\s/).reduce((response, word) => (response += word.slice(0, 1)), "");
  return acronym;
}

Dropdown.propTypes = {
  personalData: PropTypes.string,
  link: PropTypes.string,
  type: PropTypes.string,
  setPage: PropTypes.func,
  errorMsg: PropTypes.bool,
};
export default Dropdown;
