import React from "react";
import "./Footer.css";

class Footer extends React.Component {
  render() {
    return (
      <div className="footer-items">
        <h1 className="copyright">© copyrights podróżownik</h1>
      </div>
    );
  }
}

export default Footer;
