import axios from "axios";
import { ACCESS_TOKEN_NAME } from "../../constants/apiConstants.js";

export function fetchRouteData(routeId, onSuccess, onError) {
  axios
    .get(`/route/${routeId}`)
    .then(response => {
      const { data } = response;
      data.description = data.description
        .replaceAll("rgb(102, 102, 102);", "rgb(200, 200, 200);")
        .replaceAll("rgb(22, 22, 22);", "rgb(215, 215, 215);");
      onSuccess(data);
    })
    .catch(error => {
      onError(error.name + ":" + error.message);
    });
}

// export function fetchMapLink(routeId, onSuccess) {
//   const data =
//     '<iframe src="https://www.google.com/maps/embed?pb=!1m74!1m12!1m3!1d322818.71930057375!2d15.840416012645655!3d50.79371485942523!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m59!3e0!4m3!3m2!1d50.921502!2d16.395305999999998!4m5!1s0x470ef946c49ee2b7%3A0xc87a79cf05d86c0f!2zU8SZZHppc8WCYXc!3m2!1d50.8196339!2d16.0632694!4m5!1s0x470ee3c4c7044759%3A0x375a608f66de6e69!2sTrzci%C5%84sko!3m2!1d50.879007699999995!2d15.8719456!4m5!1s0x470ee138f3cacfe1%3A0x564f574949f21113!2s58-500%20Wojan%C3%B3w!3m2!1d50.871868799999994!2d15.8212814!4m5!1s0x470ee58e4cb3ddad%3A0x66101aa22bfbc5d9!2sKowary!3m2!1d50.791709999999995!2d15.836649999999999!4m5!1s0x470ef1094b748827%3A0xcde4e50671969fca!2sSzczepan%C3%B3w!3m2!1d50.6979699!2d15.9449809!4m5!1s0x470e5894d1ac609d%3A0xa69af7e90054f649!2zxYHEhWN6bmE!3m2!1d50.6689745!2d16.1333366!4m5!1s0x470e574d7d9fbf67%3A0x98dba0ff64c2c744!2zVW5pc8WCYXcgxZpsxIVza2k!3m2!1d50.7130972!2d16.2400551!4m5!1s0x470e52073e081cb5%3A0xcc4a4b900f67f1f4!2sZ%C5%82oty%20Las!3m2!1d50.780556!2d16.4155561!4m5!1s0x470fac6e7bf17df3%3A0x4530a20b2a9530da!2sPasieczna!3m2!1d50.9205256!2d16.3968474!5e0!3m2!1spl!2spl!4v1608673856533!5m2!1spl!2spl" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>';
//   onSuccess(data);
//   /*
//   axios
//     .get(`/route/${routeId}`)
//     .then(response => {
//       onSuccess(response.data);
//     })
//     .catch(error => {
//       onError(error.name + ":" + error.message);
//     });*/
// }

export function fetchUserData(userId, onSuccess, onError) {
  const token = JSON.parse(localStorage.getItem(ACCESS_TOKEN_NAME));

  axios
    .get(`/users/${userId}`, {
      headers: {
        Authorization: `Token ${token.token}`,
      },
    })
    .then(response => {
      onSuccess(response.data);
    })
    .catch(() => {
      onError(true);
    });
}

export function sendUserComment(data, onSuccess, onError) {
  const token = JSON.parse(localStorage.getItem(ACCESS_TOKEN_NAME));
  axios
    .post(`/mark/add/`, data, {
      headers: {
        Authorization: `Token ${token.token}`,
      },
    })
    .then(response => {
      onSuccess(response.data);
    })
    .catch(error => {
      onError(error.name + ":" + error.message);
    });
}
