import React, { useState, useEffect } from "react";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import {
  makeStyles,
  CircularProgress,
  useTheme,
  useMediaQuery,
  Container,
} from "@material-ui/core";
import AwesomeSlider from "react-awesome-slider";
import "react-awesome-slider/dist/styles.css";
import "react-awesome-slider/dist/custom-animations/cube-animation.css";
import TraceRatings from "./TraceRatings/TraceRatings";
import TraceDescription from "./TraceDescription/TraceDescription";
import PropTypes from "prop-types";
import { fetchRouteData } from "./Api";
import ErrorTile from "../ErrorTile";
import { ThemeProvider } from "@material-ui/styles";
import CommentField from "../CommentField/CommentField";

const useStyles = makeStyles({
  mainGrid: {
    width: "100%",
    display: "flex",
    margin: "0px",
    minWidth: "250px",
  },
  sliderGrid: {
    width: "100%",
    justifyContent: "center",
    padding: "20px",
  },
  grid: {
    width: "100%",
  },
  grid_column_left: {
    float: "left",
    width: "100%",
  },
  grid_column_right: {
    float: "right",
    width: "100%",
    "&:last-child": {
      marginBottom: "20px",
    },
  },
  paper: {
    background: "transparent",
    width: "100%",
  },
  paperSlider: {
    background: "transparent",
    width: "100%",
    height: "100%",
  },
  paperSliderMobile: {
    background: "transparent",
    width: "100%",
    height: "100%",
  },
  loaderBackground: {
    background: "rgb(68, 68, 67)",
    width: "100vw",
    height: "100vh",
  },
  photoProperties: {
    position: "relative",
    width: "100vw",
    height: "auto",
  },
  circular: {
    position: "absolute",
    left: "calc(50% - 20px)",
    top: "50%",
  },
  errorContainer: {
    position: "center",
  },
});

function split(arr) {
  var len = arr.length / 2;
  var chunks = [],
    i = 0,
    n = arr.length;
  while (i < n) {
    chunks.push(arr.slice(i, (i += len)));
  }
  return chunks;
}

function RouteView(props) {
  const classes = useStyles();

  const [traceData, setTraceData] = useState();
  const [state, setState] = useState("loading");
  const [errorMsg, setErrorMsg] = useState();
  const [comments, setComments] = useState([]);

  const theme = useTheme();

  const isLargeScreen = useMediaQuery(theme.breakpoints.up("md"));

  function getData() {
    setState("loading");
    fetchRouteData(
      props.routeId,
      data => {
        setTraceData(data);
        setErrorMsg("no error");
        setState("view");
        setComments(split(data.marks));
      },
      error => {
        setErrorMsg(error);
        setState("error");
      }
    );
  }
  useEffect(() => {
    getData();
  }, []);

  return (
    <ThemeProvider theme={theme}>
      {state == "error" && (
        <Paper className={classes.loaderBackground}>
          <ErrorTile
            title={"Error occured!"}
            message={errorMsg}
            buttonMessage={"try again."}
            onExit={getData}
          />
        </Paper>
      )}
      {state == "loading" && (
        <Paper className={classes.loaderBackground}>
          <CircularProgress className={classes.circular} />
        </Paper>
      )}

      {traceData && state == "view" && comments.length == 2 && (
        <Container maxWidth="lg">
          <Grid
            container
            className={classes.mainGrid}
            spacing={2}
            direction="row"
            maxwidth={"xl"}
            minwidth={"xs"}
          >
            <Grid item className={classes.sliderGrid}>
              <Grid container className={classes.sliderGrid}>
                <Grid item lg={1} className={classes.paper} />
                <Grid item md={12} xs={12} lg={10}>
                  <Paper className={classes.paper}>
                    <AwesomeSlider
                      animation="smoothLettering"
                      fillParent={false}
                      buttons={traceData.photos.length > 1}
                      className={isLargeScreen ? classes.paperSlider : classes.paperSliderMobile}
                    >
                      {traceData.photos.map(item => (
                        <div key={item.id} data-src={item.link} />
                      ))}
                    </AwesomeSlider>
                  </Paper>
                </Grid>
                <Grid item lg={1} className={classes.paper} />
              </Grid>
            </Grid>

            <Grid item className={classes.grid}>
              <Grid container>
                <Grid item lg={1} className={classes.paper} />
                <Grid item className={classes.grid_column_left} lg={10} xs={12} md={12}>
                  <TraceDescription
                    traceId={props.routeId}
                    traceName={traceData.title}
                    traceFeatures={traceData.features}
                    traceDescription={traceData.description}
                    setPage={props.setPage}
                  />

                  <div className="ratings-wrapper">
                    <Grid container spacing={isLargeScreen ? 2 : 0}>
                      <Grid item className={classes.grid_column_left} lg={6} md={6} xs={12}>
                        {props.userId ? (
                          <CommentField
                            setPage={getData}
                            route_id={traceData.id}
                            userId={props.userId}
                          />
                        ) : (
                          <></>
                        )}

                        {comments[0].map((item, index) => {
                          return (
                            <TraceRatings
                              key={index}
                              name={item.commenter}
                              link={item.link}
                              mark={item.mark}
                              comment={item.comment}
                              commentDate={item.date}
                            />
                          );
                        })}
                      </Grid>
                      <Grid item className={classes.grid_column_right} lg={6} md={6} xs={12}>
                        {comments[1].map((item, index) => {
                          return (
                            <TraceRatings
                              key={index}
                              name={item.commenter}
                              link={item.link}
                              mark={item.mark}
                              comment={item.comment}
                              commentDate={item.date}
                            />
                          );
                        })}
                      </Grid>
                    </Grid>
                  </div>
                </Grid>

                <Grid item className={classes.grid_column_right} lg={3} xs={12} md={4}></Grid>
                <Grid item lg={1} className={classes.paper} />
              </Grid>
            </Grid>
          </Grid>
        </Container>
      )}
    </ThemeProvider>
  );
}

RouteView.propTypes = {
  routeId: PropTypes.string,
  userId: PropTypes.number,
  setPage: PropTypes.func,
};

export default RouteView;
