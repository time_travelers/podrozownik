import React from "react";
import "./TraceDescription.css";
import Rating from "@material-ui/lab/Rating";
import TimerIcon from "@material-ui/icons/Timer";
import MotorcycleIcon from "@material-ui/icons/Motorcycle";
import ReportProblemIcon from "@material-ui/icons/ReportProblem";
import LocationOnIcon from "@material-ui/icons/LocationOn";
import ReactHtmlParser from "react-html-parser";
import { Button, useMediaQuery } from "@material-ui/core";
import PropTypes from "prop-types";
import RouteMap from "../../RouteMap/RouteMap";
import { TraceDescriptionMessages } from "../../../constants/messages";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";

function TraceDescription(props) {
  const state = {
    activityType: props.traceFeatures.type,
    traceLength: props.traceFeatures.length,
    traceTime: props.traceFeatures.duration,
    averageMark: props.traceFeatures.mark,
    difficultyLevel: props.traceFeatures.difficulty,
    location: props.traceFeatures.location,
    title: props.traceName,
    description: props.traceDescription,
    routeId: props.routeId,
    googleLink: props.traceFeatures.google_link,
  };
  const messages = TraceDescriptionMessages.infoMessages;
  const isBigScreen = useMediaQuery(theme => theme.breakpoints.up("lg"));

  return (
    <div className="description-contener">
      <div className="name-wrapper">
        <h1>
          <Button style={{ marginTop: "-10px" }} color="primary">
            <ArrowBackIcon onClick={() => props.setPage("home")} />
          </Button>
          {state.title}
        </h1>
      </div>

      <div className="features-wrapper">
        <div className="feature-block">
          <h1> {messages.activityType} </h1>
          <p>
            <MotorcycleIcon fontSize={isBigScreen ? "large" : "default"} />
            {state.activityType}
          </p>
        </div>

        <div className=" feature-block">
          <h1>{messages.time}</h1>
          <p>
            <TimerIcon fontSize={isBigScreen ? "large" : "small"} />
            {state.traceTime}
          </p>
        </div>

        <div className="feature-block">
          <h1> {messages.length}</h1>
          <p>
            <MotorcycleIcon fontSize={isBigScreen ? "large" : "default"} />
            {state.traceLength + "km"}
          </p>
        </div>

        <div className="feature-block">
          <h1>{messages.averageMark} </h1>
          <p>
            <Rating
              size={isBigScreen ? "large" : "small"}
              name="read-only"
              value={state.averageMark}
              readOnly
            />
          </p>
        </div>
        <div className="feature-block">
          <h1>{messages.difficulty}</h1>
          <p>
            <Rating
              size={isBigScreen ? "large" : "small"}
              name="read-only"
              value={state.difficultyLevel}
              readOnly
              icon={<ReportProblemIcon fontSize="inherit" />}
            />
          </p>
        </div>

        <div className="feature-block">
          <h1> {messages.location}</h1>
          <p>
            <LocationOnIcon fontSize={isBigScreen ? "large" : "small"} />
            {state.location}
          </p>
        </div>
      </div>
      <div className="map-container">
        {" "}
        <RouteMap googleLink={state.googleLink} />
      </div>

      <div className="description-wrapper">
        <h1>{messages.description}</h1>
        {ReactHtmlParser(state.description)}
      </div>
    </div>
  );
}

TraceDescription.propTypes = {
  traceFeatures: PropTypes.object,
  traceDescription: PropTypes.string,
  traceName: PropTypes.string,
  routeId: PropTypes.number,
  setPage: PropTypes.func,
};

export default TraceDescription;
