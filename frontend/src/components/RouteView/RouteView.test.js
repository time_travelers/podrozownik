import React from "react";
import { shallow, mount } from "enzyme";
import RouteView from "./RouteView";

describe("rendering components", () => {
  it("test if routeView components render properly", () => {
    shallow(<RouteView />);
  });

  it("props passing to RouteView test", () => {
    const wrapper = mount(<RouteView id={1} marks={[1, 2, 3, 4]} />);
    expect(wrapper.prop("id")).toEqual(1);
    expect(wrapper.prop("marks")).toHaveLength(4);
  });
});
