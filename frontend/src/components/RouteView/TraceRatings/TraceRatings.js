import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { useMediaQuery, useTheme } from "@material-ui/core";
import clsx from "clsx";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import Collapse from "@material-ui/core/Collapse";
import Avatar from "@material-ui/core/Avatar";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Typography from "@material-ui/core/Typography";
import Rating from "@material-ui/lab/Rating";
import PropTypes from "prop-types";
import { ThemeProvider } from "@material-ui/styles";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles(theme => ({
  root: {
    padding: "10px 10px 10px 10px",
    marginBottom: "20px",
    backgroundColor: "rgb(68, 68, 67)",
    color: "white",
  },
  grid: {
    width: "100%",
  },
  paper: {
    background: "rgb(68, 68, 67)",
  },
  media: {
    height: 0,
  },
  expand: {
    transform: "rotate(0deg)",
    marginLeft: "auto",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: "rotate(180deg)",
  },
  largeAvatar: {
    width: theme.spacing(7),
    height: theme.spacing(7),
  },
  typoCut: {
    margin: "10px 0 15px 15px",
    color: "rgb(199, 191, 191)",
    fontSize: "15px",
    overflow: "hidden",
    textOverflow: "ellipsis",
    //display: "-webkit-box",
    //"-webkit-line-clamp": 2 /* number of lines to show */,
    //"-webkit-box-orient": "vertical",
  },
  typoCutLarge: {
    margin: "10px 0 15px 15px",
    color: "rgb(199, 191, 191)",
    fontSize: "20px",
    overflow: "hidden",
    textOverflow: "ellipsis",
    //display: "-webkit-box",
    //"-webkit-line-clamp": 2 /* number of lines to show */,
    //"-webkit-box-orient": "vertical",
  },
  typo: {
    color: "rgb(199, 191, 191)",
    fontSize: "15px",
  },
  typoLarge: {
    color: "rgb(199, 191, 191)",
    fontSize: "20px",
  },
  date: {
    color: "rgb(199, 191, 191)",
    fontSize: "15px",
  },
  cardHeader: {
    fontSize: "50px",
    color: "rgb(199, 191, 191)",
  },
  hide: {
    visibility: "hidden",
  },
  rating: {
    marginLeft: "15px",
  },
  collapse: {
    transition: "width 2s, height 4s",
  },
}));
const textSize = 120;

function cutComment(comment) {
  if (comment.length > textSize) {
    return comment.substring(0, textSize) + " ...";
  } else {
    return comment;
  }
}

function get_initials(str) {
  let acronym = str.split(/\s/).reduce((response, word) => (response += word.slice(0, 1)), "");
  return acronym;
}

function RatingsCard(props) {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  const state = {
    fullName: props.name,
    mark: props.mark,
    link: props.link,
    comment: props.comment,
    commentDate: props.commentDate,
  };

  const handleExpandClick = () => {
    setTimeout(setExpanded(!expanded), 1000);
  };
  const theme = useTheme();
  const isBigScreen = useMediaQuery(theme => theme.breakpoints.up("lg"));

  return (
    <ThemeProvider theme={theme}>
      <Grid className={classes.grid}>
        <Card className={classes.root}>
          <CardHeader
            className={classes.cardHeader}
            avatar={
              <Avatar src={state.link} className={classes.largeAvatar}>
                {get_initials(state.fullName)}
              </Avatar>
            }
            action={
              <ExpandMoreIcon
                className={
                  state.comment && state.comment.length > textSize
                    ? clsx(classes.expand, {
                        [classes.expandOpen]: expanded,
                      })
                    : classes.hide
                }
                onClick={handleExpandClick}
                aria-expanded={expanded}
                aria-label="show more"
                color="action"
              >
                <i className={"fas fa-bars"}> </i>
              </ExpandMoreIcon>
            }
            title={
              <Typography
                className={isBigScreen ? classes.typoLarge : classes.typo}
                variant="body2"
                color="textSecondary"
                component="p"
              >
                {state.fullName}
              </Typography>
            }
            subheader={<Typography className={classes.date}> {state.commentDate}</Typography>}
          />

          <Rating
            className={classes.rating}
            size={isBigScreen ? "large" : "small"}
            defaultValue={state.mark}
            readOnly
          />

          {!expanded && state.comment.length >= textSize && (
            <Typography
              className={isBigScreen ? classes.typoCutLarge : classes.typoCut}
              color="textSecondary"
              component="p"
            >
              {cutComment(state.comment)}
            </Typography>
          )}
          {!expanded && state.comment.length < textSize && (
            <Typography
              className={isBigScreen ? classes.typoCutLarge : classes.typoCut}
              color="textSecondary"
              component="p"
            >
              {state.comment}
            </Typography>
          )}
          <Collapse className={classes.collapse} in={expanded} timeout="auto" unmountOnExit>
            <CardContent>
              <Typography
                className={isBigScreen ? classes.typoLarge : classes.typo}
                variant="body2"
                color="textSecondary"
                component="p"
              >
                {expanded && state.comment}
              </Typography>
            </CardContent>
          </Collapse>

          <Collapse
            className={classes.collapse}
            in={expanded}
            timeout="auto"
            unmountOnExit
          ></Collapse>
        </Card>
      </Grid>
    </ThemeProvider>
  );
}

RatingsCard.propTypes = {
  name: PropTypes.string,
  mark: PropTypes.number,
  comment: PropTypes.string,
  commentDate: PropTypes.string,
  link: PropTypes.string,
};

export default RatingsCard;
