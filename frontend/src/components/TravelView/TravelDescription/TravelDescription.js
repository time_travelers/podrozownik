import React, { useRef, useEffect } from "react";
import "./TravelDescription.css";
import TimerIcon from "@material-ui/icons/Timer";
import LaunchIcon from "@material-ui/icons/Launch";
import LocationOnIcon from "@material-ui/icons/LocationOn";
import ReactHtmlParser from "react-html-parser";
import { useMediaQuery } from "@material-ui/core";
import PropTypes from "prop-types";
import { TravelViewMessages as messages } from "../../../constants/messages";

const { description } = messages;

function TravelDescription(props) {
  const state = {
    activityType: props.travelFeatures.type,
    tripLength: props.travelFeatures.length,
    tripTime: props.travelFeatures.duration,
    tripName: props.travelFeatures.title,
    averageMark: props.travelFeatures.mark,
    difficultyLevel: props.travelFeatures.difficulty,
    location: props.travelFeatures.location,
    title: props.travelName,
    description: props.travelDescription,
    tags: props.tags,
    tripID: props.travelFeatures.id,
  };

  const isBigScreen = useMediaQuery(theme => theme.breakpoints.up("lg"));

  const routeDetailsclicker = useRef(null);
  addRouteOnClickEvent(routeDetailsclicker, () => props.setPage(`route#${state.tripID}`));

  return (
    <div className="description-contener">
      <div className="name-wrapper">
        <h1> {state.title} </h1>
      </div>

      <div className="features-wrapper">
        <div className="feature-block">
          <h1> {description.organizer} </h1>
          <p>{state.tags.organizer.username}</p>
        </div>

        <div className=" feature-block">
          <h1>{description.length}</h1>
          <p>
            <TimerIcon fontSize={isBigScreen ? "large" : "small"} />
            &nbsp;
            {state.tripLength}km
          </p>
        </div>

        <div className="feature-block">
          <h1> {description.date}</h1>
          <p>
            <TimerIcon fontSize={isBigScreen ? "large" : "small"} />
            &nbsp;
            {state.tags.travel_datetime.slice(0, 10)}
          </p>
        </div>
        <div className="feature-block">
          <h1> {description.meetingPoint}</h1>
          <p>
            <LocationOnIcon fontSize={isBigScreen ? "large" : "small"} />
            &nbsp;
            {state.tags.assembly_point}
          </p>
        </div>
        <div className="feature-block feature-block-2" ref={routeDetailsclicker}>
          <h1> {description.route}</h1>
          <p>
            {/* <LocationOnIcon fontSize={isBigScreen ? "large" : "small"} /> */}
            {state.tripName}
            &nbsp;
            <LaunchIcon fontSize={isBigScreen ? "large" : "small"} />
          </p>
        </div>
      </div>

      <div className="description-wrapper">
        <h1> {description.info} </h1>
        <p>{ReactHtmlParser(state.description)}</p>
      </div>
    </div>
  );
}

function addRouteOnClickEvent(routeDetailsclicker, callback) {
  useEffect(() => {
    function handleClickInside(event) {
      if (routeDetailsclicker.current && routeDetailsclicker.current.contains(event.target)) {
        callback();
      }
    }

    document.addEventListener("mousedown", handleClickInside);
    return () => {
      document.removeEventListener("mousedown", handleClickInside);
    };
  }, [routeDetailsclicker]);
}

TravelDescription.propTypes = {
  travelFeatures: PropTypes.object,
  travelDescription: PropTypes.string,
  travelName: PropTypes.string,
  tags: PropTypes.object,
  setPage: PropTypes.func,
};

export default TravelDescription;
