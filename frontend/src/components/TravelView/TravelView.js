import React, { useState, useEffect } from "react";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import {
  makeStyles,
  Button,
  CircularProgress,
  useTheme,
  useMediaQuery,
  Container,
} from "@material-ui/core";
import AwesomeSlider from "react-awesome-slider";
import "react-awesome-slider/dist/styles.css";
import "react-awesome-slider/dist/custom-animations/cube-animation.css";
import TravelDescription from "./TravelDescription/TravelDescription";
import ParticipantList from "./ParticipantList/ParticipantList";
import PropTypes from "prop-types";
import { fetchTravelData, postAssignData } from "./Api";
import ErrorTile from "../ErrorTile";
import { ThemeProvider } from "@material-ui/styles";
import { TravelViewMessages as messages } from "../../constants/messages";
import { ACCESS_TOKEN_NAME } from "../../constants/apiConstants.js";

const { fetchError, buttons, participant } = messages;

const useStyles = makeStyles(() => ({
  mainGrid: {
    width: "100%",
    display: "flex",
    margin: "0px",
    minWidth: "250px",
  },
  sliderGrid: {
    width: "100%",
    justifyContent: "center",
    padding: "20px",
  },
  grid: {
    width: "100%",
  },
  paper: {
    background: "transparent",
    width: "100%",
  },
  paperSlider: {
    background: "transparent",
    width: "100%",
    height: "100%",
  },
  paperSliderMobile: {
    background: "transparent",
    width: "100%",
    height: "100%",
  },
  loaderBackground: {
    background: "rgb(68, 68, 67)",
    width: "100vw",
    height: "100vh",
  },
  photoProperties: {
    position: "relative",
    width: "100vw",
    height: "auto",
  },
  circular: {
    position: "absolute",
    left: "calc(50% - 20px)",
    top: "50%",
  },
  errorContainer: {
    position: "center",
  },
  h2: {
    color: "white",
    fontSize: "1.5rem",
  },
  buttons: {
    "& button, a": {
      fontSize: "1.2rem",
    },
    paddingBottom: "30px",
  },
}));

function TravelView(props) {
  const classes = useStyles();

  const [travelData, setTravelData] = useState();
  const [state, setState] = useState("loading");
  const [errorMsg, setErrorMsg] = useState();
  const [comments, setComments] = useState([]);
  const [isAssigned, setIsAssigned] = useState([]);

  const theme = useTheme();

  const isLargeScreen = useMediaQuery(theme.breakpoints.up("md"));

  function getData() {
    setState("loading");
    if (!JSON.parse(localStorage.getItem(ACCESS_TOKEN_NAME))) {
      setErrorMsg(fetchError.notLogged);
      setState("notLogged");
    } else {
      fetchTravelData(
        props.travelId,
        data => {
          setTravelData(data);
          setErrorMsg("no error");
          setState("view");
          setComments(data.participants);
          setIsAssigned(data.assigned);
        },
        error => {
          setErrorMsg(error);
          setState("error");
        }
      );
    }
  }
  useEffect(() => {
    getData();
  }, []);

  function postAssign() {
    const data = {
      secret_id: props.travelId,
    };
    postAssignData(
      data,
      () => {
        getData();
      },
      error => {
        setErrorMsg(error);
        setState("error");
      }
    );
  }

  return (
    <ThemeProvider theme={theme}>
      {state === "error" && (
        <Paper className={classes.loaderBackground}>
          <ErrorTile
            title={fetchError.title}
            message={errorMsg}
            buttonMessage={fetchError.tryAgain}
            onExit={getData}
          />
        </Paper>
      )}
      {state === "notLogged" && (
        <Paper className={classes.loaderBackground}>
          <ErrorTile
            title={fetchError.title}
            message={errorMsg}
            buttonMessage={fetchError.homePage}
            onExit={() => props.setPage("home")}
          />
        </Paper>
      )}
      {state === "loading" && (
        <Paper className={classes.loaderBackground}>
          <CircularProgress className={classes.circular} />
        </Paper>
      )}

      {travelData && state === "view" && comments.length && (
        <Container maxWidth="lg">
          <Grid
            container
            className={classes.mainGrid}
            spacing={3}
            direction="column"
            alignItems="center"
            maxwidth={"xl"}
            minwidth={"xs"}
          >
            <Grid item className={classes.sliderGrid}>
              <Grid container className={classes.sliderGrid}>
                <Grid item lg={1} className={classes.paper} />
                <Grid item md={12} xs={12} lg={10}>
                  <Paper className={classes.paper}>
                    <AwesomeSlider
                      animation="smoothLettering"
                      fillParent={false}
                      buttons={travelData.photos.length > 1}
                      className={isLargeScreen ? classes.paperSlider : classes.paperSliderMobile}
                    >
                      {travelData.photos.map(item => (
                        <div key={item.id} data-src={item} />
                      ))}
                    </AwesomeSlider>
                  </Paper>
                </Grid>
                <Grid item lg={1} className={classes.paper} />
              </Grid>
            </Grid>

            <Grid item className={classes.grid}>
              <Grid container xs={12}>
                <Grid item lg={12} className={classes.paper} />
                <Grid item className={classes.grid_column_left} lg={12} xs={12} md={12}>
                  <TravelDescription
                    travelName={travelData.title}
                    travelFeatures={travelData.route}
                    travelDescription={travelData.description}
                    tags={travelData.tags}
                    setPage={props.setPage}
                  />
                </Grid>

                <Grid item className={classes.grid_column_right} lg={3} xs={12} md={4}></Grid>
                <Grid item lg={1} className={classes.paper} />
              </Grid>
            </Grid>

            <Grid item xs={12} md={10} className={classes.grid}>
              <Grid
                container
                alignItems="center"
                spacing={2}
                justify="space-around"
                className={classes.buttons}
              >
                {!isAssigned && (
                  <Grid item>
                    <Button variant="contained" onClick={() => postAssign()}>
                      {buttons.join}
                    </Button>
                  </Grid>
                )}
                {isAssigned && (
                  <Grid item>
                    <Button
                      variant="contained"
                      href={travelData.conversation_link}
                      target="_blank"
                      size="lg"
                    >
                      {buttons.whatsapp}
                    </Button>
                  </Grid>
                )}
                {isAssigned && (
                  <Grid item>
                    <Button variant="contained" size="lg" onClick={() => postAssign()}>
                      {buttons.leave}
                    </Button>
                  </Grid>
                )}
              </Grid>
            </Grid>

            <Grid item xs={12} md={11} className={classes.grid}>
              <h2 className={classes.h2}>{participant.participant}</h2>
              <Grid container spacing={2} justify="center">
                {comments.map((item, index) => {
                  return (
                    <Grid item md={4} xs={10} key={index} justify="space-around">
                      <ParticipantList participant={item} name={travelData.route.title} />
                    </Grid>
                  );
                })}
              </Grid>
            </Grid>
          </Grid>
        </Container>
      )}
    </ThemeProvider>
  );
}

TravelView.propTypes = {
  travelId: PropTypes.string,
  setPage: PropTypes.func,
};

export default TravelView;
