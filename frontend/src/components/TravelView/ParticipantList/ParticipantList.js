import React from "react";
import "./ParticipantList.css";
import PropTypes from "prop-types";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: "rgb(68, 68, 67)",
    color: "white",
  },
  contener: {
    background: "rgb(63, 62, 62)",
    borderRadius: "10px",
  },
  paper: {
    background: "rgb(68, 68, 67)",
  },
  media: {
    height: 0,
  },
  expand: {
    transform: "rotate(0deg)",
    marginLeft: "auto",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: "rotate(180deg)",
  },
  typo: {
    color: "rgb(199, 191, 191)",
    fontSize: "15px",
    [theme.breakpoints.up("lg")]: {
      fontSize: "20px",
    },
  },
  cardHeader: {
    color: "rgb(199, 191, 191)",
    fontSize: "50px",
  },
  hide: {
    visibility: "hidden",
  },
}));

function get_initials(str) {
  return str.split(/\s/).map(name => name.charAt(0));
}

function ParticipantList(props) {
  const classes = useStyles(props);
  const state = {
    title: props.tripName,
    participant: props.participant,
  };

  return (
    <div className={classes.contener}>
      <div className="name-wrapper">
        <h1> {state.title} </h1>
        <Card className={classes.root}>
          <CardHeader
            className={classes.cardHeader}
            avatar={
              <Avatar src={state.participant.avatar} className={classes.largeAvatar}>
                {get_initials(state.participant.username)}
              </Avatar>
            }
            title={
              <Typography
                className={classes.typo}
                variant="body2"
                color="textSecondary"
                component="p"
              >
                {state.participant.username}
              </Typography>
            }
          />
        </Card>
      </div>
    </div>
  );
}

ParticipantList.propTypes = {
  name: PropTypes.string,
  participant: PropTypes.array,
  tripName: PropTypes.string,
};

export default ParticipantList;
