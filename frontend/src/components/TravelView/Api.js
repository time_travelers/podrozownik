import axios from "axios";
import { ACCESS_TOKEN_NAME } from "../../constants/apiConstants.js";

export function fetchTravelData(travelID, onSuccess, onError) {
  const token = JSON.parse(localStorage.getItem(ACCESS_TOKEN_NAME));
  axios
    .get(`/travels/${travelID}`, {
      headers: {
        Authorization: `Token ${token.token}`,
      },
    })
    .then(response => {
      onSuccess(response.data);
    })
    .catch(error => {
      onError(error.name + ":" + error.message);
    });
}

export function postAssignData(data, onSuccess, onError) {
  const token = JSON.parse(localStorage.getItem(ACCESS_TOKEN_NAME));
  axios
    .post(`/travels/participate/`, data, {
      headers: {
        Authorization: `Token ${token.token}`,
      },
    })
    .then(response => {
      onSuccess(response.data);
    })
    .catch(error => {
      onError(error.name + ":" + error.message);
    });
}
