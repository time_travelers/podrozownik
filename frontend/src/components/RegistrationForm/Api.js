import axios from "axios";

export function pushRegisterData(payload, onSuccess, onError) {
  axios
    .post("/users/register/", payload)
    .then(response => {
      onSuccess(response);
    })
    .catch(error => {
      onError(error);
    });
}
