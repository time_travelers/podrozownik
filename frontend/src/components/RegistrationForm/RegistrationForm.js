import React, { useState } from "react";
import "./RegistrationForm.css";
import { FaAt, FaKey, FaUser } from "react-icons/fa";
import PropTypes from "prop-types";
import Popup from "../Popup/Popup";
import { Snackbar } from "@material-ui/core";
import MuiAlert from "@material-ui/lab/Alert";
import { RegisterFormMessages as messages } from "../../constants/messages";
import { pushRegisterData } from "./Api";

/**
 * Messages and lebels got from outer file
 */
const { popup, fieldLabels, infoMessages } = messages;

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function RegistrationForm(props) {
  const [state, setState] = useState({
    username: "",
    email: "",
    password: "",
    confirmPassword: "",
    showSnackbar: false,
    snackbarType: "error",
    snackbarMessage: "test",
  });
  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setState(prevState => ({ ...prevState, showSnackbar: false }));
  };
  const handleChange = e => {
    const { id, value } = e.target;
    setState(prevState => ({
      ...prevState,
      [id]: value,
    }));
  };
  const toggleAlert = (type, message) => {
    setState(prevState => ({
      ...prevState,
      showSnackbar: true,
      snackbarType: type,
      snackbarMessage: message,
    }));
  };
  const sendDetailsToServer = () => {
    if (state.username.length && state.email.length && state.password.length) {
      const payload = {
        username: state.username,
        email: state.email,
        password: state.password,
      };
      pushRegisterData(
        payload,
        response => {
          if (response.status === 201) {
            toggleAlert("success", infoMessages.success);
            setTimeout(() => {
              redirectToHome();
            }, 3000);
          } else {
            toggleAlert("error", infoMessages.error);
          }
        },
        () => {
          toggleAlert("error", infoMessages.errorNetwork);
        }
      );
    } else {
      toggleAlert("warning", infoMessages.emptyfileds);
    }
  };
  const redirectToHome = () => {
    props.setPage("home");
  };
  const redirectToLogin = () => {
    props.setPage("login");
  };
  const handleSubmitClick = e => {
    e.preventDefault();
    if (state.password === state.confirmPassword) {
      sendDetailsToServer();
    } else {
      toggleAlert("error", infoMessages.errorPasswords);
    }
  };
  return (
    <Popup
      content={
        <>
          <div className="card login-card hv-center">
            <div className="title-card-register">
              <h1>{popup.title}</h1>
            </div>
            <form>
              <div className="form-group text-left">
                <label htmlFor="exampleInputUsername">
                  <FaUser></FaUser>
                </label>
                <input
                  type="text"
                  className="form-control"
                  id="username"
                  aria-describedby="nameHelp"
                  placeholder={fieldLabels.username}
                  value={state.username}
                  onChange={handleChange}
                />
              </div>
              <div className="form-group text-left">
                <label htmlFor="exampleInputEmail1">
                  <FaAt></FaAt>
                </label>
                <input
                  type="email"
                  className="form-control"
                  id="email"
                  aria-describedby="emailHelp"
                  placeholder={fieldLabels.email}
                  value={state.email}
                  onChange={handleChange}
                />
              </div>
              <div className="form-group text-left">
                <label htmlFor="exampleInputPassword1">
                  <FaKey></FaKey>
                </label>
                <input
                  type="password"
                  className="form-control"
                  id="password"
                  placeholder={fieldLabels.password}
                  value={state.password}
                  onChange={handleChange}
                />
              </div>
              <div className="form-group text-left">
                <label htmlFor="exampleInputPassword1">
                  <FaKey></FaKey>
                </label>
                <input
                  type="password"
                  className="form-control"
                  id="confirmPassword"
                  placeholder={fieldLabels.password2}
                  value={state.confirmPassword}
                  onChange={handleChange}
                />
              </div>
              <div className="btn-1-grid">
                <button
                  type="submit"
                  className="btn btn-1 form-group btn-2-register"
                  onClick={handleSubmitClick}
                >
                  {fieldLabels.register}
                </button>
                <button
                  type="submit"
                  className="btn btn-1 form-group btn-2-register"
                  onClick={() => redirectToLogin()}
                >
                  {fieldLabels.login}
                </button>
              </div>
            </form>
          </div>
          <Snackbar open={state.showSnackbar} onClose={handleClose} autoHideDuration={5000}>
            <Alert severity={state.snackbarType}>{state.snackbarMessage}</Alert>
          </Snackbar>
        </>
      }
      handleClose={() => {
        redirectToHome();
      }}
    />
  );
}

RegistrationForm.propTypes = {
  setPage: PropTypes.func,
};

export default RegistrationForm;
