import React from "react";
import "./Popup.css";
import { FaTimes } from "react-icons/fa";
import PropTypes from "prop-types";

function Popup(props) {
  return (
    <div className="popup-box">
      <div className="box">
        <span className="close-icon" onClick={props.handleClose}>
          <FaTimes></FaTimes>
        </span>
        {props.content}
      </div>
    </div>
  );
}

Popup.propTypes = {
  handleClose: PropTypes.func,
  content: PropTypes.object,
};

export default Popup;
