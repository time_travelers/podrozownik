import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { useMediaQuery, useTheme } from "@material-ui/core";
import Card from "@material-ui/core/Card";
import Avatar from "@material-ui/core/Avatar";
import Rating from "@material-ui/lab/Rating";
import PropTypes from "prop-types";
import { ThemeProvider } from "@material-ui/styles";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import Button from "@material-ui/core/Button";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import ExpandLessIcon from "@material-ui/icons/ExpandLess";
import InsertEmoticonIcon from "@material-ui/icons/InsertEmoticon";
import { fetchUserData, sendUserComment } from "../RouteView/Api";
import CachedIcon from "@material-ui/icons/Cached";
import ErrorIcon from "@material-ui/icons/Error";
import "emoji-mart/css/emoji-mart.css";
import { Picker } from "emoji-mart";
import ReplayIcon from "@material-ui/icons/Replay";
import { commentFieldMessages } from "../../constants/messages";

const useStyles = makeStyles(theme => ({
  root: {
    padding: "20px 10px 20px 10px",
    marginBottom: "20px",

    background: "linear-gradient(90deg, rgb(218, 255, 118) 0%, rgb(149, 207, 12) 100%)",
    color: "black",
    display: "flex",
  },
  introSentence: {
    marginBottom: "10px",
  },
  introSentenceSmall: {
    marginBottom: "10px",
    fontSize: "13px",
  },
  username: {
    marginTop: "20px",
    marginLeft: "20px",
  },
  box: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    margin: "5px",
  },
  horizontalBox: {
    width: "100%",
    display: "flex",
    flexDirection: "row",
    position: "center",
  },
  horizontalBoxSmall: {
    width: "80%",
    display: "flex",
    flexDirection: "row",
    position: "center",
  },
  button: {
    width: "auto",
    fontSize: "13px",
    height: "40px",
    margin: "0px 5px 10px 5px",
  },
  grid: {
    width: "100%",
  },
  paper: {
    background: "rgb(68, 68, 67)",
  },
  media: {
    height: 0,
  },
  infoMessage: {
    marginLeft: "20px",
    padding: "10px",
  },
  largeAvatar: {
    backgroundColor: "#ff4000",
    width: theme.spacing(7),
    height: theme.spacing(7),
  },
  typoCut: {
    margin: "10px 0 15px 15px",
    color: "rgb(199, 191, 191)",
    fontSize: "15px",
    overflow: "hidden",
    textOverflow: "ellipsis",
    display: "-webkit-box",
    "-webkit-line-clamp": 2 /* number of lines to show */,
    "-webkit-box-orient": "vertical",
  },
  typoCutLarge: {
    margin: "10px 0 15px 15px",
    color: "rgb(199, 191, 191)",
    fontSize: "20px",
    overflow: "hidden",
    textOverflow: "ellipsis",
    display: "-webkit-box",
    "-webkit-line-clamp": 2 /* number of lines to show */,
    "-webkit-box-orient": "vertical",
  },
  typo: {
    color: "rgb(199, 191, 191)",
    fontSize: "15px",
  },
  typoLarge: {
    color: "rgb(199, 191, 191)",
    fontSize: "20px",
  },
  date: {
    color: "rgb(199, 191, 191)",
    fontSize: "15px",
  },
  cardHeader: {
    fontSize: "50px",
    color: "rgb(199, 191, 191)",
  },
  hide: {
    visibility: "hidden",
  },
  rating: {
    marginTop: "15px",
  },
  collapse: {
    transition: "width 2s, height 4s",
  },
  label: {
    marginTop: "10px",
    fontSize: "20px",
    color: "secondary",
  },
  comment: {
    color: "black",
    marginTop: "10px",
    width: "100%",
    marginBottom: "20px",
    "&:before": {
      borderColor: "black",
    },
    "&:after": {
      borderColor: "black",
    },
  },
  reply: {
    right: "right",
  },
  select: {
    "&:after": {
      borderColor: "black",
    },
  },
}));

function get_initials(str) {
  let acronym = str.split(/\s/).reduce((response, word) => (response += word.slice(0, 1)), "");
  return acronym;
}

function CommentField(props) {
  const classes = useStyles();
  const [comment, setComment] = React.useState("");
  const [error, setError] = React.useState(false);
  const [rating, setRating] = React.useState(5);
  const [user, setUser] = React.useState("");
  const [state, setState] = React.useState("loading");
  var mark = null;

  const [emojiView, setEmojiView] = React.useState(false);
  const messages = commentFieldMessages.infoMessages;

  const onEmojiClick = emojiObject => {
    setComment(comment + " " + emojiObject.native);
  };

  function getData() {
    fetchUserData(
      props.userId,
      data => {
        setUser(data);
        setState("view");
      },
      () => {
        setState("error");
      }
    );
  }

  function setMark(val) {
    mark = val;
    setRating(mark);
  }
  function submit() {
    getData();
    setComment("");
    setMark(null);
    setError(true);
    props.setPage();

    const commentData = {
      route_id: props.route_id,
      mark: rating,
      comment: comment,
      date_created: getDate(),
      positioning: 1.0,
    };
    sendUserComment(
      commentData,
      () => {
        setState("view");
      },
      () => {
        setState("view");
      }
    );
  }

  function updateError() {
    if (mark !== null) {
      setError(false);
    }
  }
  function showEmoji() {
    setEmojiView(!emojiView);
  }

  const updateComment = e => {
    setComment(e.target.value);
    updateError();
  };

  function getDate() {
    var today = new Date();
    var date = today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate();
    return date;
  }

  const theme = useTheme();
  const isBigScreen = useMediaQuery(theme => theme.breakpoints.up("lg"));
  const isSmallScreen = useMediaQuery(theme => theme.breakpoints.up("sm"));

  return (
    <ThemeProvider theme={theme}>
      <Grid className={classes.grid}>
        {state === "view" && (
          <Card className={classes.root}>
            <div className={classes.box}>
              <p className={isSmallScreen ? classes.introSentence : classes.introSentenceSmall}>
                {messages.addCommentAs}
              </p>
              <div className={classes.horizontalBox}>
                <Avatar className={classes.largeAvatar}>{get_initials(user.username)}</Avatar>
                <p className={classes.username}>{user.username}</p>
              </div>

              <FormControl className={classes.formControl}>
                <InputLabel className={classes.label} htmlFor="grouped-native-select"></InputLabel>

                <Rating
                  key={1}
                  precision={1}
                  label={"ocena"}
                  defaultValue={5}
                  className={classes.rating}
                  size={isBigScreen ? "large" : "small"}
                  value={rating}
                  onChange={e => {
                    setRating(e.target.value);
                    setError(false);
                  }}
                />
              </FormControl>

              <TextField
                value={comment}
                label={messages.comment}
                className={classes.comment}
                id="standard-secondary"
                color="secondary"
                onChange={updateComment}
                rows="3"
                multiline
              />
              <div className={isSmallScreen ? classes.horizontalBox : classes.horizontalBoxSmall}>
                <Button
                  variant="contained"
                  color="secondary"
                  disabled={error ? true : false}
                  onClick={submit}
                  className={classes.button}
                >
                  {messages.addComment}
                </Button>
                <Button
                  variant="contained"
                  color="secondary"
                  onClick={showEmoji}
                  className={classes.button}
                >
                  <InsertEmoticonIcon /> {emojiView ? <ExpandLessIcon /> : <ExpandMoreIcon />}
                </Button>
              </div>
              {emojiView && (
                <Picker
                  onSelect={onEmojiClick}
                  style={
                    isSmallScreen
                      ? { width: "100%", position: "center" }
                      : { width: "200px", position: "center" }
                  }
                />
              )}
            </div>
          </Card>
        )}
        {state === "loading" && getData() && (
          <Card className={classes.root}>
            <CachedIcon fontSize="large" />
            <p className={classes.infoMessage}>{messages.fetchingData}</p>
          </Card>
        )}
        {state === "error" && (
          <Card className={classes.root}>
            <div className={classes.horizontalBox}>
              <ErrorIcon fontSize="large" />
              <p className={classes.infoMessage}>{messages.loadError}</p>

              <Button
                onClick={() => {
                  getData;
                }}
              >
                <ReplayIcon fontSize="large" className={classes.reply} />
              </Button>
            </div>
          </Card>
        )}
      </Grid>
    </ThemeProvider>
  );
}

CommentField.propTypes = {
  userId: PropTypes.number,
  route_id: PropTypes.number,
  setPage: PropTypes.number,
};

export default CommentField;
