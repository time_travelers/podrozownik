import React, { useState, useEffect } from "react";
import { Container, IconButton, List, ListItem, CircularProgress } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import RouteItem from "./hooks/RouteItem";
import { fetchRoutes } from "./Api";
import ErrorTile from "../ErrorTile";
import PropTypes from "prop-types";
import { NAVBAR_HEIGHT, FOOTER_HEIGHT } from "../../constants/siteConstants";
import { RouteViewMessages } from "../../constants/messages";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import SearchBar from "../SearchBar/SearchBar";
import MarkField from "../SearchBar/Hooks/MarkField";

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    padding: "20px",
    marginBottom: "20px",
  },
  listView: {
    minHeight: `max(calc(100vh - ${NAVBAR_HEIGHT} - ${FOOTER_HEIGHT}), 180px)`,
    display: "flex",
  },
  progress: {
    margin: "auto",
  },
  backButton: {
    backgroundColor: "#545454",
    marginRight: "10px",
    width: "48px",
    opacity: 1,
    transition: [
      theme.transitions.create(["opacity", "width", "padding"], {
        duration: theme.transitions.duration.short,
        delay: 0,
      }),
      theme.transitions.create(["marginRight"], {
        duration: theme.transitions.duration.standard,
        delay: 500,
      }),
    ],
    "&:hover": {
      backgroundColor: "#545454",
    },
  },
  backButtonHidden: {
    opacity: 0,
    width: 0,
    paddingInline: 0,
    marginRight: 0,
  },
  item: {
    marginBottom: "25px",
    paddingInline: 0,
  },
}));

const { fetchError } = RouteViewMessages;

function fetchData(setViewState) {
  fetchRoutes(
    routes => {
      setViewState({
        view: "list",
        payload: routes,
      });
    },
    error => {
      setViewState({
        view: "error",
        error,
      });
    }
  );
}

function RouteList(props) {
  const classes = useStyles(props);
  const [viewState, setViewState] = useState({
    view: "loading",
    payload: [],
  });

  const [routes, setRoutes] = useState([]);
  const [routesVisible, setRoutesVisible] = useState(true);

  useEffect(() => {
    fetchData(setViewState);
  }, []);

  useEffect(() => {
    setRoutes(viewState.payload);
  }, [viewState]);

  function handleExit() {
    props.setPage("home");
  }

  return (
    <Container maxWidth="md" className={classes.listView}>
      {viewState.view === "loading" && (
        <div className={classes.progress}>
          <CircularProgress style={{ width: "100px", height: "100px" }} />
        </div>
      )}
      {viewState.view === "error" && (
        <ErrorTile {...fetchError} onExit={() => props.setPage("home")} />
      )}
      {viewState.view === "list" && (
        <List className={classes.root}>
          <ListItem className={classes.item} alignItems="flex-start">
            <IconButton
              color="primary"
              className={`${classes.backButton} ${!routesVisible && classes.backButtonHidden}`}
              onClick={handleExit}
            >
              <ArrowBackIcon />
            </IconButton>
            <SearchBar
              setPage={props.setPage}
              allRoutes={viewState.payload}
              setRoutes={setRoutes}
              setRoutesVisible={setRoutesVisible}
            >
              <MarkField />
            </SearchBar>
          </ListItem>

          {routesVisible &&
            routes.map(route => <RouteItem key={route.id} setPage={props.setPage} route={route} />)}
        </List>
      )}
    </Container>
  );
}

RouteList.propTypes = {
  setPage: PropTypes.func,
};

export default RouteList;
