import axios from "axios";

export function fetchRoutes(onSuccess, onError) {
  axios
    .get("/route")
    .then(response => {
      onSuccess(response.data);
    })
    .catch(error => {
      onError(error);
    });
}
