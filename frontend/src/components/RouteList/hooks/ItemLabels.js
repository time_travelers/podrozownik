import React from "react";
import PropTypes from "prop-types";
import { Rating } from "@material-ui/lab";
import ReportProblemIcon from "@material-ui/icons/ReportProblem";

function ItemLabels(props) {
  if (props.mark !== undefined)
    return <Rating name="size-medium-mark" size="small" value={props.mark} readOnly={true} />;
  else if (props.difficulty !== undefined)
    return (
      <Rating
        name="size-medium-diff"
        value={props.difficulty}
        size="small"
        readOnly={true}
        icon={<ReportProblemIcon fontSize="inherit" />}
      />
    );
  else if (props.duration !== undefined) return <span>Czas: {props.duration}</span>;
  else return <h1>ERROR</h1>;
}

ItemLabels.propTypes = {
  difficulty: PropTypes.number,
  mark: PropTypes.number,
  duration: PropTypes.string,
};

export default ItemLabels;
