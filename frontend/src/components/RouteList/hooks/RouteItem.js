import React from "react";
import PropTypes from "prop-types";
import {
  ListItem,
  Button,
  Typography,
  Avatar,
  ListItemText,
  ListItemAvatar,
  makeStyles,
} from "@material-ui/core";
import ItemLabels from "./ItemLabels";

const useStyles = makeStyles(theme => ({
  item: {
    backgroundColor: theme.palette.background.routeTile,
    marginBottom: "10px",
    "&:last-child": {
      marginBottom: 0,
    },
    [theme.breakpoints.down("xs")]: {
      flexDirection: "column",
    },
    alignItems: "center",
  },

  title: {
    color: theme.palette.primary.title,
  },

  paper: {
    backgroundColor: "#4e4d4d",
    width: "fit-content",
    display: "inline-block",
    margin: "5px",
    padding: "7px",
    borderRadius: "4px",
  },

  label: {
    backgroundColor: "#5d5d5d",
    display: "inline-block",
    padding: "2px 10px",
    marginRight: "10px",
    borderRadius: "20px",
    color: "#fff",
    "&:last-child": {
      marginRight: "0",
    },
  },

  author: {
    color: theme.palette.primary.subtitle,
    marginLeft: "10px",
    whiteSpace: "nowrap",
  },

  description: {
    color: theme.palette.primary.content,
    maxHeight: "63px",
    [theme.breakpoints.up("sm")]: {
      display: "-webkit-box",
      "-webkit-box-orient": "vertical",
      "-webkit-line-clamp": 3,
      overflow: "hidden",
    },
    [theme.breakpoints.down("sm")]: {
      maxHeight: "55px",
      fontSize: "13px",
    },
    [theme.breakpoints.down("xs")]: {
      fontSize: "11px",
      maxHeight: "initial",
    },
  },

  imageContainer: {
    margin: "10px",
    minWidth: "25%",
    [theme.breakpoints.down("xs")]: {
      display: "none",
    },
  },

  image: {
    maxHeight: "140px",
    maxWidth: "210px",
    width: "fit-content",
    height: "fit-content",
  },

  smallImage: {
    width: "50%",
    minWidth: "200px",
    [theme.breakpoints.up("sm")]: {
      display: "none",
    },
  },

  readMoreButton: {
    float: "right",
    marginTop: "5px",
  },
}));

function RouteItem(props) {
  const classes = useStyles(props);
  const { route } = props;
  const { features } = route;

  function readMore() {
    props.setPage(`route#${route.id}`);
  }

  return (
    <ListItem className={classes.item} alignItems="flex-start">
      <img src={route.photos[0].link} className={classes.smallImage} />
      <ListItemAvatar className={classes.imageContainer}>
        <Avatar
          alt={route.photos[0].uploader}
          variant="square"
          className={classes.image}
          src={route.photos[0].link}
        />
      </ListItemAvatar>
      <ListItemText
        disableTypography
        primary={
          <div style={{ marginBottom: "10px" }}>
            <Typography
              component="div"
              variant="body1"
              color="textPrimary"
              className={classes.title}
            >
              {route.title}
            </Typography>

            <Typography
              component="div"
              variant="subtitle2"
              color="textPrimary"
              className={classes.author}
            >
              {route.createdBy}
            </Typography>
          </div>
        }
        secondary={
          <React.Fragment>
            <div className={classes.description}>{route.short_description}</div>
            <div className={classes.paper}>
              <div className={classes.label}>
                <ItemLabels mark={features.mark} />
              </div>
              <div className={classes.label}>
                <ItemLabels difficulty={features.difficulty} />
              </div>
              <div className={classes.label}>
                <ItemLabels duration={features.duration} />
              </div>
            </div>
            <Button
              variant="outlined"
              color="primary"
              size="small"
              className={classes.readMoreButton}
              onClick={readMore}
            >
              Czytaj dalej
            </Button>
          </React.Fragment>
        }
      />
    </ListItem>
  );
}

RouteItem.propTypes = {
  route: PropTypes.object,
  setPage: PropTypes.func,
};

export default RouteItem;
