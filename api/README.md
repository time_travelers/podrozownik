# Podrozownik - api

Django app to serve REST api requests.

Documentation is available at [podrozownik-prod.herokuapp.com/api](https://podrozownik-prod.herokuapp.com/api).

## Installation

### Use Docker to run app

Being in `docker-compose.yml` directory run:

```bash
docker-compose up
```

### Use python3.8

Install all the things using: 

```bash
make config
```

This will create virtual envoronment, install all dependencies from poetry.lock, and configure [git hooks](#git-hooks)

## Usage

### Docker containers

To delete current docker image, run:

```bash
docker-compose down [--rmi all] [-v]
```

### Virtual environment

To create virtual environment, run:

```bash
make env
```

To configure variables in virtual environment, run:

```bash
make variables
```

You can activate virtual environment by:

```bash
source .venv/bin/activate
```

And deactivate with:

```bash
deactivate
```

Or just run installed dependency like `black` with:

```bash
.venv/bin/black api/
```

## Git hooks

Git hooks are pre defined actions, that runs before `git commit` command. It is very helpful, becouse you don't have to run all the commands like `black`, `isort` etc. and do it for you to ensure that, code is valid with pep8.

Git hooks are configured by default with `make config` command, but can also be configured with:

```bash
make hooks
```

## Database mock
To load example database into django run:

```bash
make db
```
