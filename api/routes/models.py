import json

from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

from accounts.models import CustomUser


class RouteManager(models.Manager):
    def create_route(self, length, type, creator_id, places_list, components=None):
        if not length:
            raise ValueError("Route must have a length")

        if not type:
            raise ValueError("Route must have a type")

        if not creator_id:
            raise ValueError("Route must have a creator")

        if not places_list:
            raise ValueError("Route must have a google reference")

        route = self.model()
        route.length = length
        route.type = type
        route.creator_id = creator_id
        route.set_places(places_list)

        if components is not None:
            route.set_components(components)

        route.save(using=self._db)
        return route


class RouteModel(models.Model):
    id = models.AutoField(primary_key=True)
    featured = models.BooleanField(default=True, blank=False)
    related_models = models.CharField(max_length=200, blank=True)
    title = models.CharField(max_length=100, blank=True)
    mark = models.FloatField(default=0, validators=[MaxValueValidator(5.0), MinValueValidator(0.0)])
    difficulty = models.IntegerField(
        default=0, validators=[MaxValueValidator(6), MinValueValidator(0)]
    )
    length = models.IntegerField(verbose_name="Length (km)")
    duration = models.CharField(max_length=20, blank=True)
    type = models.CharField(
        max_length=30,
        blank=True,
        default="moto",
        choices=[("moto", "moto"), ("bike", "bike"), ("hike", "hike")],
    )
    votes_cnt = models.IntegerField(default=0)
    creator_id = models.ForeignKey(
        CustomUser,
        blank=False,
        null=True,
        on_delete=models.SET_NULL,
        limit_choices_to={"staff": True},
    )

    places_list = models.CharField(max_length=200, blank=True, default=None)
    short_description = models.CharField(max_length=1000, blank=True)
    description = models.CharField(max_length=15000, blank=True)
    google_link = models.CharField(max_length=2000, blank=True)

    objects = RouteManager()

    def set_components(self, list):
        self.related_models = json.dumps(list)

    def get_components(self):
        return json.loads(self.related_models)

    def update_votes(self, new_mark, new_count):
        self.mark = new_mark
        self.votes_cnt = new_count

    def change_type(self, new_type):
        self.type = new_type

    def change_creator(self, new_creator):
        self.creator_id = new_creator

    def modify_length(self, len):
        self.length = len

    def set_places(self, new_list):
        self.places_list = json.dumps(new_list)

    def get_places(self):
        try:
            if isinstance(self.places_list, str):
                new_list = self.places_list.split(",")
                new_list = [loc.strip() for loc in new_list]
            return new_list
        except Exception as e:
            print(e)
            return ""

    def remove_place(self, place):
        tmp = json.loads(self.places_list)
        tmp.remove(place)
        self.places_list = json.dumps(tmp)

    def set_title(self, new_title):
        self.title = new_title

    def set_duration(self, dur):
        self.duration = dur

    def set_description(self, text):
        self.description = text

    def set_short_description(self, text):
        self.short_description = text
