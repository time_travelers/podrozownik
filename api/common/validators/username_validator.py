import re

from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _


def username_validator(username):
    min_length = 3
    max_length = 24
    if not min_length <= len(username) <= max_length:
        raise ValidationError(
            _("Username must be between %(min_length)d and %(max_length)d characters"),
            code="wrong_length_of_username",
            params={"min_length": min_length, "max_length": max_length},
        )
    if bool(re.search(r"\W", username)):
        raise ValidationError(_("Username must not contain special characters"))
