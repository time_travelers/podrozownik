import re

from django.core.exceptions import ValidationError
from django.utils.translation import gettext as _


def password_validator(password):
    min_length = 8
    max_length = 32
    if not min_length <= len(password) <= max_length:
        raise ValidationError(
            _(
                "Password must contain more than %(min_length)d "
                "and less than %(max_length)d characters"
            ),
            code="wrong_length_of_password",
            params={"min_length": min_length, "max_length": max_length},
        )
    if not bool(re.search(r"\d", password)):  # regex checks if there is a digit in password
        raise ValidationError(
            _("Password must contain at least one digit"), code="lack_of_digit_in_password"
        )
    if bool(re.search(r"\W", password)):
        raise ValidationError(_("Password must not contain special characters"))
