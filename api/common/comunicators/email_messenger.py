from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

from accounts.tokens import account_activation_token


def send_confirmation_email(user=None, email=None, domain=None):
    mail_subject = "Activate your blog account."
    message = render_to_string(
        "acc_active_email.html",
        {
            "user": user,
            "domain": domain,
            "uid": urlsafe_base64_encode(force_bytes(user.pk)),
            "token": account_activation_token.make_token(user),
        },
    )
    to_email = email
    email = EmailMessage(mail_subject, message, to=[to_email])
    email.send()


def send_reset_password_email(email=None, token=None, domain=None):
    mail_subject = "Reset your password"
    message = render_to_string(
        "reset_pass_email.html",
        {
            "domain": domain,
            "token": token,
            "email": email,
        },
    )
    to_email = email
    email = EmailMessage(mail_subject, message, to=[to_email])
    email.send()
