# Generated by Django 3.1.4 on 2021-01-02 14:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("travels", "0006_auto_20210102_1418"),
    ]

    operations = [
        migrations.AlterField(
            model_name="travelmodel",
            name="secret_id",
            field=models.CharField(max_length=100, null=True),
        ),
    ]
