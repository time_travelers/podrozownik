# Generated by Django 3.1.4 on 2020-12-29 22:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("travels", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="travelmodel",
            name="is_private",
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name="travelmodel",
            name="description",
            field=models.CharField(blank=True, max_length=1000),
        ),
    ]
