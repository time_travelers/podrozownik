from django.urls import path

from . import views


urlpatterns = [
    path("create/", views.TravelCreate.as_view(), name="travel-create"),
    path("cancel/", views.CancelTravel.as_view(), name="travel-cancel"),
    path("participate/", views.JoinTravel.as_view(), name="travel-participate"),
    path("<str:secret_id>", views.GetTravel.as_view(), name="travel-get"),
]
