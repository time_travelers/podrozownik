import uuid

from django.conf import settings
from django.db import models

from routes.models import RouteModel


class TravelManager(models.Manager):
    def create_travel(
        self,
        organizer_id,
        route_id,
        title,
        description,
        travel_datetime,
        assembly_point,
        facebook_link,
        conversation_link,
        is_private,
    ):
        if not organizer_id:
            raise ValueError("Travel must have an organizer")
        if not route_id:
            raise ValueError("Travel must have a route")
        if not title:
            raise ValueError("Travel must have a title")
        if not travel_datetime:
            raise ValueError("Travel must have a starting date and time")
        if not assembly_point:
            raise ValueError("Travel must have an assembly point")

        travel = self.model()

        travel.organizer_id = organizer_id
        travel.route_id = route_id
        travel.title = title
        travel.description = description
        travel.travel_datetime = travel_datetime
        travel.assembly_point = assembly_point
        travel.facebook_link = facebook_link
        travel.conversation_link = conversation_link
        travel.is_private = is_private

        travel.save(using=self._db)
        return travel


class TravelModel(models.Model):
    id = models.AutoField(primary_key=True)
    organizer_id = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        related_name="organizers2travels",
        on_delete=models.SET_NULL,
    )
    route_id = models.ForeignKey(RouteModel, on_delete=models.CASCADE)
    title = models.CharField(max_length=100, blank=True)
    description = models.CharField(max_length=1000, blank=True)
    travel_datetime = models.DateTimeField(verbose_name="Starts (date and time)")
    assembly_point = models.CharField(max_length=80, verbose_name="assembly point")
    facebook_link = models.CharField(max_length=80, verbose_name="facebook group link", blank=True)
    conversation_link = models.CharField(
        max_length=80, verbose_name="communicator link", blank=True
    )
    is_private = models.BooleanField(default=False)
    participants = models.ManyToManyField(
        settings.AUTH_USER_MODEL, symmetrical=False, related_name="participants2travels"
    )
    secret_id = models.CharField(
        max_length=100, null=True  # equal to id if travel is non-private. Encoded otherwise.
    )
    objects = TravelManager()

    def save(self, *args, **kwargs):
        """
        Overridden save method, so secret_id is properly updated
        """
        self.secret_id = str(uuid.uuid4()) if self.is_private else self.id
        super(TravelModel, self).save(*args, **kwargs)

    def get_organizer_id(self):
        return self.organizer_id.pk

    def get_route_id(self):
        return self.route_id.pk

    def is_assigned(self, user):
        """
        input: user - instance of user model (CustomUser)
        output: boolean value - True if user is participant. Otherwise False
        """
        return user in self.participants.all()
