from datetime import datetime, timedelta

from rest_framework import serializers

from .models import TravelModel


class TravelSerializer(serializers.ModelSerializer):
    next_week = datetime.now() + timedelta(days=7)

    title = serializers.CharField(max_length=100)
    description = serializers.CharField(max_length=1000, default="")
    travel_datetime = serializers.DateTimeField(default=next_week)
    assembly_point = serializers.CharField(max_length=80, required=True)
    facebook_link = serializers.CharField(max_length=80, default="")
    conversation_link = serializers.CharField(max_length=80, default="")
    is_private = serializers.BooleanField(default=False)

    def create(self, validated_data):
        travel = TravelModel.objects.create_travel(
            organizer_id=validated_data["organizer_id"],
            route_id=validated_data["route_id"],
            title=validated_data["title"],
            description=validated_data["description"],
            travel_datetime=validated_data["travel_datetime"],
            assembly_point=validated_data["assembly_point"],
            facebook_link=validated_data["facebook_link"],
            conversation_link=validated_data["conversation_link"],
            is_private=validated_data["is_private"],
        )
        return travel

    class Meta:
        model = TravelModel
        fields = (
            "organizer_id",
            "route_id",
            "title",
            "description",
            "travel_datetime",
            "assembly_point",
            "facebook_link",
            "conversation_link",
            "is_private",
        )
