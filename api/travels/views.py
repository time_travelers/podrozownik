from django.http import HttpResponseBadRequest, JsonResponse
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from photos.models import RoutePhoto, UserPhoto
from travels.models import TravelModel
from travels.serializers import TravelSerializer


class TravelCreate(APIView):
    """
    Logged user creates a new travel.
    """

    permission_classes = (IsAuthenticated,)

    def post(self, request, format="json"):
        logged_user = request.user
        request.data["organizer_id"] = logged_user.pk
        serializer = TravelSerializer(data=request.data)

        if serializer.is_valid():
            travel = serializer.save()
            if travel:
                travel.participants.add(request.data["organizer_id"])
            travel.save()
            return Response(travel.secret_id, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CancelTravel(APIView):
    """
    Logged user who is the travel organizer cancels the travel
    """

    permission_classes = (IsAuthenticated,)

    def delete(self, request, format="json"):
        user = request.user
        travel_id = request.data["travel_id"]

        try:
            travel = TravelModel.objects.get(id=travel_id)

            if travel.get_organizer_id() != user.pk:
                return HttpResponseBadRequest("You are not authorized to do that")
        except TravelModel.DoesNotExist:
            return HttpResponseBadRequest("Such travel does not exist")

        travel.delete()
        return Response(status=status.HTTP_200_OK)


class JoinTravel(APIView):
    """
    Logged user joins a private or non private travel if isn't signed yet.
    Otherwise the user signs out of a travel.
    """

    permission_classes = (IsAuthenticated,)

    def post(self, request, format="json"):
        """
        request should contain json with travel secret_id
        """
        user = request.user
        secret_id = request.data["secret_id"]
        try:
            travel = TravelModel.objects.get(secret_id=secret_id)

            if user in travel.participants.all():
                travel.participants.remove(user.pk)
                return Response("successfully signed out from travel ", status=status.HTTP_200_OK)
            else:
                travel.participants.add(user.pk)

        except TravelModel.DoesNotExist:
            return HttpResponseBadRequest("Such travel does not exist")

        return Response("successfully joined the travel", status=status.HTTP_201_CREATED)


class GetTravel(APIView):
    """
    Logged user gets information about a travel
    """

    permission_classes = (IsAuthenticated,)

    def get(self, request, secret_id):
        try:
            user = request.user
            travel = TravelModel.objects.get(secret_id=secret_id)
        except TravelModel.DoesNotExist:
            return HttpResponseBadRequest("Travel with given id is private or doesn't exist")

        participants = travel.participants.all()

        photos = RoutePhoto.objects.filter(route=travel.route_id)
        participants_info = [
            {"id": p.pk, "username": p.username, "avatar": UserPhoto.get_users_avatar_url(p)}
            for p in participants
        ]

        organizer_info = {
            "id": travel.organizer_id.pk,
            "username": travel.organizer_id.username,
            "avatar": UserPhoto.get_users_avatar_url(travel.organizer_id),
        }

        tags = {
            "organizer": organizer_info,
            "travel_datetime": travel.travel_datetime,
            "assembly_point": travel.assembly_point,
            "facebook_link": travel.facebook_link,
        }

        route = travel.route_id

        route_info = {
            "id": route.id,
            "title": route.title,
            "mark": route.mark,
            "difficulty": route.difficulty,
            "length": route.length,
            "duration": route.duration,
            "type": route.type,
        }

        response = {
            "id": travel.id,
            "secret_id": travel.secret_id,
            "is_private": travel.is_private,
            "photos": [photo.get_url() for photo in photos],
            "title": travel.title,
            "route": route_info,
            "tags": tags,
            "description": travel.description,
            "participants": participants_info,
            "navi_link": "",  # TODO: what is navi link and how to get one?
            "conversation_link": travel.conversation_link,
            "assigned": travel.is_assigned(user),
        }

        return JsonResponse(response, status=status.HTTP_200_OK)
