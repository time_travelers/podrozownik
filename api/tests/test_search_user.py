import json

from django.contrib.auth import get_user_model
from django.test import Client, TestCase


class TestUserRoute(TestCase):
    def setUp(self):
        User = get_user_model()
        user = User.objects.create_user(
            email="thebill@user.com", username="pu55yThe5troy3r", password="abcdef123"
        )
        user.first_name = "Jerzy"
        user.last_name = "Basior"
        user.save()

        c = Client()
        response_login = c.post(
            "/users/login/",
            {"username": "pu55yThe5troy3r", "password": "abcdef123"},
        )
        json_data = json.loads(response_login.content)
        self.auth_header = "Token " + json_data["token"]

    def test_search_user_success(self):
        """
        Do:
            - send GET /users/search-user/<search_phrase> request
        Expected:
            - server responses with 200 status code
            - server returns at least 1 match
        """
        c = Client()
        response = c.get("/users/search-user/The5", HTTP_AUTHORIZATION=self.auth_header)
        parsed_response = json.loads(response.content)
        try:
            searched_user = next(
                item for item in parsed_response if item["username"] == "pu55yThe5troy3r"
            )
        except StopIteration:
            self.assertTrue(False)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(searched_user["id"], 1)
        self.assertEqual(searched_user["username"], "pu55yThe5troy3r")
        self.assertEqual(searched_user["firstName"], "Jerzy")
        self.assertEqual(searched_user["lastName"], "Basior")

    def test_search_user_fails(self):
        """
        Do:
            - send GET /users/search-user/<search_phrase> request
        Expected:
            - server responses with 200 status code
            - server returns cannot find a match
        """
        c = Client()
        response = c.get(
            "/users/search-user/Thfdsfsfdsfae33412adse5", HTTP_AUTHORIZATION=self.auth_header
        )
        # parsed_response = json.loads(response.content)
        self.assertEqual(response.status_code, 204)
        self.assertEqual(response.content, b"")
