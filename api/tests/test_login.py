from django.contrib.auth import get_user_model
from django.test import Client, TestCase


class TestLogin(TestCase):
    def test_correct_login(self):
        User = get_user_model()
        User.objects.create_user(email="jsmith@gmail.com", username="john", password="smith123")
        c = Client()
        response = c.post(
            "/users/login/",
            {"username": "john", "password": "smith123", "email": "jsmith@gmail.com"},
        )
        check_token = "token" in str(response.content)
        self.assertTrue(check_token)

    def test_bad_login(self):
        User = get_user_model()
        User.objects.create_user(
            email="jshsmith@gmail.com", username="josh", password="smith11awkward"
        )
        c = Client()
        response = c.post(
            "/users/login/",
            {"username": "john", "password": "smith132", "email": "jsmith@gmail.com"},
        )
        self.assertFalse("token" in str(response.content))
        error_message = 'non_field_errors":["Unable to log in with provided credentials'
        self.assertTrue(error_message in str(response.content))
