from django.contrib.auth import get_user_model
from django.test import TestCase


class TestUserModel(TestCase):
    def test_create_user(self):
        """
        Test if user is created correctly
        """
        User = get_user_model()
        user = User.objects.create_user(
            email="normal@user.com", username="TestUser", password="foo"
        )

        self.assertEqual(user.email, "normal@user.com")
        self.assertTrue(user.is_active)
        self.assertFalse(user.is_staff)
        self.assertFalse(user.is_admin)
        self.assertEqual(user.username, "TestUser")

    def test_create_user_errors(self):
        """
        Test if errors while creating user
        are raised
        """
        User = get_user_model()

        with self.assertRaises(TypeError):
            User.objects.create_user()
        with self.assertRaises(TypeError):
            User.objects.create_user(email="")
        with self.assertRaises(ValueError):
            User.objects.create_user(email="", username="", password="foo")

    def test_create_superuser(self):
        """
        Test if superuser is created correctly
        """
        User = get_user_model()
        admin_user = User.objects.create_superuser("super@user.com", "TestAdmin", "foo")

        self.assertEqual(admin_user.email, "super@user.com")
        self.assertTrue(admin_user.is_active)
        self.assertTrue(admin_user.is_staff)
        self.assertTrue(admin_user.is_admin)

    def test_create_staff(self):
        """
        Test if staff user is created correctly
        """
        User = get_user_model()
        staff_user = User.objects.create_staffuser("staff@user.com", "StaffUser", "foo")

        self.assertEqual(staff_user.email, "staff@user.com")
        self.assertTrue(staff_user.is_active)
        self.assertTrue(staff_user.is_staff)
        self.assertFalse(staff_user.is_admin)

    def test_set_first_and_last_name(self):
        """
        Test setting first and last name for user
        """
        User = get_user_model()
        some_user = User.objects.create_user("k4r0l@m4.ma1eg0", "LubieMotory", "125")

        self.assertFalse(some_user.get_first_name())
        self.assertFalse(some_user.get_last_name())

        some_user.first_name = "Karol"
        some_user.last_name = "Olszanski"

        self.assertEqual(some_user.get_last_name(), "Olszanski")
        self.assertEqual(some_user.get_first_name(), "Karol")
