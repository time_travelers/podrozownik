from django.core.exceptions import ValidationError
from django.test import TestCase

from common.validators.password_validator import password_validator
from common.validators.username_validator import username_validator


class TestValidators(TestCase):
    def test_username_validator(self):
        with self.assertRaises(ValidationError):
            username_validator("ab")
        with self.assertRaises(ValidationError):
            username_validator("yamahaJestNajlepszaNaSwiecie")
        with self.assertRaises(ValidationError):
            username_validator("wildcard?s")
        self.assertFalse(username_validator("abaa0Z"))

    def test_password_validator(self):
        with self.assertRaises(ValidationError):
            password_validator("yamaha")
        with self.assertRaises(ValidationError):
            password_validator("ab")
        with self.assertRaises(ValidationError):
            password_validator("a" * 32 + "1")
        with self.assertRaises(ValidationError):
            password_validator("to nie moze byc haslem")
        self.assertFalse(username_validator("borewicz2"))
