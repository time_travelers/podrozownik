import sys
import unittest


class VersionTests(unittest.TestCase):
    """Check if python version is greater or equal 3.8"""

    def test_python_version(self):
        self.assertGreaterEqual(sys.version_info, (3, 8))
