from django.contrib.auth import get_user_model
from django.test import TestCase

from routes import models


class TestRouteModel(TestCase):
    def setUp(self):
        User = get_user_model()
        self.user = User.objects.create_user(
            email="normal@user.com", username="TestUser", password="foo"
        )
        self.user2 = User.objects.create_user(
            email="abnormal@user.com", username="Zdzichu", password="foo"
        )

    def test_basic_route_creation(self):
        places_list = "google_url1, google_url2"

        some_route = models.RouteModel.objects.create_route(
            420, "SuperHardXXXX", self.user, places_list
        )

        self.assertEqual(some_route.length, 420)
        self.assertEqual(some_route.type, "SuperHardXXXX")
        self.assertEqual(some_route.creator_id, self.user)
        # self.assertEqual(some_route.get_places(), ["google_url1", "google_url2"])

        self.assertFalse(some_route.related_models)

    def test_route_creation_with_components(self):
        places_list = "google_url1, google_url2"

        some_route = models.RouteModel.objects.create_route(
            69, "For small pps", self.user, places_list, [1, 5, 23, 15, 55]
        )

        self.assertEqual(some_route.get_components(), [1, 5, 23, 15, 55])

    def test_route_edition(self):
        places_list = "google_url1, google_url2"

        some_route = models.RouteModel.objects.create_route(
            69,
            "For small pps",
            self.user,
            places_list,
        )

        """Edit components"""
        self.assertFalse(some_route.related_models)
        some_route.set_components([4, 5, 6, 7, 8])
        self.assertEqual(some_route.get_components(), [4, 5, 6, 7, 8])

        """Edit creator_id"""

        self.assertEqual(some_route.creator_id, self.user)
        some_route.change_creator(self.user2)
        self.assertEqual(some_route.creator_id, self.user2)

        """Edit route places"""
        # self.assertEqual(some_route.get_places(), ["google_url1", "google_url2"])
        some_route.set_places(["Wroclaw_url", "Strzegom_url", "Jelenia_url"])
        # self.assertEqual(some_route.get_places(), ["Wroclaw_url", "Strzegom_url", "Jelenia_url"])
        some_route.remove_place("Strzegom_url")  # Elo Karolek :*
        # self.assertEqual(some_route.get_places(), ["Wroclaw_url", "Jelenia_url"])

        """Edit route type"""
        self.assertEqual(some_route.type, "For small pps")
        some_route.change_type("SUPER HARDCORE")
        self.assertEqual(some_route.type, "SUPER HARDCORE")

        """Edit marks"""
        self.assertEqual(some_route.mark, 0)
        self.assertEqual(some_route.votes_cnt, 0)
        some_route.update_votes(6, 8)
        self.assertEqual(some_route.mark, 6)
        self.assertEqual(some_route.votes_cnt, 8)

        """Change length"""
        some_route.modify_length(100)
        self.assertEqual(some_route.length, 100)

        """Set title"""
        self.assertFalse(some_route.title)
        some_route.set_title("Alko-trip")
        self.assertEqual(some_route.title, "Alko-trip")

        """Set duration"""
        self.assertFalse(some_route.duration)
        some_route.set_duration("1h 45min")
        self.assertEqual(some_route.duration, "1h 45min")

        """Set description"""
        self.assertFalse(some_route.description)
        some_route.set_description("Aaaabbbb qwertyu")
        self.assertEqual(some_route.description, "Aaaabbbb qwertyu")
