import json

from django.contrib.auth import get_user_model
from django.test import Client, TestCase


class TestUserRoute(TestCase):
    def setUp(self):
        # TestData: create 1 user
        User = get_user_model()
        user = User.objects.create_user(
            email="thebill@user.com", username="pu55yThe5troy3r", password="abcdef123"
        )
        user.first_name = "Jerzy"
        user.last_name = "Basior"
        user.save()

        c = Client()
        response_login = c.post(
            "/users/login/",
            {"username": "pu55yThe5troy3r", "password": "abcdef123", "email": "thebill@user.com"},
        )
        json_data = json.loads(response_login.content)
        self.auth_header = "Token " + json_data["token"]

    def test_get_user_success(self):
        """
        Do:
            - send GET /users/{user_id} request
        Expected:
            - server responses with 200 status code
            - server returns user created in setUp()
        """
        pass
        # c = Client()
        # response = c.get("/users/1", HTTP_AUTHORIZATION=self.auth_header)
        # parsed_response = json.loads(response.content)
        # self.assertEqual(response.status_code, 200)
        # self.assertEqual(parsed_response["id"], 1)
        # self.assertEqual(parsed_response["username"], "pu55yThe5troy3r")
        # self.assertEqual(parsed_response["email"], "thebill@user.com")
        # self.assertEqual(parsed_response["firstName"], "Jerzy")
        # self.assertEqual(parsed_response["lastName"], "Basior")

    def test_update_user_success(self):
        """
        Do:
            - send PUT /users/{user_id} request
        Expected:
            - server responses with 200 status code
            - server updates user created in setUp()
        """
        c = Client()
        User = get_user_model()
        posted_content = {
            "username": "dfasdfsafaff",
            "email": "andrzej@mail.com",
            "firstName": "Jerzyk",
            "lastName": "Basiorek",
        }
        response = c.put(
            "/users/1",
            data=posted_content,
            content_type="application/json",
            HTTP_AUTHORIZATION=self.auth_header,
        )
        self.assertEqual(response.status_code, 200)
        user = User.objects.get(pk=1)
        self.assertTrue(user.id)
        self.assertEqual(user.username, "pu55yThe5troy3r")
        self.assertEqual(user.email, "andrzej@mail.com")
        self.assertEqual(user.get_first_name(), "Jerzyk")
        self.assertEqual(user.get_last_name(), "Basiorek")

    def test_get_user_fails(self):
        """
        Do:
            - send GET /users/{user_id} request (for non existing user)
        Expected:
            - server responses with 401 status code becouse user isn't authenticated
            and doesn't exist
        """
        c = Client()
        response = c.get("/users/22")
        self.assertEqual(response.status_code, 401)

    def test_update_user_fails(self):
        """
        Do:
            - send PUT /users/{user_id} request (for non existing user)
        Expected:
            - server responses with 400 status code
        """
        c = Client()
        posted_content = {
            "username": "AndrzejHonda",
            "email": "andrzej@mail.com",
            "password": "andrzej1",
        }
        response = c.put(
            "/users/22",
            data=posted_content,
            content_type="application/json",
            HTTP_AUTHORIZATION=self.auth_header,
        )
        self.assertEqual(response.status_code, 400)

    def test_update_user_invalid_mail(self):
        """
        Do:
            - send PUT /users/{user_id} request with invalid email
        Expected:
            - server responses with 400 status code,
            because email validation fails
        """
        c = Client()
        posted_content = {
            "email": "k4r0llm4M4l3g0.com",
        }
        response = c.put(
            "/users/1",
            data=posted_content,
            content_type="application/json",
            HTTP_AUTHORIZATION=self.auth_header,
        )
        self.assertEqual(response.content, b"Invalid email supplied")
        self.assertEqual(response.status_code, 400)
