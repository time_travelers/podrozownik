from django.contrib.auth import get_user_model
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase


User = get_user_model()


class AccountsTest(APITestCase):
    def setUp(self):
        # We want to go ahead and originally create a user.
        self.test_user = User.objects.create_user(
            username="testuser", email="test@example.com", password="testpassword1"
        )
        # URL for creating an account....
        self.create_url = reverse("account-create")

    def test_create_user(self):
        """
        Ensure we can create a new user and a valid token is created with it.
        """
        data = {"username": "foobar", "email": "foobar@example.com", "password": "somepassword1"}

        response = self.client.post(self.create_url, data, format="json")

        # We want to make sure we have two users in the database..
        self.assertEqual(User.objects.count(), 2)
        # And that we're returning a 201 created code.
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # Additionally, we want to return the username and email upon successful creation.
        self.assertTrue(response.data is None)

    def test_create_user_with_too_long_username(self):
        data = {"username": "foo" * 30, "email": "foobarbaz@example.com", "password": "foobar1234"}

        response = self.client.post(self.create_url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(len(response.data["username"]), 1)

    def test_create_user_with_no_username(self):
        data = {"username": "", "email": "foobarbaz@example.com", "password": "foobar12345"}

        response = self.client.post(self.create_url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(len(response.data["username"]), 1)

    def test_create_user_with_preexisting_username(self):
        data = {"username": "testuser", "email": "user@example.com", "password": "testuser12345"}

        response = self.client.post(self.create_url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(len(response.data["username"]), 1)
