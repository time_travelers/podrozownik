import re

from django.contrib.auth import get_user_model
from django.core import mail
from django.test import Client, TestCase

from common.comunicators.email_messenger import send_confirmation_email


REGISTRATION_URL = "/users/register/"
ACTIVATION_URL = "/users/activate/"


class TestUserRegistration(TestCase):
    def setUp(self) -> None:
        self.username = "fooyara3"
        self.email = "ratajczakbartlomiej30@email.com"
        self.password = "OTREbyOwsiane123"
        self.client = Client()

    def test_registration(self):
        mail.outbox = []
        response = self.client.post(
            REGISTRATION_URL,
            {"username": self.username, "email": self.email, "password": self.password},
        )
        # has status 201 Created
        self.assertEqual(response.status_code, 201)
        self.assertEqual(len(mail.outbox), 1)
        mail_content = mail.outbox[0].body
        User = get_user_model()
        user = User.objects.get(email=self.email)
        # user should not be active at this moment
        self.assertFalse(user.is_active)
        match = re.search(r"(?<=/users/activate/).*", mail_content)
        token = match.group(0)
        # activate account
        response = self.client.get(ACTIVATION_URL + token)
        user = User.objects.get(email=self.email)
        # now user should be active
        self.assertTrue(user.is_active)
        # check if redirected
        self.assertEqual(response.status_code, 302)

    def test_email_messenger(self):
        mail.outbox = []
        User = get_user_model()
        user = User.objects.create_user(
            email="thebill@user.com", username="andrzejek1", password="foolaboga2o"
        )
        send_confirmation_email(user=user, email="thebill@user.com", domain="nicciekawego")
        self.assertEqual(len(mail.outbox), 1)
