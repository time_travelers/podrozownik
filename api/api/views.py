import json

from django.contrib.auth import get_user_model
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.core.validators import validate_email
from django.http import HttpResponse, HttpResponseBadRequest, JsonResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView

from accounts.serializers import UserReadSerializer
from marks import models as mark_models
from photos import models as photos_models
from photos.models import RoutePhoto, UserPhoto
from routes import models as route_models
from travels import models as travel_models


class GetOrUpdateUser(APIView):

    permission_classes = (IsAuthenticated,)

    @csrf_exempt
    def get(self, request, user_id):
        User = get_user_model()
        UserPhoto = photos_models.UserPhoto

        try:
            user = User.objects.get(id=user_id)
        except ObjectDoesNotExist:
            return HttpResponseBadRequest("Invalid user_id supplied")

        user_profile_photo = UserPhoto.objects.filter(uploader=user_id)

        response = {
            "id": user.id,
            "username": user.username,
            "email": user.email,
            "firstName": user.get_first_name(),
            "lastName": user.get_last_name(),
            "link": user_profile_photo[0].image.url,
        }
        return JsonResponse(response)

    @csrf_exempt
    def put(self, request, user_id):
        User = get_user_model()
        try:
            user = User.objects.get(id=user_id)
        except ObjectDoesNotExist:
            return HttpResponseBadRequest("Invalid user_id supplied")

        logged_user = request.user

        if user_id != logged_user.pk:
            return HttpResponseBadRequest("You are not authorized to do that")

        request_body = json.loads(request.body)

        if "email" in request_body.keys() and request_body["email"] != user.email:
            new_email = request_body["email"]
            try:
                validate_email(new_email)
            except ValidationError:
                return HttpResponseBadRequest("Invalid email supplied")
            user.email = new_email
        if "firstName" in request_body.keys():
            if user.first_name != request_body["firstName"]:
                user.first_name = request_body["firstName"]
        if "lastName" in request_body.keys():
            user.last_name = request_body["lastName"]
        user.save()
        return HttpResponse(status=200)


class SearchUserByUsername(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def get(request, search_phrase):
        User = get_user_model()
        users = User.objects.filter(username__icontains=search_phrase)
        serializer = UserReadSerializer(users, many=True)
        if serializer.data:
            return JsonResponse(serializer.data, status=status.HTTP_200_OK, safe=False)
        else:
            return HttpResponse(status=status.HTTP_204_NO_CONTENT)


def create_route_json(route_id):
    Route = route_models.RouteModel
    Mark = mark_models.RouteMarkModel
    Photo = photos_models.RoutePhoto
    UserPhoto = photos_models.UserPhoto

    route = Route.objects.get(id=route_id)
    marks_query = Mark.objects.filter(route_id=route_id)
    photo_query = Photo.objects.filter(route=route_id)

    features = {
        "type": str(route.type),
        "length": str(route.length),
        "duration": str(route.duration),
        "mark": route.mark,
        "difficulty": route.difficulty,
        "location": route.get_places(),
        "google_link": route.google_link,
    }

    photos = []

    for photo in photo_query:
        photo_json = {
            "id": photo.id,
            "uploader": str(photo.uploader.get_full_name()),
            "link": str(photo.image.url),
        }

        photos.append(photo_json)

    marks = []

    for mark in marks_query:
        user_profile_photo = UserPhoto.objects.filter(uploader=mark.user_id)

        link = " " if len(user_profile_photo) != 1 else user_profile_photo[0].image.url

        mark_json = {
            "id": mark.id,
            "commenter": str(mark.user_id.get_full_name()),
            "link": str(link),
            "mark": mark.mark,
            "comment": mark.comment,
            "date": str(mark.date_created),
        }

        marks.append(mark_json)

    response = {
        "id": str(route.id),
        "featured": str(route.featured),
        "title": str(route.title),
        "createdBy": str(route.creator_id.get_full_name()),
        "description": str(route.description),
        "short_description": str(route.short_description),
        "features": features,
        "photos": photos,
        "marks": marks,
    }

    return response


def get_route(request, route_id):
    try:
        if request.method == "GET":
            return JsonResponse(create_route_json(route_id), safe=False)
    except ObjectDoesNotExist:
        return HttpResponseBadRequest("Invalid route_id supplied")


def get_all_routes(request):
    Route = route_models.RouteModel
    try:
        routes_id = Route.objects.values_list("id", flat=True)
        if request.method == "GET":
            route_tab = [create_route_json(id) for id in routes_id]

            return JsonResponse(route_tab, safe=False)
    except ObjectDoesNotExist:
        return HttpResponseBadRequest("Something went wrong")


def index(request):
    return render(request, "index.html")


def get_all_travels(request):
    if request.method == "GET":
        Travel = travel_models.TravelModel
        travel_ids = Travel.objects.values_list("id", flat=True)
        travel_tab = [travel_json(_id) for _id in travel_ids]
        return JsonResponse(travel_tab, safe=False)


def travel_json(travel_id):
    travel = travel_models.TravelModel.objects.get(id=travel_id)
    participants = travel.participants.all()

    photos = RoutePhoto.objects.filter(route=travel.route_id)
    participants_info = [
        {"id": p.pk, "username": p.username, "avatar": UserPhoto.get_users_avatar_url(p)}
        for p in participants
    ]

    organizer_info = {
        "id": travel.organizer_id.pk,
        "username": travel.organizer_id.username,
        "avatar": UserPhoto.get_users_avatar_url(travel.organizer_id),
    }

    tags = {
        "organizer": organizer_info,
        "travel_datetime": travel.travel_datetime,
        "assembly_point": travel.assembly_point,
        "facebook_link": travel.facebook_link,
    }

    route = travel.route_id

    route_info = {
        "id": route.id,
        "title": route.title,
        "mark": route.mark,
        "difficulty": route.difficulty,
        "length": route.length,
        "duration": route.duration,
        "type": route.type,
    }

    response = {
        "id": travel.id,
        "secret_id": travel.secret_id,
        "is_private": travel.is_private,
        "photos": [photo.get_url() for photo in photos],
        "title": travel.title,
        "route": route_info,
        "tags": tags,
        "description": travel.description,
        "participants": participants_info,
        "navi_link": "",  # TODO: what is navi link and how to get one?
        "conversation_link": travel.conversation_link,
    }
    return response
