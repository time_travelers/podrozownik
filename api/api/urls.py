"""api URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1Hi {{ user.username }}, welcome to Podrozownik!
Please click on the link to confirm your registration,http://{{ domain }}{% url 'activate'
 uidb64=uid token=token %}
   2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path
from django.views.generic import RedirectView

from . import views


urlpatterns = [
    path("", views.index, name="home"),
    path("api/", include("docs.urls")),
    path("admin/", admin.site.urls),
    path("users/<int:user_id>", views.GetOrUpdateUser.as_view()),
    url(r"^users/", include("accounts.urls")),
    url(r"^images/", include("photos.urls")),
    path("favicon.ico", RedirectView.as_view(url="/static/favicon.ico")),
    path("manifest.json", RedirectView.as_view(url="/static/manifest.json")),
    path("route/<int:route_id>", views.get_route),
    path("route", views.get_all_routes),
    url(r"^mark/", include("marks.urls")),
    url(r"^travels/", include("travels.urls")),
    path("users/search-user/<str:search_phrase>", views.SearchUserByUsername.as_view()),
    path("travels/", views.get_all_travels),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
