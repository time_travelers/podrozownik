from cloudinary.models import CloudinaryField
from django.db import models

from accounts.models import CustomUser
from marks.models import RouteMarkModel
from routes.models import RouteModel


class UserPhotoManager(models.Manager):
    use_in_migrations = True

    def create_photo(self, image, uploader):
        if not image:
            raise ValueError("Image not attached")
        if not uploader:
            raise ValueError("Uploader is required")
        photo = self.model()
        photo.image = image
        photo.uploader = uploader

        photo.save(using=self._db)
        return photo


class UserPhoto(models.Model):
    id = models.AutoField(primary_key=True)
    uploader = models.OneToOneField(CustomUser, on_delete=models.CASCADE)
    image = CloudinaryField("image")

    objects = UserPhotoManager()
    REQUIRED_FIELDS = ["image", "uploader"]
    """ Informative name for model """

    def __unicode__(self):
        try:
            public_id = self.image.public_id
        except AttributeError:
            public_id = ""
        return "Photo <%s:%s>" % (self.title, public_id)

    def get_id(self):
        return self.id

    def get_uploader(self):
        if self.uploader.first_name and self.uploader.last_name:
            return str(self.uploader.first_name + self.uploader.last_name)
        else:
            return str(self.uploader.username)

    def get_image(self):
        return self.image

    def get_public_id(self):
        return self.image.public_id

    def get_url(self):
        return self.image.url

    @staticmethod
    def get_users_avatar_url(user):
        try:
            avatar = UserPhoto.objects.get(uploader=user)
            avatar = avatar.get_url()
        except UserPhoto.DoesNotExist:
            avatar = None
        return avatar


class RoutePhotoManager(models.Manager):
    use_in_migrations = True

    def create_photo(self, image, uploader, route, in_description=False):
        if not image:
            raise ValueError("Image not attached")
        if not uploader:
            raise ValueError("Uploader is required")
        if not route:
            raise ValueError("Route is required")
        photo = self.model()
        photo.image = image
        photo.uploader = uploader
        photo.route = route
        photo.in_description = in_description

        photo.save(using=self._db)
        return photo


class RoutePhoto(models.Model):
    id = models.AutoField(primary_key=True)
    image = CloudinaryField("image")
    route = models.ForeignKey(RouteModel, on_delete=models.CASCADE)
    uploader = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    in_description = models.BooleanField(default=False)
    REQUIRED_FIELDS = ["image", "uploader", "route"]
    objects = RoutePhotoManager()

    def __unicode__(self):
        try:
            public_id = self.image.public_id
        except AttributeError:
            public_id = ""
        return "Photo <%s:%s>" % (self.title, public_id)

    def get_route(self):
        return self.route

    def get_id(self):
        return self.id

    def get_uploader(self):
        if self.uploader.first_name and self.uploader.last_name:
            return str(self.uploader.first_name + self.uploader.last_name)
        else:
            return str(self.uploader.username)

    def get_image(self):
        return self.image

    def get_public_id(self):
        return self.image.public_id

    def get_url(self):
        return self.image.url


class CommentPhotoManager(models.Manager):
    use_in_migrations = True

    def create_photo(self, image, uploader, comment):
        if not image:
            raise ValueError("Image not attached")
        if not uploader:
            raise ValueError("Uploader is required")
        if not comment:
            raise ValueError("Comment is required")
        photo = self.model()
        photo.image = image
        photo.uploader = uploader
        photo.comment = comment

        photo.save(using=self._db)
        return photo


class CommentPhoto(models.Model):
    id = models.AutoField(primary_key=True)
    image = CloudinaryField("image")
    comment = models.ForeignKey(RouteMarkModel, on_delete=models.CASCADE)
    uploader = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    REQUIRED_FIELDS = ["image", "uploader", "mark"]
    objects = CommentPhotoManager()

    def __unicode__(self):
        try:
            public_id = self.image.public_id
        except AttributeError:
            public_id = ""
        return "Photo <%s:%s>" % (self.title, public_id)

    def get_comment(self):
        return self.comment

    def get_id(self):
        return self.id

    def get_uploader(self):
        if self.uploader.first_name and self.uploader.last_name:
            return str(self.uploader.first_name + self.uploader.last_name)
        else:
            return str(self.uploader.username)

    def get_image(self):
        return self.image

    def get_public_id(self):
        return self.image.public_id

    def get_url(self):
        return self.image.url
