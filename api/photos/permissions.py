from rest_framework import permissions
from rest_framework.permissions import SAFE_METHODS, BasePermission


class IsAdminOrStaffUser(BasePermission):
    """
    Allows access only to admin users.
    """

    def has_permission(self, request, view):
        return request.user and (request.user.is_staff or request.user.is_superuser)


class IsStaffOrReadOnly(permissions.BasePermission):
    """
    Allows WRITE operations for only staff and admin users
    """

    def has_permission(self, request, view):
        is_or_staff_user = IsAdminOrStaffUser()
        is_staff = is_or_staff_user.has_permission(request, view)
        return request.method in SAFE_METHODS or is_staff
