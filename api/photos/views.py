import cloudinary
from django.contrib.auth import get_user_model
from django.http import JsonResponse
from rest_framework import permissions, status
from rest_framework.parsers import JSONParser, MultiPartParser
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import CommentPhoto, RoutePhoto, UserPhoto
from .permissions import IsStaffOrReadOnly
from .serializers import (
    CommentPhotoReadSerializer,
    CommentPhotoWriteSerializer,
    RoutePhotoReadSerializer,
    RoutePhotoWriteSerializer,
    UserPhotoReadSerializer,
    UserPhotoWriteSerializer,
)


class UserImageView(APIView):
    parser_classes = (
        MultiPartParser,
        JSONParser,
    )

    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    @staticmethod
    def post(request, user_id=None):
        user = request.user  # verifies if one is posting photo for his account.
        if user.pk != user_id:
            return Response({"response": "You can only upload an image for your account."})

        request.data["uploader"] = user_id
        print(request.data)
        serializer = UserPhotoWriteSerializer(data=request.data)
        if serializer.is_valid():
            photo = serializer.save()
            if photo:
                photo.save()
                return JsonResponse(serializer.data, status=status.HTTP_201_CREATED)
        else:
            print(serializer.errors)
            return Response(status=status.HTTP_406_NOT_ACCEPTABLE)

    @staticmethod
    def get(request, user_id=None):
        user_model = get_user_model()
        try:
            user_model.objects.get(id=user_id)
        except user_model.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        photos = UserPhoto.objects.filter(uploader=user_id)
        serializer = UserPhotoReadSerializer(photos, many=True)
        return JsonResponse(serializer.data, status=status.HTTP_200_OK, safe=False)

    @staticmethod
    def put(request, user_id):
        """
        deleting user image
        """
        user = request.user  # verifies if one is deleting his own photo
        if user.pk != user_id:
            return Response({"response": "you do not have a permission to do that."})

        user_model = get_user_model()
        try:
            user_model.objects.get(id=user_id)
        except user_model.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        photo = UserPhoto.objects.get(uploader=user_id)
        public_id = photo.image.public_id
        serializer = UserPhotoWriteSerializer(photo, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            cloudinary.uploader.destroy(public_id)
        return JsonResponse(serializer.data, status=status.HTTP_200_OK, safe=False)


class RouteImageView(APIView):
    parser_classes = (
        MultiPartParser,
        JSONParser,
    )
    permission_classes = [IsStaffOrReadOnly]

    @staticmethod
    def post(request, route_id=None):
        request.data["route"] = route_id
        serializer = RoutePhotoWriteSerializer(data=request.data)

        if serializer.is_valid():
            photo = serializer.save()
            if photo:
                photo.save()
                return JsonResponse(serializer.data, status=status.HTTP_201_CREATED)
        else:
            print(serializer.errors)
            return Response(status=status.HTTP_406_NOT_ACCEPTABLE)

    @staticmethod
    def get(request, route_id=None):
        photos = RoutePhoto.objects.filter(route=route_id)
        serializer = RoutePhotoReadSerializer(photos, many=True)
        return JsonResponse(serializer.data, status=status.HTTP_200_OK, safe=False)


class RouteDescriptionImageView(APIView):
    parser_classes = (
        MultiPartParser,
        JSONParser,
    )
    permission_classes = [IsStaffOrReadOnly]

    @staticmethod
    def post(request, route_id=None):
        request.data["route"] = route_id
        request.data["in_description"] = True
        serializer = RoutePhotoWriteSerializer(data=request.data)

        if serializer.is_valid():
            photo = serializer.save()
            if photo:
                photo.save()
                return JsonResponse(serializer.data, status=status.HTTP_201_CREATED)
        else:
            print(serializer.errors)
            return Response(status=status.HTTP_406_NOT_ACCEPTABLE)

    @staticmethod
    def get(request, route_id=None):
        photos = RoutePhoto.objects.filter(route=route_id)
        serializer = RoutePhotoReadSerializer(photos, many=True)
        return JsonResponse(serializer.data, status=status.HTTP_200_OK, safe=False)


class CommentImageView(APIView):
    parser_classes = (
        MultiPartParser,
        JSONParser,
    )
    permissions = [permissions.IsAuthenticatedOrReadOnly]

    @staticmethod
    def post(request, comment_id=None):
        request.data["uploader"] = request.user.pk
        request.data["comment"] = comment_id
        serializer = CommentPhotoWriteSerializer(data=request.data)

        if serializer.is_valid():
            photo = serializer.save()
            if photo:
                photo.save()
                return JsonResponse(serializer.data, status=status.HTTP_201_CREATED)
        else:
            print(serializer.errors)
            return Response(status=status.HTTP_406_NOT_ACCEPTABLE)

    @staticmethod
    def get(request, comment_id=None):
        photos = CommentPhoto.objects.filter(comment=comment_id)
        serializer = CommentPhotoReadSerializer(photos, many=True)
        return JsonResponse(serializer.data, status=status.HTTP_200_OK, safe=False)
