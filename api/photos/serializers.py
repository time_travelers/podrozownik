from rest_framework import serializers

from .models import CommentPhoto, RoutePhoto, UserPhoto


class UserPhotoWriteSerializer(serializers.ModelSerializer):
    image = serializers.ImageField()

    def create(self, validated_data):
        return UserPhoto.objects.create_photo(**validated_data)

    def update(self, instance, validated_data):
        instance.image = validated_data.get("image", instance.image)
        instance.save()
        return instance

    class Meta:
        model = UserPhoto
        fields = ("uploader", "image")


class UserPhotoReadSerializer(serializers.ModelSerializer):
    public_id = serializers.ReadOnlyField(source="image.public_id")
    uploader = serializers.ReadOnlyField(source="uploader.username")
    url = serializers.URLField(read_only=True, source="image.url")

    class Meta:
        model = UserPhoto
        fields = ("public_id", "uploader", "url")


class RoutePhotoWriteSerializer(serializers.ModelSerializer):
    image = serializers.ImageField()

    def create(self, validated_data):
        return RoutePhoto.objects.create_photo(**validated_data)

    class Meta:
        model = RoutePhoto
        fields = ("uploader", "image", "route", "in_description")


class RoutePhotoReadSerializer(serializers.ModelSerializer):
    public_id = serializers.ReadOnlyField(source="image.public_id")
    uploader = serializers.ReadOnlyField(source="uploader.username")
    url = serializers.URLField(read_only=True, source="image.url")

    class Meta:
        model = RoutePhoto
        fields = ("public_id", "uploader", "url", "in_description")


class CommentPhotoWriteSerializer(serializers.ModelSerializer):
    image = serializers.ImageField()
    # comment = serializers.IntegerField(write_only=True, validators=[comment_validator])

    def create(self, validated_data):
        return CommentPhoto.objects.create_photo(**validated_data)

    def validate_comment(self, comment):
        if CommentPhoto.objects.filter(comment=comment).count() >= 5:
            raise serializers.ValidationError("Comment already has maximal amount of photos (5)")
        return comment

    class Meta:
        model = CommentPhoto
        fields = ("uploader", "image", "comment")


class CommentPhotoReadSerializer(serializers.ModelSerializer):
    public_id = serializers.ReadOnlyField(source="image.public_id")
    uploader = serializers.ReadOnlyField(source="uploader.username")
    url = serializers.URLField(read_only=True, source="image.url")

    class Meta:
        model = CommentPhoto
        fields = ("public_id", "uploader", "url")
