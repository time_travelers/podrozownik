from django.urls import path

from .views import CommentImageView, RouteDescriptionImageView, RouteImageView, UserImageView


urlpatterns = [
    path("users/<int:user_id>", UserImageView.as_view(), name="images-users"),
    path("usersGallery/<int:user_id", UserImageView.as_view(), name="images-gallery"),
    path("routes/<int:route_id>", RouteImageView.as_view(), name="images-routes"),
    path(
        "routes/description/<int:route_id>",
        RouteDescriptionImageView.as_view(),
        name="images-description",
    ),
    path("comments/<int:comment_id>", CommentImageView.as_view(), name="images-comments"),
]
