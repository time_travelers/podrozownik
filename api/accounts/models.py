import os

from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.db import models
from django.dispatch import receiver
from django_rest_passwordreset.signals import reset_password_token_created

from common.comunicators.email_messenger import send_reset_password_email


class CustomUserManager(BaseUserManager):
    use_in_migrations = True

    def create_user(self, email, username, password):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError("Users must have an email address")

        if not username:
            raise ValueError("Users must have an username")

        if not password:
            raise ValueError("Users must have a password")

        user = self.model(
            email=self.normalize_email(email),
        )

        user.username = username

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_staffuser(self, email, username, password):
        """
        Creates and saves a staff user with the given email and password.
        """
        user = self.create_user(
            email,
            username,
            password=password,
        )

        user.staff = True
        user.save(using=self._db)
        return user

    def create_superuser(self, email, username, password):
        """
        Creates and saves a superuser with the given email and password.
        """
        user = self.create_user(
            email,
            username,
            password=password,
        )
        user.staff = True
        user.admin = True
        user.save(using=self._db)
        return user


class CustomUser(AbstractBaseUser):
    id = models.AutoField(primary_key=True)
    email = models.EmailField(
        verbose_name="email address",
        max_length=255,
        blank=False,
        unique=True,
    )
    username = models.CharField("username", max_length=30, blank=False, unique=True)
    first_name = models.CharField("firstname", max_length=30, blank=True, unique=False)
    last_name = models.CharField("lastname", max_length=30, blank=True, unique=False)
    active = models.BooleanField(default=True)
    staff = models.BooleanField(default=False)  # a admin user; non super-user
    admin = models.BooleanField(default=False)  # a superuser

    objects = CustomUserManager()

    USERNAME_FIELD = "username"
    REQUIRED_FIELDS = ["email"]  # Username & Password are required by default.

    def get_nickname(self):
        # The user is identified by their email address
        return self.username

    def get_last_name(self):
        return self.last_name

    def get_first_name(self):
        return self.first_name

    def get_full_name(self):
        return self.get_first_name() + " " + self.get_last_name()

    def get_email(self):
        # The user is identified by their email address
        return self.email

    def __str__(self):  # __unicode__ on Python 2
        return self.username

    def has_perm(self, perm, obj=None):
        """
        Does the user have a specific permission?
        This method will be modified in future
        """
        return True

    def has_module_perms(self, app_label):
        """
        Does the user have permissions to view the app `app_label`?
        This method will be modified in future
        """
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        return self.staff

    @property
    def is_admin(self):
        "Is the user a admin member?"
        return self.admin

    @property
    def is_active(self):
        "Is the user active?"
        return self.active


@receiver(reset_password_token_created)
def password_reset_token_created(reset_password_token, *args, **kwargs):
    send_reset_password_email(
        reset_password_token.user.email, reset_password_token.key, os.environ["BASE_DOMAIN"]
    )
