from django.conf.urls import include
from django.urls import path
from django.views.generic import TemplateView

from . import views


urlpatterns = [
    path("register/", views.UserCreate.as_view(), name="account-create"),
    path("login/", views.CustomAuthToken.as_view(), name="login"),
    path("logout/", views.Logout.as_view(), name="logout"),
    path(
        "activate/<slug:uidb64>/<slug:token>/", views.activate, name="activate"
    ),  # email confirmation
    path("summernote/", include("django_summernote.urls")),
    path("change-password/", views.ChangePasswordView.as_view(), name="change-password"),
    path(
        "new-password/",
        TemplateView.as_view(template_name="index.html"),
        name="reset-password-form",
    ),
    path("reset-password/", include("django_rest_passwordreset.urls", namespace="reset-password")),
]
