import cloudinary
from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import Group
from django_summernote.admin import SummernoteModelAdmin

from marks.models import RouteMarkModel
from photos.models import CommentPhoto, RoutePhoto, UserPhoto
from routes.models import RouteModel
from travels.models import TravelModel

from .forms import UserAdminChangeForm, UserAdminCreationForm
from .models import CustomUser


class UserAdmin(BaseUserAdmin):
    # The forms to add and change user instances
    form = UserAdminChangeForm
    add_form = UserAdminCreationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ("email", "username", "first_name", "last_name", "staff", "admin")
    list_filter = ("admin", "staff")
    fieldsets = (
        (None, {"fields": ("email",)}),
        ("Personal info", {"fields": ("username", "first_name", "last_name")}),
        ("Permissions", {"fields": ("staff", "admin")}),
    )
    readonly_fields = ("admin",)
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": (
                    "email",
                    "username",
                    "first_name",
                    "last_name",
                    "password1",
                    "password2",
                    "staff",
                ),
            },
        ),
    )
    search_fields = ("email",)
    ordering = ("email",)
    filter_horizontal = ()


class VotesInLine(admin.TabularInline):
    model = RouteMarkModel
    extra = 0
    verbose_name = "vote"
    verbose_name_plural = "votes"
    readonly_fields = ["user_id", "date_created"]
    fieldsets = ((None, {"fields": ("user_id", "mark", "comment", "date_created")}),)


class RouteAdmin(SummernoteModelAdmin):
    readonly_fields = ["id"]
    summernote_fields = "description"
    list_display = ("id", "title", "length", "duration", "type", "mark")
    list_filter = ("length", "duration", "type", "mark")
    fieldsets = (
        (
            None,
            {
                "fields": (
                    "id",
                    "related_models",
                    "title",
                    "length",
                    "difficulty",
                    "duration",
                    "type",
                    "google_link",
                )
            },
        ),
        (
            "Description",
            {"fields": ("places_list", "description", "short_description", "creator_id")},
        ),
        ("Votes", {"fields": ("mark", "votes_cnt")}),
    )
    search_fields = ("title",)
    inlines = [VotesInLine]


class RouteMarkAdmin(admin.ModelAdmin):
    list_display = ("user_id", "get_route_id_title", "mark")
    search_fields = ("user_id", "route_id")
    list_filter = ("mark",)

    def get_route_id_title(self, obj):
        return "{}: {}".format(obj.route_id.id, obj.route_id.title)

    get_route_id_title.admin_order_field = "route_id"  # Allows column order sorting
    get_route_id_title.short_description = "Route Title"  # Renames column head

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "route_id":
            return RouteChoiceField(queryset=RouteModel.objects.all())
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


class UserPhotoAdmin(admin.ModelAdmin):
    actions = ["delete_selected"]
    # readonly_fields = ["uploader"]
    list_display = ("uploader", "image")
    fieldsets = ((None, {"fields": ("uploader", "image")}),)
    search_fields = ("uploader",)

    def delete_queryset(self, request, queryset):
        for obj in queryset:
            public_id = obj.get_public_id()
            cloudinary.uploader.destroy(public_id)
            obj.delete()


class RoutePhotoAdmin(admin.ModelAdmin):
    actions = ["delete_selected"]
    # readonly_fields = ["uploader", "route"]
    list_display = ("uploader", "get_route_id_title", "image")
    fieldsets = ((None, {"fields": ("uploader", "route", "image")}),)
    search_fields = ("uploader", "route")

    def delete_queryset(self, request, queryset):
        for obj in queryset:
            public_id = obj.get_public_id()
            cloudinary.uploader.destroy(public_id)
            obj.delete()

    def get_route_id_title(self, obj):
        return "{}: {}".format(obj.route.id, obj.route.title)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "route":
            return RouteChoiceField(queryset=RouteModel.objects.all())
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    get_route_id_title.admin_order_field = "route"  # Allows column order sorting
    get_route_id_title.short_description = "Route"  # Renames column head


class CommentPhotoAdmin(admin.ModelAdmin):
    actions = ["delete_selected"]
    readonly_fields = ["uploader", "comment"]
    list_display = ("uploader", "get_comment_id_route", "image")
    fieldsets = ((None, {"fields": ("uploader", "comment", "image")}),)
    search_fields = ("uploader", "comment")

    def delete_queryset(self, request, queryset):
        for obj in queryset:
            public_id = obj.get_public_id()
            cloudinary.uploader.destroy(public_id)
            obj.delete()

    def get_comment_id_route(self, obj):
        return "{}: ({}) {}".format(
            obj.comment.id, obj.comment.route_id.id, obj.comment.route_id.title
        )

    get_comment_id_route.admin_order_field = "comment"  # Allows column order sorting
    get_comment_id_route.short_description = "Comment"  # Renames column head


class TravelAdmin(admin.ModelAdmin):
    readonly_fields = [
        "secret_id",
    ]
    list_display = (
        "id",
        "organizer_id",
        "get_route_title",
        "title",
        "assembly_point",
        "is_private",
    )
    filter_horizontal = ("participants",)

    def get_route_title(self, obj):
        return obj.route_id.title

    get_route_title.admin_order_field = "route_id"  # Allows column order sorting
    get_route_title.short_description = "Route Title"  # Renames column head

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "route_id":
            return RouteChoiceField(queryset=RouteModel.objects.all())
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


class RouteChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return "{}: {}".format(obj.id, obj.title)


class CommentChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return "{}: {}".format(obj.id, obj.title)


admin.site.register(CustomUser, UserAdmin)
admin.site.register(RouteModel, RouteAdmin)
admin.site.register(RouteMarkModel, RouteMarkAdmin)
admin.site.register(UserPhoto, UserPhotoAdmin)
admin.site.register(RoutePhoto, RoutePhotoAdmin)
admin.site.register(CommentPhoto, CommentPhotoAdmin)
admin.site.register(TravelModel, TravelAdmin)
admin.site.unregister(Group)
admin.site.site_header = "Podrozownik Admin"
admin.site.site_title = "Podrozownik Admin Portal"
admin.site.index_title = "Welcome to Podrozownik Portal"
