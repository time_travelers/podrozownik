from django.contrib.auth.tokens import PasswordResetTokenGenerator


class TokenGenerator(PasswordResetTokenGenerator):
    """
    Generates token for registration
    """

    def _make_hash_value(self, user, timestamp):
        return (
            # text type in P3 should be str according to documentation
            str(user.pk)
            + str(timestamp)
            + str(user.is_active)
        )


account_activation_token = TokenGenerator()
