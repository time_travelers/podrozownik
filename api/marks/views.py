from django.http import HttpResponse
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import RouteMarkModel
from .serializers import RouteMarkSerializer


class AddComment(APIView):
    """
    Adds comment.
    """

    permission_classes = (IsAuthenticated,)

    def post(self, request, format="json"):

        if request.method == "POST":
            request.data["user_id"] = request.user.id
            serializer = RouteMarkSerializer(data=request.data)
            if serializer.is_valid():
                Mark = serializer.save()
                if Mark:
                    Mark.save()
                    return Response(status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class EditComment(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request, format="json"):

        if request.method == "POST":

            Mark = RouteMarkModel.objects.get(id=request.data["id"])

            if Mark is not None:

                if request.user.id == Mark.user_id.id:
                    if request.data["mark"] is not None:
                        Mark.set_mark(request.data["mark"])

                    if request.data["comment"] is not None:
                        Mark.set_comment(request.data["comment"])

                    Mark.save()

                    return HttpResponse(status=status.HTTP_201_CREATED)

        return HttpResponse(status=status.HTTP_400_BAD_REQUEST)
