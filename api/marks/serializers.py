from rest_framework import serializers

from .models import RouteMarkModel


class RouteMarkSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        RouteMark = RouteMarkModel.objects.create_route_mark(
            route_id=validated_data["route_id"],
            user_id=validated_data["user_id"],
            mark=validated_data["mark"],
            comment=validated_data["comment"],
            date_created=validated_data["date_created"],
            positioning=validated_data["positioning"],
        )
        return RouteMark

    class Meta:
        model = RouteMarkModel
        fields = ("route_id", "user_id", "mark", "comment", "date_created", "positioning")
