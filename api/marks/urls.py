from django.urls import path

from . import views


urlpatterns = [
    path("add/", views.AddComment.as_view(), name="add"),
    path("edit/", views.EditComment.as_view(), name="edit"),
]
