from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.utils import timezone

from accounts.models import CustomUser
from routes.models import RouteModel


class RouteMarkManager(models.Manager):
    def create_route_mark(self, route_id, user_id, mark, comment, date_created, positioning):
        if not route_id:
            raise ValueError("Mark must have a route")

        if not user_id:
            raise ValueError("Mark must have a user")

        if not mark:
            raise ValueError("Mark must have a value")

        if not date_created:
            raise ValueError("Mark must have date")

        route_mark = self.model()
        route_mark.user_id = user_id
        route_mark.route_id = route_id
        route_mark.mark = mark
        if comment is not None:
            route_mark.comment = comment
        route_mark.date_created = date_created
        if positioning is not None:
            route_mark.positioning = positioning
        route_mark.save(using=self._db)
        return route_mark


class RouteMarkModel(models.Model):
    id = models.AutoField(primary_key=True)
    route_id = models.ForeignKey(RouteModel, on_delete=models.CASCADE)
    user_id = models.ForeignKey(CustomUser, null=True, on_delete=models.SET_NULL)
    mark = models.IntegerField(default=5, validators=[MaxValueValidator(5), MinValueValidator(0)])
    comment = models.TextField(max_length=1000, blank=True)
    date_created = models.DateField(default=timezone.now)
    positioning = models.FloatField(default=1.0)
    objects = RouteMarkManager()

    def get_id(self):
        return self.id

    def get_route_id(self):
        return self.route_id

    def set_route_id(self, new_id):
        self.route_id = new_id

    def get_user_id(self):
        return self.user_id

    def set_user_id(self, new_id):
        self.user_id = new_id

    def get_mark(self):
        return self.mark

    def set_mark(self, new_mark):
        self.mark = new_mark

    def get_comment(self):
        return self.comment

    def set_comment(self, new_comment):
        self.comment = new_comment

    def get_date(self):
        return self.date_created

    def set_date(self, new_date):
        self.date_created = new_date

    def get_positioning(self):
        return self.positioning

    def set_positioning(self, new_value):
        self.positioning = new_value
