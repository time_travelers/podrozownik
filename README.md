# Podróżownik

An app that connects motorcyclists and makes it easy for them to create events and sign up for them. Thanks to our application motorcyclists can browse interesting routes proposed by other users, or create events where you will be able to travel it in a tour with other motorcycle enthusiasts.

## Technology

The project was designed to work as a single page application.
Technology stack:

- [ReactJS](https://reactjs.org/) - frontend
- [Django](https://www.djangoproject.com/) - with use of [Django REST framework](https://www.django-rest-framework.org/) for api calls from frontend.<br>

Api documentation is available at [podrozownik-prod.herokuapp.com/api](https://podrozownik-prod.herokuapp.com/api).

## Launch

To launch an app, go to project root directory where `docker-compose.yml` file is and then run:

```bash
docker-compose up
```

This will create:
- database,
- api server with compiled react app.

## Enviroment configuration

### Development environment

App is fully configured to run in development mode.

**WARNING**
> If you'll not provide value of Cloudinary Api key and Cloudinary Secret, route for upload image will not work.

### Production environment
To run app in production mode, add file named `.env` (where in same directory, where this file is) where you will add environmental variables with keys to 
Cloudinary and Google API, to allow app to store images and send real e-mails

**.env example**
```env
CLOUDINARY_API_KEY=0123456789
CLOUDINARY_API_SECRET=asdfghjkl
EMAIL_HOST_USER=admin@gmail.com
EMAIL_HOST_PASSWORD=password1234
```

Read more in [api](./api/README.md) and [frontend](./frontend/README.md) README.md.

## Authors

- Damian Kokot(leader),
- Szymon Skoczylas,
- Marcin Szadkowski,
- Wojtek Wróblewski,
- Barłomiej Ratajczak,
- Karol Olszański
